##### usb快捷方式
- app目录放入apk (优先级最高)
- replaceFace目录替换人脸 (优先级次之)
- addFace目录添加人脸(优先级最低)


#### 待解完成问题
1. ACRA配置NativeError捕获、ARN捕获、日志本地记录
2. 怎么hook程序Crash
3. 热修复模块的实现
4. 程序更新
5. 添加程序更新时的签名验证，URL验证，MD5验证
7. 首屏启动空白3S问题
8. 添加Bug服务器地址配置<可选>

- Room查看版本更新错误 ：断点 RoomDatabase -> init()方法内断点 mOpenHelper.getReadableDatabase() 可以产看到

#### 人脸识别
##### 虹软
###### 特点
- 特征大小20K
- 无活体检测
- 可识别身份证上的人脸
###### 性能 CPU Rockchip RK3288下
- 640*480特征提取速度700ms
- 单线程下，640*480一帧 1000人脸对比时间 （600ms对比时间 + 700ms提取特征时间 = 1300ms）
#### 虹软人脸识别2.1
###### 特点
- 特征大小2K
- 有活体检测
- 身份证上的人脸需要使用单独的sdk
###### 性能 CPU Rockchip RK3288下
- 不限制分辨率特征提取速度320ms
###### 注意
- 身份证识别和一般人脸识别需要使用不同SDK——由于jar包使用相同包名类名需要分开打包
    - 解决办法是
    - 1.在gradle里面配置两套jar包引用方式来共享代码（修改 app 的 build.gradle 的 useToIDCard 变量)。
    - 2.不同打包要手动更换libarcsoft_face.so——在sdk文件夹下的libarcsoft_face.so(useToIDCard=false),libarcsoft_face_idcard.so(useToIDCard=true)文件，其中libarcsoft_face_idcard.so使用时更名为libarcsoft_face.so

##### 新板子设备连接接口
- 二维码： /dev/ttyS4
- 高频(身份证)、超高频： /dev/ttyS1
- 指纹： /dev/ttyS0


###### 当前任务
- //todo 监控软件状态（摄像头是否卡死,软件是否死掉）
- //todo 远程控制设备（配置设备,重启设备）
- //todo 视频流监控
- //todo 调查RtmpCameraHelperBase -> microphoneManager.stop()的异常有什么影响
