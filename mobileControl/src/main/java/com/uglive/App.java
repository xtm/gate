package com.uglive;

import android.app.Application;

import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.blankj.utilcode.util.Utils;

import okhttp3.internal.Util;

public class App extends Application {

    @Override public void onCreate() {
        super.onCreate();
        TypefaceProvider.registerDefaultIconSets();
        Utils.init(this);
    }
}


