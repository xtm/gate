package com.uglive.handticketcheck.network;

import com.alibaba.fastjson.JSON;
import com.uglive.handticketcheck.bean.TicketUsedShareMessage;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UDPControlller {
    public static ExecutorService exe = Executors.newSingleThreadExecutor();
    public static final byte MSG_DELETE_TICKETFACE = (byte)1;
    public static void deleteFace(String ticketID){
        byte[] msgByteArray = JSON.toJSONString(new TicketUsedShareMessage(ticketID,"","")).getBytes();
        // 数据长度是 一个 int 代表msgByteArray长度 + 一个 byte 代表数据类型 + msgByteArray长度
        byte[] data = new byte[msgByteArray.length+1+4];
        data[0] = MSG_DELETE_TICKETFACE;
        byte[] lengthByteArray = ByteBuffer.allocate(4).putInt(msgByteArray.length).array();
        System.arraycopy(lengthByteArray,0,data,1,lengthByteArray.length);
        System.arraycopy(msgByteArray, 0, data, 5, msgByteArray.length);
        exe.submit(() -> {
            try {
                new DatagramSocket().send(new DatagramPacket(
                        data,
                        data.length,
                        InetAddress.getByName(getBroadcastIpAddress()),
                        22000));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    /**
     * Return the ip address of broadcast.
     *
     * @return the ip address of broadcast
     */
    public static String getBroadcastIpAddress() {
        try {
            Enumeration<NetworkInterface> nis = NetworkInterface.getNetworkInterfaces();
            LinkedList<InetAddress> adds = new LinkedList<>();
            while (nis.hasMoreElements()) {
                NetworkInterface ni = nis.nextElement();
                if (!ni.isUp() || ni.isLoopback()) continue;
                List<InterfaceAddress> ias = ni.getInterfaceAddresses();
                for (int i = 0; i < ias.size(); i++) {
                    InterfaceAddress ia = ias.get(i);
                    InetAddress broadcast = ia.getBroadcast();
                    if (broadcast != null) {
                        return broadcast.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return "";
    }
}
