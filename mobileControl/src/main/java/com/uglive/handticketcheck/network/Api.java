package com.uglive.handticketcheck.network;

import com.alibaba.fastjson.JSON;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.uglive.handticketcheck.bean.ApiResult;
import com.uglive.handticketcheck.bean.DeleteFaceResult;
import com.uglive.handticketcheck.bean.Ticket;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Api {
    private static String ip = SPUtils.getInstance().getString("IP","116.62.57.42");
//    private final String ip = "192.168.3.98";
    private static String rootUrl = "http://" + ip + ":8084/Api";

    private static String deleteFaceUrl =  "http://" + ip +":60000/app/deleteFace";

    public static void setIp(String ip){
        SPUtils.getInstance().put("IP",ip);
        Api.ip = ip;
        Api.rootUrl = "http://" + ip + ":8084/Api";
        Api.deleteFaceUrl = "http://" + ip +":60000/app/deleteFace";
    }

    private final OkHttpClient mClient = new OkHttpClient
            .Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    public List<Ticket> loadData(String activityId) {
        Request request = new Request.Builder()
                .url(rootUrl + "/getWorkList.php" + "?projectId=" + activityId)
                .build();
        try (Response response = mClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                ApiResult result = JSON.parseObject(response.body().string(), ApiResult.class);
                if (result.getCode() == 1) {
                    List<Ticket> ticketList = new LinkedList<Ticket>();
                    int id = 0;
                    for (Map<String, Object> map : result.getList()) {
                        Ticket t = new Ticket();
                        t.setId(id++);
                        t.setM1((String) map.get("chip"));
                        t.setQrCode((String) map.get("qrcode"));
                        t.setValidateType((int) map.get("type"));
                        ticketList.add(t);
                    }
                    return ticketList;
                } else {
                    ToastUtils.showShortSafe("服务器错误");
                }
            } else {
                ToastUtils.showShortSafe("返回值错误");
            }
        } catch (IOException e) {
            e.printStackTrace();
            ToastUtils.showShortSafe("网络连接失败");
        }
        return null;
    }

    public int deleteFace(String ticketId){
        Request request = new Request.Builder()
                .url(deleteFaceUrl + "?rfid=" +ticketId)
                .build();
        try (Response response = mClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                if (response.body() != null){
                  DeleteFaceResult result =  JSON.parseObject(response.body().string(), DeleteFaceResult.class);
                  return (int) result.getData();
                }
            } else {
                ToastUtils.showShortSafe("返回值错误");
            }
        } catch (IOException e) {
            e.printStackTrace();
            ToastUtils.showShortSafe("网络连接失败");
        }
        return 0;
    }
}
