package com.uglive.handticketcheck.ui.checkTicket;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;

import com.uglive.handticketcheck.device.scan.DH8600Scan;
import com.uglive.handticketcheck.device.scan.IScan;
import com.uglive.handticketcheck.model.InfoModel;
import com.uglive.handticketcheck.model.TicketModel;
import com.uglive.handticketcheck.utils.SoundPoolUtil;
import com.yuwei.utils.ByteUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainPresenter implements MainContract.Presenter {

    private TicketModel ticketModel;
    private InfoModel infoModel = new InfoModel();

    private MainContract.View view;

    private SoundPoolUtil soundPoolUtil;
    private IScan iScan;

    private byte[] buffer = new byte[32];

    private ExecutorService taskThreadPool = Executors.newSingleThreadExecutor();

    private Handler mainHandler = new Handler(Looper.getMainLooper());

    MainPresenter(TicketModel ticketModel, MainContract.View view) {
        this.ticketModel = ticketModel;
        this.view = view;
        soundPoolUtil = new SoundPoolUtil((Context)view);
    }


    @Override
    public void loadTickets(String activityId) {
        view.showProgress();
        taskThreadPool.submit(() -> {
            final long size = ticketModel.loadData(activityId);
            mainHandler.post(() -> {
                view.dismissProgress();
                view.showCountTickets((int) size);
            });
        });
        infoModel.saveActivtyId(activityId);
    }

    @Override
    public void scanM1(String secret) {
//        soundPoolUtil.play();
        taskThreadPool.submit(() -> {
            switch (iScan.readSecret(secret, buffer)) {
                case AUTH_FAIL:
                    mainHandler.post(() -> {
                        view.showTip("该票据不是本场活动的票", Color.RED);
                    });
                    break;
                case REQUEST_FAIL:
                    mainHandler.post(() -> {
                        view.showTip("没扫到票，请贴紧扫卡处", Color.CYAN);
                    });
                    break;
                case SUCCESS:
                    String decode = ByteUtil.decode(new String(buffer));
                    String ticket = decode.substring(0, 12);
//                    if (!ticketModel.containTicket(ticket, Ticket.M1)) {
//                        mainHandler.post(() -> {
//                            view.showTicket(ticket);
//                            view.showTip("该票据不是本场活动的票", Color.RED);
//                        });
//                    } else {
//                        mainHandler.post(() -> {
//                            view.showTicket(ticket);
//                            view.showTip("验证成功", Color.GREEN);
//                        });
//                    }
                    if (ticket.contains("@@@@@@@")) {
                        mainHandler.post(() -> {
                            view.showTip("验证通过", Color.GREEN);
                            int count = ticketModel.getScanCount() + 1;
                            ticketModel.setScanCount(count);
                            view.updateScanCount(count);
                        });
                    } else if (ticket.contains("!!!!!")) {
                        Set<String> list = ticketModel.scanList();
                        if (null == list) list = new HashSet<>();
                        if (!list.contains(ticket)) {
                            list.add(ticket);
                            ticketModel.setScanTicket(list);
                            mainHandler.post(() -> {
                                view.showTip("验证通过", Color.GREEN);
                                int count = ticketModel.getScanCount() + 1;
                                ticketModel.setScanCount(count);
                                view.updateScanCount(count);
                            });
                        } else {
                            mainHandler.post(() -> view.showTip("该票已经使用", Color.RED));
                        }
                    } else {
                        mainHandler.post(() -> {
                            view.showTip("验证通过", Color.GREEN);
                            int count = ticketModel.getScanCount() + 1;
                            ticketModel.setScanCount(count);
                            view.updateScanCount(count);
                        });
                    }

                    break;
            }
        });
        infoModel.saveSecret(secret);
    }

    @Override
    public String getActivityId() {
        return infoModel.getActivtyId();
    }

    @Override
    public String getSecrect() {
        return infoModel.getSecret();
    }

    @Override
    public void clearScanCount() {
        ticketModel.setScanCount(0);
        ticketModel.setScanTicket(new HashSet<>());
        view.updateScanCount(0);
    }

    @Override
    public void saveSecrect(String secret) {
        infoModel.saveSecret(secret);
    }

    @Override
    public void init() {
        iScan = new DH8600Scan();
        iScan.open((AppCompatActivity) view);
        taskThreadPool.submit(() -> {
            int count = ticketModel.getTicketCount();
            mainHandler.post(() -> {
                view.showCountTickets(count);
            });
        });
    }

    @Override
    public void release() {
        iScan.close();
    }
}
