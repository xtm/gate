package com.uglive.handticketcheck.ui.delFaceInGateApp;

public interface  ControllerContract {
    interface View {
        void showTicket(String ticket);

        void showTip(String msg, int color);

    }

    interface Presenter {

        void saveSecret(String secret);

        void deleteFace();

        void init();

        String getIp();

        void saveIp(String ip);

        void release();
    }
}
