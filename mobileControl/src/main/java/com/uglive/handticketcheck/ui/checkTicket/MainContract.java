package com.uglive.handticketcheck.ui.checkTicket;

public interface MainContract {
    interface View {
        void showTicket(String ticket);

        void showCountTickets(int size);

        void showTip(String msg, int color);

        void showProgress();

        void updateScanCount(int count);

        void dismissProgress();
    }

    interface Presenter {
        /**
         * 加载活动数据
         *
         * @param activityId 活动号
         */
        void loadTickets(String activityId);

        /**
         * 扫描m1卡
         *
         * @param secret 秘钥
         */
        void scanM1(String secret);

        String getActivityId();

        String getSecrect();

        void clearScanCount();

        void saveSecrect(String secret);

        void init();

        void release();
    }
}
