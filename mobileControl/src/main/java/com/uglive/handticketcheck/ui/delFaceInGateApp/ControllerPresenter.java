package com.uglive.handticketcheck.ui.delFaceInGateApp;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;

import com.blankj.utilcode.util.SPUtils;
import com.uglive.handticketcheck.device.scan.DH8600Scan;
import com.uglive.handticketcheck.device.scan.IScan;
import com.uglive.handticketcheck.model.InfoModel;
import com.uglive.handticketcheck.network.Api;
import com.uglive.handticketcheck.network.UDPControlller;
import com.yuwei.utils.ByteUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ControllerPresenter implements ControllerContract.Presenter {


    private IScan iScan;
    private ControllerContract.View view;
    private InfoModel infoModel = new InfoModel();
    private ExecutorService taskThreadPool = Executors.newSingleThreadExecutor();
    private byte[] buffer = new byte[32];

    private Api api = new Api();


    public ControllerPresenter(ControllerContract.View view) {
        this.view = view;
    }


    @Override
    public void saveSecret(String secret) {
        infoModel.saveSecret(secret);
    }

    @Override
    public void deleteFace() {
        taskThreadPool.submit(() -> {
            switch (iScan.readSecret(infoModel.getSecret(), buffer)) {
                case AUTH_FAIL:
                        view.showTip("此票秘钥错误", Color.RED);
                    break;
                case REQUEST_FAIL:
                        view.showTip("没扫到票，请贴紧扫卡处", Color.CYAN);
                    break;
                case SUCCESS:
                    String decode = ByteUtil.decode(new String(buffer));
                    String ticket = decode.substring(0, 12);
                    view.showTicket(ticket);
                    UDPControlller.deleteFace(ticket);
                    int devices = api.deleteFace(ticket);
                    if (devices > 0) view.showTip("成功通知" + devices + "台设备删除图片", Color.GREEN);
                    else view.showTip("无设备连接", Color.CYAN);
                    //TODO 删除人脸
                    break;
            }
        });
    }



    @Override
    public void init() {
        iScan = new DH8600Scan();
        iScan.open((AppCompatActivity) view);
    }

    @Override
    public String getIp() {
        return SPUtils.getInstance().getString("IP");
    }

    @Override
    public void saveIp(String ip) {
        Api.setIp(ip);
    }

    @Override
    public void release() {
        iScan.close();
    }
}
