package com.uglive.handticketcheck.ui.delFaceInGateApp;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.uglive.handticketcheck.R;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ControllerActivity extends AppCompatActivity implements ControllerContract.View {

    @BindView(R.id.ticketId)
    TextView ticketId;
    @BindView(R.id.showStateInfo)
    TextView showStateInfo;


    private ControllerContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controler);
        ButterKnife.bind(this);
        presenter = new ControllerPresenter(this);
        presenter.init();
    }

    @OnClick(R.id.setSecret)
    void click() {
        LinearLayout ll = new LinearLayout(this);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(24, 12, 24, 12);
        EditText editText = new EditText(this);
        ll.addView(editText, llp);
        new AlertDialog
                .Builder(this)
                .setView(ll)
                .setTitle("输入管理员密码")
                .setPositiveButton("确定", (d, w) -> {
                    if (editText.getText().toString().trim().equals("147852")) {
                        d.dismiss();
                        LinearLayout ll1 = new LinearLayout(this);
                        LinearLayout.LayoutParams llp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        llp1.setMargins(24, 12, 24, 12);
                        EditText editText1 = new EditText(this);
                        ll1.addView(editText1, llp1);
                        new AlertDialog
                                .Builder(this)
                                .setView(ll1)
                                .setTitle("输入秘钥")
                                .setPositiveButton("确定", (d1, w1) -> {
                                    if (!TextUtils.isEmpty(editText1.getText().toString().trim())) {
                                        presenter.saveSecret(editText1.getText().toString().trim());
                                        d1.dismiss();
                                    } else {
                                        ToastUtils.showShortSafe("秘钥不能为空");
                                    }
                                }).show();
                    } else {
                        ToastUtils.showShortSafe("管理员密码错误");
                    }

                })
                .show();
    }

    @OnClick(R.id.delFace)
    void delFace(){
        presenter.deleteFace();
    }

    @OnClick(R.id.setIP)
    void setIp(){
        LinearLayout ll1 = new LinearLayout(this);
        LinearLayout.LayoutParams llp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp1.setMargins(24, 12, 24, 12);
        EditText editText1 = new EditText(this);
        editText1.setText(presenter.getIp());
        ll1.addView(editText1, llp1);
        new AlertDialog
                .Builder(this)
                .setView(ll1)
                .setTitle("输入服务器IP")
                .setPositiveButton("确定", (d1, w1) -> {
                    if (!TextUtils.isEmpty(editText1.getText().toString().trim())) {
                        presenter.saveIp(editText1.getText().toString().trim());
                        d1.dismiss();
                    } else {
                        ToastUtils.showShortSafe("IP不能为空");
                    }
                }).show();
    }


    @Override
    public void showTicket(String ticket) {
        runOnUiThread(() ->  ticketId.setText(ticket));
    }

    @Override
    public void showTip(String msg, int color) {
        runOnUiThread(() ->  {
            showStateInfo.setTextColor(color);
            showStateInfo.setText(msg);
            resetScanInfoDelay();
        });
    }

    @Override
    protected void onDestroy() {
        presenter.release();
        super.onDestroy();
    }
    private TimerTask resetUITask;

    private void resetScanInfoDelay() {
        if (resetUITask != null) resetUITask.cancel();
        resetUITask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    ticketId.setText("");
                    showStateInfo.setText(R.string.tipmsg);
                    showStateInfo.setTextColor(getResources().getColor(android.R.color.holo_orange_light));
                });
            }
        };
        new Timer().schedule(resetUITask, 2500);
    }
}
