package com.uglive.handticketcheck.ui.checkTicket;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.ToastUtils;
import com.uglive.handticketcheck.R;
import com.uglive.handticketcheck.model.TicketModel;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    @BindView(R.id.tvTip)
    TextView tip;
    @BindView(R.id.edtActId)
    EditText editActId;
    @BindView(R.id.edtSecret)
    EditText edtSecret;
    @BindView(R.id.tvTicket)
    TextView tvTicket;
    @BindView(R.id.tvTicketSize)
    TextView tvTicketSize;

    @BindView(R.id.scancount)
    TextView tvScanCount;
    private ProgressDialog pd;
    private MainContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        pd = new ProgressDialog(this);
        presenter = new MainPresenter(new TicketModel(this), this);
        editActId.setText(presenter.getActivityId());
        edtSecret.setText(presenter.getSecrect());
        presenter.init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.release();
    }

    @OnClick(R.id.btnLoadData)
    void onClick() {
        String actId = editActId.getText().toString();
        if (TextUtils.isEmpty(actId)) {
            Toast.makeText(this, "请输入活动号", Toast.LENGTH_SHORT).show();
        } else {
            presenter.loadTickets(actId);
        }

    }

    @OnClick(R.id.btnSetSecret)
    void click() {
        LinearLayout ll = new LinearLayout(this);
        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llp.setMargins(24, 12, 24, 12);
        EditText editText = new EditText(this);
        ll.addView(editText, llp);
        new AlertDialog
                .Builder(this)
                .setView(ll)
                .setTitle("输入管理员密码")
                .setPositiveButton("确定", (d, w) -> {
                    if (editText.getText().toString().trim().equals("147852")) {
                        d.dismiss();
                        LinearLayout ll1 = new LinearLayout(this);
                        LinearLayout.LayoutParams llp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        llp1.setMargins(24, 12, 24, 12);
                        EditText editText1 = new EditText(this);
                        ll1.addView(editText1, llp1);
                        new AlertDialog
                                .Builder(this)
                                .setView(ll1)
                                .setTitle("输入秘钥")
                                .setPositiveButton("确定", (d1, w1) -> {
                                    if (!TextUtils.isEmpty(editText1.getText().toString().trim())) {
                                        presenter.saveSecrect(editText1.getText().toString().trim());
                                        d1.dismiss();
                                    } else {
                                        ToastUtils.showShortSafe("秘钥不能为空");
                                    }
                                }).show();
                    } else {
                        ToastUtils.showShortSafe("管理员密码错误");
                    }

                })
                .show();
    }
    @OnClick(R.id.btnClearCount)
    void clearCount(){
        new AlertDialog
                .Builder(this)
                .setTitle("清楚统计数")
                .setMessage("确定清楚统计数吗？")
                .setPositiveButton("确定", (d, w) -> {
                    presenter.clearScanCount();
                    d.dismiss();
                })
                .setNegativeButton("取消",null)
                .show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 141) {
            String secret = presenter.getSecrect();
            if (TextUtils.isEmpty(secret)) {
                Toast.makeText(this, "请输入秘钥", Toast.LENGTH_SHORT).show();
            } else {
                presenter.scanM1(secret);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void showTicket(String ticket) {
        tvTicket.setText(ticket);
    }

    @Override
    public void showCountTickets(int size) {
        tvTicketSize.setText(size + "");
    }

    @Override
    public void showTip(String msg, int color) {
        tip.setText(msg);
        tip.setTextColor(color);
        resetScanInfoDelay();
    }

    @Override
    public void showProgress() {
        pd.show();
    }

    @Override
    public void updateScanCount(int count) {
        runOnUiThread(()->tvScanCount.setText(count+""));
    }

    @Override
    public void dismissProgress() {
        pd.dismiss();
    }

    private TimerTask resetUITask;

    private void resetScanInfoDelay() {
        if (resetUITask != null) resetUITask.cancel();
        resetUITask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    tvTicket.setText("");
                    tip.setText(R.string.tipmsg);
                    tip.setTextColor(getResources().getColor(android.R.color.holo_orange_light));
                });
            }
        };
        new Timer().schedule(resetUITask, 3000);
    }

}
