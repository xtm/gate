package com.uglive.handticketcheck.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.uglive.handticketcheck.bean.Ticket;

import java.util.List;

@Dao
public interface TicketDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Ticket> tickets);

    @Query("DELETE FROM ticket")
    void deleteAll();

    @Query("SELECT COUNT(id) FROM ticket")
    int ticketSize();

    @Query("SELECT * FROM ticket WHERE m1 = :m1 "
            + "AND validateType = :type")
    List<Ticket> findTicketWithM1(String m1,int type);
}
