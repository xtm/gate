package com.uglive.handticketcheck.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.uglive.handticketcheck.bean.Ticket;

@Database(entities = {Ticket.class}, version = 1)
public abstract class DB extends RoomDatabase {
    public abstract TicketDao ticketDao();

    private static DB INSTANCE;


    public static DB getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (DB.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DB.class, "word_database")
                            .build();

                }
            }
        }
        return INSTANCE;
    }
}
