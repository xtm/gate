package com.uglive.handticketcheck.bean;

import java.util.List;
import java.util.Map;

public class ApiResult {
    private int code;
    private String message;
    private List<Map<String,Object>> list;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Map<String, Object>> getList() {
        return list;
    }

    public void setList(List<Map<String, Object>> list) {
        this.list = list;
    }
}
