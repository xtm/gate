package com.uglive.handticketcheck.bean;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "ticket")
public class Ticket {
    //票据验证类型
    public static final int
            M1 = 1,
            QRCODE = 2,
            BOTH = 0;


    @PrimaryKey @NonNull private int id;
    @ColumnInfo private String m1;
    @ColumnInfo private String qrCode;
    @ColumnInfo private int validateType;

    public String getM1() {
        return m1;
    }

    public void setM1(String m1) {
        this.m1 = m1;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public int getValidateType() {
        return validateType;
    }

    public void setValidateType(int validateType) {
        this.validateType = validateType;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }
}
