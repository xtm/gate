package com.uglive.handticketcheck.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.uglive.handticketcheck.R;


public class SoundPoolUtil {
    private static SoundPoolUtil soundPoolUtil;
    private static SoundPool soundPool;
    public static int DEEP;//蜂鸣


    public SoundPoolUtil(Context context) {
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 10);
        //加载音频文件
        DEEP = soundPool.load(context, R.raw.beep, 1);
    }

    //播放音频
    public void play() {
        soundPool.play(DEEP, 0.9f, 0.9f, 0, 0, 1);
    }
}
