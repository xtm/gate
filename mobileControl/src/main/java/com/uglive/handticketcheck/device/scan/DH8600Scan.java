package com.uglive.handticketcheck.device.scan;

import android.support.v7.app.AppCompatActivity;

import com.yuwei.utils.Card;
import com.yuwei.utils.CardM1;
import com.yuwei.utils.ModuleControl;

public class DH8600Scan implements IScan {
    @Override
    public boolean open(AppCompatActivity act) {
        CardM1.init(act,115200);
        return true;
    }

    @Override
    public void close() {
        ModuleControl.rf_rfinf_reset(CardM1.id, (byte) 0);
        CardM1.exit();
    }

    @Override
    public ScanResultType readSecret(String secret, byte[] buffer) {
        CardM1.ScanResult result = CardM1.read_hex(secret,buffer);
//        CardM1.ScanResult result = CardM1.read_hex_0(secret,buffer);
//        Card.rf_beep(200);
        if (result== CardM1.ScanResult.SUCCESS){
            return ScanResultType.SUCCESS;
        }else if (result == CardM1.ScanResult.AUTH_FAIL){
            return ScanResultType.AUTH_FAIL;
        }else {
            return ScanResultType.REQUEST_FAIL;
        }
    }

    @Override
    public ScanResultType readId(byte[] buffer) {
        return null;
    }
    @Override
    public String scanQrcode() {
        String bytes = Card.oneDimensionalCode();
        if (bytes != null) {
            Card.rf_beep(50);
        }
        return bytes;
    }
}
