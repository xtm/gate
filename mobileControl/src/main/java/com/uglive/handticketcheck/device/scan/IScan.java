package com.uglive.handticketcheck.device.scan;

import android.support.v7.app.AppCompatActivity;

public interface IScan {
    enum ScanResultType {
        AUTH_FAIL,//密码验证失败
        REQUEST_FAIL,//寻卡失败
        SUCCESS
    }

    boolean open(AppCompatActivity act);

    void close();

    //读取加密数据
    ScanResultType readSecret(String secret, byte[] buffer);

    //读取卡片ID
    ScanResultType readId(byte[] buffer);

    //扫二维码条码
    String scanQrcode();
}
