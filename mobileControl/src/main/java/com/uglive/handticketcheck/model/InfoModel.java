package com.uglive.handticketcheck.model;

import com.blankj.utilcode.util.SPUtils;

public class InfoModel {

    private final String SP_SCRECT = "sp_screct";
    private final String SP_ACTID = "sp_actid";


    public String getSecret() {
        return SPUtils.getInstance().getString(SP_SCRECT, "cs1234");
    }

    public void saveSecret(String secret) {
        SPUtils.getInstance().put(SP_SCRECT,secret);
    }

    public String getActivtyId() {
        return SPUtils.getInstance().getString(SP_ACTID, "");
    }

    public void saveActivtyId(String activtyId) {
        SPUtils.getInstance().put(SP_ACTID, activtyId);
    }
}
