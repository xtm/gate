package com.uglive.handticketcheck.model;

import android.content.Context;

import com.blankj.utilcode.util.SPUtils;
import com.uglive.handticketcheck.bean.Ticket;
import com.uglive.handticketcheck.db.DB;
import com.uglive.handticketcheck.db.TicketDao;
import com.uglive.handticketcheck.network.Api;

import java.util.List;
import java.util.Set;

public class TicketModel {
    private final String SP_SCAN_COUNT = "SP_SCAN_COUNT";
    private final String SP_SCAN_LIST = "SP_SCAN_LIST";
    private TicketDao dao;
    private Api api;

    public TicketModel(Context context) {
        dao = DB.getDatabase(context).ticketDao();
        api = new Api();
    }

    public int getTicketCount() {
        return dao.ticketSize();
    }

    //查询数据库是否包含票
    public boolean containTicket(String ticket, int type) {
        return !dao.findTicketWithM1(ticket, type).isEmpty();
    }

    //从服务器下载数据，同时更新数据库
    public long loadData(String activityId) {
        dao.deleteAll();
        List<Ticket> data = api.loadData(activityId);
        if (data != null) {
            dao.insert(data);
            return data.size();
        }
        return 0;
    }

    public void setScanCount(int count) {
        SPUtils.getInstance().put(SP_SCAN_COUNT, count);
    }

    public int getScanCount() {
        return SPUtils.getInstance().getInt(SP_SCAN_COUNT, 0);
    }

    public Set<String> scanList(){
        return SPUtils.getInstance().getStringSet(SP_SCAN_LIST);
    }
    public void setScanTicket(Set<String> tickets){
        SPUtils.getInstance().put(SP_SCAN_LIST,tickets);
    }
}
