package com.uglive.gate.db.localDB

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.uglive.gate.bean.LocalTicket


@Dao
interface LocalTicketDao {
    @Insert
    fun insert(tickets: List<LocalTicket>)

    @Insert
    fun insert(ticket: LocalTicket)

    @Query("select m1id from LocalTicket")
    fun getAll(): List<String>
}