package com.uglive.gate.db.localDB

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import com.uglive.gate.bean.TicketBean


@Dao
interface TicketDao {

    @Insert
    fun insert(tickets: List<TicketBean>)

    @Insert
    fun insert(ticket: TicketBean)

    @Query(value = "delete from ticket")
    fun deleteAll()

    @Query(value = "select * from ticket")
    fun getAll(): List<TicketBean>

    @Query(value = "select count(*) from ticket")
    fun countTickets(): Int

    @Query(value = "select * from ticket where soldChip like :rfid and soldCode like :qrCode order by soldId limit :pageSize offset :offset ")
    fun queryTickets(pageSize: Int, offset: Int, rfid: String, qrCode: String): List<TicketBean>

    @Update
    fun updateItem(ticket: TicketBean)

}