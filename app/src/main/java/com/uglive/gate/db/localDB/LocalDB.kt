package com.uglive.gate.db.localDB

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.migration.Migration
import android.content.Context
import com.uglive.gate.bean.Face
import com.uglive.gate.bean.LocalTicket
import com.uglive.gate.bean.RequestBean
import com.uglive.gate.bean.TicketBean

@Database(entities = [TicketBean::class, RequestBean::class, LocalTicket::class, Face::class], version = 4)
abstract class LocalDB : RoomDatabase() {

    abstract fun ticketDao(): TicketDao

    abstract fun requestDao(): RequestDao

    abstract fun localTicketDao(): LocalTicketDao

    abstract fun faceDao(): FaceDao

    companion object {
        private var instance: LocalDB? = null
        @Synchronized
        fun get(context: Context): LocalDB {
            if (instance == null) {
                instance = Room.databaseBuilder(context.applicationContext, LocalDB::class.java, "localDatabase")
                        .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4).build()
            }
            return instance!!
        }

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                //字段名不能加``号，否者对应不上room的字段
                database.execSQL("""CREATE  TABLE  `LocalTicket` (m1id TEXT NOT NULL, PRIMARY KEY(m1id))""")
            }
        }
        val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""ALTER  TABLE  `request` ADD COLUMN req_success INTEGER """)
            }
        }
        val MIGRATION_3_4 = object :Migration(3,4){
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""CREATE  TABLE  `face` (id INTEGER PRIMARY KEY,fileName TEXT, ticketId TEXT, picSrcIp TEXT, future BLOB)""")
            }

        }
    }
}


