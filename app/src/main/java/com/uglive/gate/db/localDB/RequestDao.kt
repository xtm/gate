package com.uglive.gate.db.localDB

import android.arch.persistence.room.*
import com.uglive.gate.bean.RequestBean

/**
 * Created by xiantm on 2018/5/3.
 */
@Dao
interface RequestDao {

    @Insert
    fun insert(requestBean: RequestBean)

    @Insert
    fun insert(requestBean: List<RequestBean>)

    @Delete
    fun delete(requestBean: RequestBean)

    @Update
    fun update(requestBean: RequestBean)

    @Query("select * from request")
    fun getAll(): List<RequestBean>

    @Query("select * from request where req_success=0")
    fun getNotUpload(): List<RequestBean>

    @Query(value = "delete from request")
    fun deleteAll()

    @Query(value = "select count(*) from request where req_success=1")
    fun countSucSize(): Int

    @Query(value = "select count(*) from request")
    fun countTotal(): Int
}