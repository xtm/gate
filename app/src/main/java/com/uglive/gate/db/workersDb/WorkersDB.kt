package com.uglive.gate.db.workersDb

import android.arch.persistence.db.SupportSQLiteOpenHelper
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.PathUtils
import com.blankj.utilcode.util.SPUtils
import com.uglive.gate.bean.Worker
import java.io.FileOutputStream
import java.io.IOException

@Database(entities = [Worker::class],version = 1)
abstract class WorkersDB : RoomDatabase(){


    abstract fun workerDao():WorkerDao

    companion object {
        private var instance: WorkersDB? = null
        @Synchronized
        fun get(context: Context): WorkersDB {
            if (instance == null) {
                instance = Room
                        .databaseBuilder(context.applicationContext, WorkersDB::class.java, "worker")
                        .addMigrations()
                        .build()
            }
            return instance!!
        }
    }
}


