package com.uglive.gate.db.localDB

import android.arch.persistence.room.*
import com.uglive.gate.bean.Face

@Dao
interface FaceDao {
    @Insert
    fun insert(face: Face)

    @Insert
    fun insert(faces: List<Face>)

    @Delete
    fun delete(face: Face)

    @Query("delete from face where fileName = :name")
    fun deleteByName(name: String)

    @Update
    fun update(face: Face)

    @Query("select * from face")
    fun getAll(): List<Face>

    @Query("select fileName from face")
    fun getAllNames(): List<String>

    @Query(value = "delete from face")
    fun deleteAll()

    @Query(value = "select * from face where fileName = :filename")
    fun selectOneByFileName(filename: String): Face?

    @Query(value = "select * from face where ticketId = :ticketId")
    fun selectByTicketId(ticketId: String):List<Face>
}