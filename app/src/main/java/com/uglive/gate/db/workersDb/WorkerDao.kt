package com.uglive.gate.db.workersDb

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import com.uglive.gate.bean.Worker

@Dao
interface WorkerDao {

    @Query("select * from WORKER")
    fun getWorkerTickets(): List<Worker>

    @Query("select * from WORKER where id=:id")
    fun getWorkerById(id: String): Worker?

    @Update
    fun updateWorkerTickets(worker: Worker)

    @Query("update WORKER set FUTURE=NULL,IP=NULL")
    fun queryReset()
}