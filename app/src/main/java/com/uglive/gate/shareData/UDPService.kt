package com.uglive.gate.shareData

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.blankj.utilcode.util.NetworkUtils
import com.blankj.utilcode.util.NetworkUtils.getBroadcastIpAddress
import com.google.gson.Gson
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.bean.Face
import com.uglive.gate.db.localDB.FaceDao
import com.uglive.gate.db.localDB.LocalDB
import com.uglive.gate.db.workersDb.WorkerDao
import com.uglive.gate.db.workersDb.WorkersDB
import com.uglive.gate.model.SCAN_TYPE_RFID_ONLY
import com.uglive.gate.shareData.bean.SocketGetInfoMessage.TYPE_FACE_GET_FUTURE
import com.uglive.gate.shareData.bean.SocketGetInfoMessage.TYPE_GET_PHOTO
import com.uglive.gate.shareData.bean.TicketUsedShareMessage
import com.uglive.gate.ui.main.ValidateFace
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.nio.ByteBuffer
import kotlin.concurrent.thread


class UDPService : Service() {

    private lateinit var workerDao: WorkerDao
    private lateinit var faceDao: FaceDao

    private val MAX_DATA_SIZE = 1024 * 30
    private var isRunning = true
    private lateinit var serverSocket: DatagramSocket
    private lateinit var UDPThread: Thread


    override fun onBind(intent: Intent): IBinder {
        return Binder()
    }

    override fun onCreate() {
        workerDao  = WorkersDB.get(this).workerDao()
        faceDao  = LocalDB.get(this).faceDao()
        //接收同步信息
        UDPThread =  thread (name = "AndroidDeviceSyncUDPThread"){
            try {
                serverSocket = DatagramSocket(port)
                while (isRunning) {
                    val body = DatagramPacket(ByteArray(MAX_DATA_SIZE + 1024), MAX_DATA_SIZE + 1024)
                    serverSocket.receive(body)
                    parseData(body.data)
                }
                if(!serverSocket.isClosed){
                    serverSocket.close()
                }
            } catch (e: Exception) {
                e.printStackTrace(System.err)
            }
        }
    }

    private fun parseData(data: ByteArray){
        if (data.isEmpty()) return
        if (data[0] == MSG_TICKT_USED){
            //计算字节长度
            val lenBB =  ByteBuffer.allocate(4).put(data.copyOfRange(1,5))
            lenBB.flip()
            val length = lenBB.int
            val msgDataArray = ByteArray(length)
            System.arraycopy(data,5,msgDataArray,0,length)
            val ticketUsedShareMessage = gson.fromJson<TicketUsedShareMessage>(String(msgDataArray),TicketUsedShareMessage::class.java)
            //若是本机发送的广播就不处理
            if (ticketUsedShareMessage.ip == NetworkUtils.getIPAddress(true)) return
            //通过tcp获取特征信息
            val msgType = if (Config.scanType == SCAN_TYPE_RFID_ONLY) TYPE_GET_PHOTO else TYPE_FACE_GET_FUTURE
            PhotoSocketGetter.getFuture(msgType,ticketUsedShareMessage.ip,ticketUsedShareMessage.ticketId,ticketUsedShareMessage.fileName){
                it?.let { future ->
                    if (Config.scanType == SCAN_TYPE_RFID_ONLY) {
                        val worker = workerDao.getWorkerById(ticketUsedShareMessage.ticketId)
                        worker?.let {
                            it.ip = ticketUsedShareMessage.ip
                            it.future = future
                            workerDao.updateWorkerTickets(it)
                        }
                    }else{
                        val face = Face(fileName = ticketUsedShareMessage.fileName,ticketId = ticketUsedShareMessage.ticketId,
                                picSrcIp = ticketUsedShareMessage.ip,feature = future)
                        faceDao.insert(face)
                        ValidateFace.faces.add(face)
                    }
                }
            }
        }
        else if (data[0] == MSG_DELETE_TICKETFACE){
            //计算字节长度
            val lenBB =  ByteBuffer.allocate(4).put(data.copyOfRange(1,5))
            lenBB.flip()
            val length = lenBB.int
            val msgDataArray = ByteArray(length)
            System.arraycopy(data,5,msgDataArray,0,length)
            val ticketUsedShareMessage = gson.fromJson<TicketUsedShareMessage>(String(msgDataArray),TicketUsedShareMessage::class.java)
            ValidateFace.deleteFaceByRfid(ticketUsedShareMessage.ticketId,this)
        }
    }

    companion object {

        const val TAG = "UDPService"
        const val port = 22000
        //共享票据第一次使用的数据
        private val MSG_TICKT_USED = 0.toByte()
        private val MSG_DELETE_TICKETFACE = 1.toByte()
        private val gson = Gson()

        fun syncFuture(id: String = "",ip: String = "", filename: String= "", future: ByteArray) {
            val msgByteArray = gson.toJson(TicketUsedShareMessage(id,ip,filename)).toByteArray()
            // 数据长度是 一个 int 代表msgByteArray长度 + 一个 byte 代表数据类型 + msgByteArray长度
            val data = ByteArray(msgByteArray.size + 1 + 4)
            data[0] = MSG_TICKT_USED
            val lengthByteArray = ByteBuffer.allocate(4).putInt(msgByteArray.size).array()
            System.arraycopy(lengthByteArray,0,data,1,lengthByteArray.size)
            System.arraycopy(msgByteArray,0, data, 5, msgByteArray.size)
            with(DatagramSocket()) {
                AppExecutors.networkIO.execute {
                    send(DatagramPacket(
                            data,
                            data.size,
                            InetAddress.getByName(NetworkUtils.getBroadcastIpAddress()),
                            this@Companion.port)
                    )
                }
            }

        }

        fun deleteFace(ticketID: String) {
            val msgByteArray = Gson().toJson(TicketUsedShareMessage(ticketID, "", "")).toByteArray()
            // 数据长度是 一个 int 代表msgByteArray长度 + 一个 byte 代表数据类型 + msgByteArray长度
            val data = ByteArray(msgByteArray.size + 1 + 4)
            data[0] = MSG_DELETE_TICKETFACE
            val lengthByteArray = ByteBuffer.allocate(4).putInt(msgByteArray.size).array()
            System.arraycopy(lengthByteArray, 0, data, 1, lengthByteArray.size)
            System.arraycopy(msgByteArray, 0, data, 5, msgByteArray.size)
            try {
                DatagramSocket().send(DatagramPacket(
                        data,
                        data.size,
                        InetAddress.getByName(getBroadcastIpAddress()),
                        22000))
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

    }

    override fun onDestroy() {
        isRunning = false
        UDPThread.interrupt()
        serverSocket.close()
    }
}
