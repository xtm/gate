package com.uglive.gate.shareData.bean;

/**
 * 获取其他闸机上的信息
 */
public class SocketGetInfoMessage {


    private String msgType;
    private Integer id;
    private String ticketId;
    private String fileName;

    //获取人脸图片
    public static String TYPE_GET_PHOTO = "0";
    //获取人脸特征
    public static String TYPE_GET_FUTURE = "1";

    public static String TYPE_FACE_GET_FUTURE = "2";
    public static String TYPE_FACE_GET_PHOTO = "3";


    public SocketGetInfoMessage() {

    }

    public SocketGetInfoMessage(String msgType, Integer id, String ticketId, String fileName) {
        this.msgType = msgType;
        this.id = id;
        this.ticketId = ticketId;
        this.fileName = fileName;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
