package com.uglive.gate.shareData

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.google.gson.Gson
import com.uglive.gate.Config
import com.uglive.gate.db.localDB.FaceDao
import com.uglive.gate.db.localDB.LocalDB
import com.uglive.gate.db.workersDb.WorkerDao
import com.uglive.gate.db.workersDb.WorkersDB
import com.uglive.gate.shareData.bean.SocketGetInfoMessage
import java.io.*
import java.net.ServerSocket
import java.net.Socket
import java.nio.charset.Charset
import java.util.concurrent.Executors
import kotlin.concurrent.thread


class PhotoSocketService : Service() {
    private val TAG = "PhotoSocketService"
    private val port = 23000
    private val serverSocket = ServerSocket(port)
    private lateinit var socketThread: Thread
    private val executorService = Executors.newFixedThreadPool(1)
    private lateinit var workerDao: WorkerDao
    private lateinit var faceDao: FaceDao


    inner class ReadRunable(private val socket:Socket): Runnable{
        private val inputStream = DataInputStream(socket.getInputStream()!!)
        private val outputStream = DataOutputStream(socket.getOutputStream())
        override fun run() {
            try {
                val len = inputStream.readLong()
                val originalData = ByteArray(len.toInt())
                inputStream.read(originalData)
                val str = String(originalData, charset = Charset.forName("utf-8"))
                val photoMessage = Gson().fromJson<SocketGetInfoMessage>(str, SocketGetInfoMessage::class.java)
                when {
                    photoMessage.msgType == SocketGetInfoMessage.TYPE_GET_PHOTO -> {
                        val pictureFileName = "${photoMessage.ticketId}.png"
                        val pictureFile = File(Config.facePictureDir, pictureFileName)
                        val bufis = BufferedInputStream(FileInputStream(pictureFile))
                        val data = ByteArray(pictureFile.length().toInt())
                        bufis.read(data, 0, data.size)
                        outputStream.write(data, 0, data.size)
                        outputStream.flush()
                        bufis.close()
                    }
                    photoMessage.msgType == SocketGetInfoMessage.TYPE_GET_FUTURE -> workerDao.getWorkerById(photoMessage.ticketId)?.let {
                        outputStream.write(it.future)
                        outputStream.flush()
                    }
                    photoMessage.msgType == SocketGetInfoMessage.TYPE_FACE_GET_FUTURE -> {
                        //人脸图片的文件名不会重复
                        faceDao.selectOneByFileName(photoMessage.fileName)?.let {
                            outputStream.write(it.feature)
                            outputStream.flush()
                        }
                    }
                    photoMessage.msgType == SocketGetInfoMessage.TYPE_FACE_GET_PHOTO -> {
                        //人脸图片的文件名不会重复
                        val pictureFile = File(Config.faceSourceDir, photoMessage.fileName)
                        val bufis = BufferedInputStream(FileInputStream(pictureFile))
                        val data = ByteArray(pictureFile.length().toInt())
                        bufis.read(data, 0, data.size)
                        outputStream.write(data, 0, data.size)
                        outputStream.flush()
                        bufis.close()
                    }
                }
                socket.close()

            } catch (e: IOException) {
                Log.e("PhotoSocketService", "socket error",e)
                outputStream.close()
                inputStream.close()
                socket.close()
            }

        }

    }

    override fun onBind(intent: Intent?): IBinder {
        return Binder()
    }

    override fun onCreate() {
        workerDao = WorkersDB.get(this).workerDao()
        faceDao = LocalDB.get(this).faceDao()
        socketThread = thread (name = "photoSocketThread"){
            while (true) {
                val socket = serverSocket.accept()
                executorService.execute(ReadRunable(socket))
            }
        }
    }

    override fun onDestroy() {
        executorService.shutdownNow()
        serverSocket.close()
        socketThread.interrupt()
    }
}