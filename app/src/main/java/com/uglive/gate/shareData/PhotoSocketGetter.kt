package com.uglive.gate.shareData

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.annotation.WorkerThread
import android.util.Log
import com.blankj.utilcode.util.SDCardUtils
import com.google.gson.Gson
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.model.SCAN_TYPE_RFID_ONLY
import com.uglive.gate.shareData.bean.SocketGetInfoMessage
import java.io.DataOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

object PhotoSocketGetter {

    private val TAG = "PhotoSocketGetter"
    private val port = 23000
    private val photoPath = SDCardUtils.getSDCardPathByEnvironment()+ File.separator+"tmp"
    private val tmpPhotoFilePath = photoPath +File.separator+"tmp.png"
    init {
        File(photoPath).apply { if (!exists()) mkdirs() }
    }

    @WorkerThread
    fun getPhoto(type:String = "0",ip: String, m1: String,fileName: String="", OnResult:(String?) -> Unit){
        val filepath = if (Config.scanType == SCAN_TYPE_RFID_ONLY) tmpPhotoFilePath else File(Config.faceSourceDir,fileName).absolutePath
        //1000ms无法连接就取消连接
        AppExecutors.networkIO.execute {
            val socket = Socket()
            try {
                socket.connect(InetSocketAddress(ip, port),1000)
            }catch (e: IOException ){
                OnResult(null)
                try {
                    socket.close()
                }catch (e: IOException){
                    Log.e(TAG,"SocketIOException",e)
                }
            }
            if (socket.isConnected){
                val inputStream = socket.getInputStream()
                val outputStream = DataOutputStream(socket.getOutputStream())
                val msg = Gson().toJson(SocketGetInfoMessage(type,null, m1,fileName)).toByteArray()
                outputStream.writeLong(msg.size.toLong())
                outputStream.write(msg)
                outputStream.flush()
                val  data = inputStream.readBytes()
                BitmapFactory
                        .decodeByteArray(data,0,data.size)
                        .compress(Bitmap.CompressFormat.PNG, 80, FileOutputStream(filepath))
                OnResult(filepath)
                inputStream.close()
                outputStream.close()
                socket.close()
            } else{
                OnResult(null)
                socket.close()
            }

        }
    }

    @WorkerThread
    fun getFuture(type: String = "1",ip: String,m1: String,fileName:String, OnResult: (ByteArray?) -> Unit){
        //1000ms无法连接就取消连接
        AppExecutors.networkIO.execute {
            val socket = Socket()
            try {
                socket.connect(InetSocketAddress(ip, port),1000)
            }catch (e: IOException ){
                OnResult(null)
                try {
                    socket.close()
                }catch (e: IOException){
                    Log.e(TAG,"SocketIOException",e)
                }
            }
            if (socket.isConnected){
                val inputStream = socket.getInputStream()
                val outputStream = DataOutputStream(socket.getOutputStream())
                val msg = Gson().toJson(SocketGetInfoMessage(type,null, m1,fileName)).toByteArray()
                outputStream.writeLong(msg.size.toLong())
                outputStream.write(msg)
                outputStream.flush()
                OnResult(inputStream.readBytes())
                inputStream.close()
                outputStream.close()
                socket.close()
            } else{
                OnResult(null)
                socket.close()
            }

        }
    }
}