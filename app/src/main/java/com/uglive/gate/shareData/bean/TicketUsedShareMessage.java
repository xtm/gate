package com.uglive.gate.shareData.bean;

import java.util.Objects;

public class TicketUsedShareMessage {
    private String ticketId;
    private String ip;
    private String fileName;

    public TicketUsedShareMessage(String ticketId, String ip,String fileName) {
        this.ticketId = ticketId;
        this.ip = ip;
        this.fileName = fileName;
    }

    public TicketUsedShareMessage() {
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketUsedShareMessage that = (TicketUsedShareMessage) o;
        return Objects.equals(ticketId, that.ticketId) &&
                Objects.equals(ip, that.ip) &&
                Objects.equals(fileName, that.fileName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(ticketId, ip, fileName);
    }
}
