package com.uglive.gate

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.blankj.utilcode.util.ActivityUtils
import com.uglive.gate.ui.face.FaceLoadActivity
import com.uglive.gate.ui.main.MainActivity

/**
 * 监听系统启动广播，用于开机自启
 */
class StartAppReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if(!ActivityUtils.isActivityExistsInStack(MainActivity::class.java)){
            startApp(context)
        }
    }

   private fun startApp(context: Context?){
        val intent = Intent(context!!, FaceLoadActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

}