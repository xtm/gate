package com.uglive.gate

import com.blankj.utilcode.util.DeviceUtils
import com.blankj.utilcode.util.SDCardUtils
import com.blankj.utilcode.util.SPUtils
import com.github.kittinunf.fuel.core.FuelManager
import com.uglive.gate.model.SP_IP
import com.uglive.gate.model.SP_PUSH_STREAM
import java.io.File


/**
 * App全局变量信息
 */
object Config {

    val fileDir = File(SDCardUtils.getSDCardPathByEnvironment(), "images")

    init {
        AppExecutors.diskIO.execute {
            if (!fileDir.exists()) {
                fileDir.mkdirs()
            }
        }
    }

    var IP: String? = null
        set(value) {
            field = value
            FuelManager.instance.basePath = "http://$value"
        }

    var bugIP: String? = null

    /**
     * 扫描类型 0 只扫RFID，1扫RFID+二维码
     */
    var scanType: Int = -1
        @Synchronized set(value) {
            field = value
        }

    /**
     * M1密钥
     */
    var secretKey: String? = null

    var deviceName: String? = null

    var faceValidateType = -1

    val REQUEST_TIME_OUT = 1000


    /**
     * 活动ID，根据该ID获取票据信息和同步数据
     */
    var projectId = "-1"

    var isTest = false

    var faceDegree = 0.2f

    var faceScale = 8
    var faceSize = 2


    var uploadValidateInfo = false
    var gonganIp = "192.168.2.132"
    var cameraScale = 1f
    var compareIdcardFace = false
    var captureNeedFace = false
    var uploadChenduPolice = false
    var useKeyboardScanQrcodeDevice = false
    var needSecret = true

    //  是否通过芯片厂家加密
    var encByFactory = false
    //  芯片厂家加密的密码
    var constantSecret = "BC0123456AD7"
    //  一人一票固定读取秘钥
    var yrypConstantSecret = "110100"

    val ARC_FACE_APP_ID = "D5XFQLXfYWuQdLZt35BW86rXsyZsqjou5oXRMPNP3zPR"
    val ARC_FACE_SDK_KEY= "8LYh9imRYw7zVph1QhA2K9H6mvzseGjNUFfB9WW2oQho"
    val ARC_FACE_IDCard_ID = "D5XFQLXfYWuQdLZt35BW86rXsyZsqjou5oXRMPNP3zPR"
    val ARC_FACE_IDCard_KEY= "8LYh9imRYw7zVph1QhA2K9HM6jXAVeQewjdw2cmUwsPs"
    fun getRtmpUrl() = "rtmp://${SPUtils.getInstance().getString(SP_IP)}" +
            ":1935/view/${DeviceUtils.getAndroidID()}"

    fun isPushStream() = SPUtils.getInstance().getBoolean(SP_PUSH_STREAM)
    val IN_COUNT = "IN_COUNT"
    val facePictureDir = File(SDCardUtils.getSDCardPathByEnvironment() + File.separator + "faceImage")
    val advFileDir = File(SDCardUtils.getSDCardPathByEnvironment() + File.separator + "advImage")
    val faceSourceDir = File(SDCardUtils.getSDCardPathByEnvironment() + File.separator + "faceSrc")
    var cameraMirror = true
    var rectMirror = true
    var shuangliuIp = ""
}
