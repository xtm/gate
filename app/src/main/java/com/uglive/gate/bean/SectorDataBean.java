package com.uglive.gate.bean;

/**
 * Created by Dell on 2017/2/22.
 * 扇区信息
 */

public class SectorDataBean {

    public String pieceZero;
    public String pieceOne;
    public String pieceTwo;
    public String pieceThree;


    @Override
    public String toString() {
        return "SectorDataBean{" +
                "pieceZero='" + pieceZero + '\'' +
                ", pieceOne='" + pieceOne + '\'' +
                ", pieceTwo='" + pieceTwo + '\'' +
                ", pieceThree='" + pieceThree + '\'' +
                '}';
    }
}
