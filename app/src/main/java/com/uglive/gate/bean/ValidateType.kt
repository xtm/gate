package com.uglive.gate.bean

/**
 * 设置验证adapter使用
 */
class ValidateType(val type:Int, val text:String) {
    override fun toString(): String  = text
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ValidateType

        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        return type
    }

}