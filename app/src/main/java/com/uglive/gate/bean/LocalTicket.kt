package com.uglive.gate.bean

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "LocalTicket")
data class LocalTicket(@PrimaryKey val m1id: String)