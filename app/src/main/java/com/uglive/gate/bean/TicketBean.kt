package com.uglive.gate.bean

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader

/**
 * 票信息
 */
@Entity(tableName = "ticket")
data class TicketBean constructor(@PrimaryKey val soldId: String, /*二维码*/val soldCode: String?,/*芯片*/ val soldChip: String?,
                                  val soldPassID: String?, val soldTimeStart: String?, val soldTimeEnd: String?,
                                  var soldAvailableTimes: String, var soldUsedTimes: String, var statusDesc: Int,
                                  var soldUsed: Int, var imageFilePath: String?) {
    override fun equals(other: Any?) = this.soldId == (other as TicketBean).soldId
}