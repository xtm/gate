package com.uglive.gate.bean

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "WORKER")
data class Worker (@PrimaryKey val ID:String,val TYPE:Int,@ColumnInfo(typeAffinity = ColumnInfo.BLOB,name = "FUTURE") var future:ByteArray?,@ColumnInfo(name = "IP")var ip: String?){
    override fun equals(other: Any?): Boolean {
        return this.ID == (other as Worker).ID
    }

    override fun hashCode(): Int {
        var result = ID.hashCode()
        result = 31 * result + TYPE
        return result
    }
}