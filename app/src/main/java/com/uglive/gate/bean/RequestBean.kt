package com.uglive.gate.bean

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * 获取信息类型的请求
 */
const val REQUEST_TYPE_PULL_DATA = 0

/**
 * 上传信息的请求
 */
const val REQUEST_TYPE_UPLOAD_DATA = 1

/**
 * 获取人脸图片的请求
 */
const val REQUEST_TYPE_LOAD_FACE_IMAGE = 2

@Entity(tableName = "request")
data class RequestBean(@PrimaryKey(autoGenerate = true) val id: Int, val url: String,
                       val params: String, var filePath: String? = null, var requestType: Int) {
    @ColumnInfo(name = "req_success", typeAffinity = ColumnInfo.INTEGER)
    var reqSuccess: Boolean? = false
}