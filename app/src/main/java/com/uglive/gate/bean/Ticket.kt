package com.uglive.gate.bean
import com.decard.entitys.IDCard

data class Ticket(var qrCode: String?=null, var idCard: IDCard?=null, var m1: String?=null){
    override fun equals(other: Any?): Boolean {
        val oth = other as Ticket
        val flag1 = qrCode == oth.qrCode && m1 == oth.m1
        var flag2 = false
        if (idCard!=null&&oth.idCard!=null&& idCard!!.id== oth.idCard!!.id)
            flag2 = true
        if (idCard == null && oth.idCard == null)
            flag2 =true
        return flag1&&flag2
    }
    fun clearInfo(){
        qrCode = null
        idCard = null
        m1 = null
    }
}