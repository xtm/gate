package com.uglive.gate.bean;

import java.util.Objects;

public class CameraPreviewSize {
    private int width;
    private int height;

    public CameraPreviewSize() {
    }

    public CameraPreviewSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return  width + "*" + height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CameraPreviewSize that = (CameraPreviewSize) o;
        return width == that.width &&
                height == that.height;
    }

    @Override
    public int hashCode() {

        return Objects.hash(width, height);
    }
}
