package com.uglive.gate.bean

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "face")
data class Face(
        @PrimaryKey(autoGenerate = true)
        val id:Int = 0,
        var fileName:String,
        var ticketId:String,
        var picSrcIp:String = "0",
        val feature: ByteArray ) {


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Face

        if (id != other.id) return false
        if (fileName != other.fileName) return false
        if (ticketId != other.ticketId) return false
        if (picSrcIp != other.picSrcIp) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + fileName.hashCode()
        result = 31 * result + ticketId.hashCode()
        result = 31 * result + picSrcIp.hashCode()
        return result
    }

}