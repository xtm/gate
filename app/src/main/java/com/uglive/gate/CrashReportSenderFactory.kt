package com.uglive.gate

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import com.uglive.gate.model.SP_DEBUG_IP
import org.acra.config.CoreConfiguration
import org.acra.data.CrashReportData
import org.acra.data.StringFormat
import org.acra.sender.HttpSender
import org.acra.sender.ReportSender
import org.acra.sender.ReportSenderFactory


/**
 * 错误日志发送配置
 */
class CrashReportSenderFactory : ReportSenderFactory {
    override fun create(context: Context, config: CoreConfiguration): ReportSender {
        // 设置默认IP
        val sp = context.getSharedPreferences("spUtils", Context.MODE_PRIVATE)
        var ip = sp.getString(SP_DEBUG_IP, "47.98.49.11")
        val formUri = "http://${ip}:5984/acra-myapp/_design/acra-storage/_update/report"
        // 设置默认BUG地址IP
        val sender = object : HttpSender(config, HttpSender.Method.PUT, StringFormat.JSON,formUri) {
            override fun send(context: Context, report: CrashReportData) {
                //发送错误日志时重启App
                val intent = context.packageManager.getLaunchIntentForPackage(context.packageName)
                val restartIntent = PendingIntent.getActivity(context.applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
                val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, restartIntent)
                super.send(context, report)
            }
        }
        sender.setBasicAuth("uglive", "uglive123")
        return sender
    }
}