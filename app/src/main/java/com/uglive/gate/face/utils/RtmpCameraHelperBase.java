package com.uglive.gate.face.utils;

import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;

import com.pedro.encoder.audio.AudioEncoder;
import com.pedro.encoder.audio.GetAacData;
import com.pedro.encoder.input.audio.GetMicrophoneData;
import com.pedro.encoder.input.audio.MicrophoneManager;
import com.pedro.encoder.input.video.Frame;
import com.pedro.encoder.input.video.GetCameraData;
import com.pedro.encoder.utils.CodecUtil;
import com.pedro.encoder.video.FormatVideoEncoder;
import com.pedro.encoder.video.GetVideoData;
import com.pedro.encoder.video.VideoEncoder;

import net.ossrs.rtmp.ConnectCheckerRtmp;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

public abstract class RtmpCameraHelperBase implements Camera.PreviewCallback,
        GetAacData, GetCameraData, GetVideoData, GetMicrophoneData {
    private static final String TAG = "RtmpCameraHelperBase";
    private Camera mCamera;
    private int mCameraId;
    private DisplayMetrics metrics;
    private View previewView;
    private Camera.Size previewSize;
    private int specificPreviewWidth;
    private int specificPreviewHeight;
    private int displayOrientation = 0;
    private int rotation;
    private boolean isMirror = false;

    private Integer specificCameraId = null;
    private CameraListener cameraListener;

    //上次预览时间,根据这变量判断相机是否卡死
    private static long lastOnPreviewTimestamp;



    protected VideoEncoder videoEncoder;
    private MicrophoneManager microphoneManager;
    private AudioEncoder audioEncoder;
    private boolean streaming = false;
    private boolean videoEnabled = true;

    private boolean onPreview = false;
    private MediaFormat videoFormat;
    private MediaFormat audioFormat;


    protected RtmpCameraHelperBase(RtmpCameraHelperBase.Builder builder) {
        previewView = builder.previewDisplayView;
        specificCameraId = builder.specificCameraId;
        cameraListener = builder.cameraListener;
        rotation = builder.rotation;
        metrics = builder.metrics;
        specificPreviewWidth = builder.previewWidth;
        specificPreviewHeight = builder.previewHeight;
        if (builder.previewDisplayView instanceof TextureView) {
            isMirror = builder.isMirror;
        } else if (isMirror) {
            throw new RuntimeException("mirror is effective only when the preview is on a textureView");
        }
    }

    public void init() {
        if (previewView instanceof TextureView) {
            ((TextureView) this.previewView).setSurfaceTextureListener(textureListener);
        } else if (previewView instanceof SurfaceView) {
            ((SurfaceView) previewView).getHolder().addCallback(surfaceCallback);
        }
        if (isMirror) {
            previewView.setScaleX(-1);
        }
        videoEncoder = new VideoEncoder(this);
        microphoneManager = new MicrophoneManager(this);
        audioEncoder = new AudioEncoder(this);
    }

    public void start() {
        synchronized (this) {

            //相机数量为2则打开1,1则打开0,相机ID 1为前置，0为后置
            mCameraId = Camera.getNumberOfCameras() - 1;
            //若指定了相机ID且该相机存在，则打开指定的相机
            if (specificCameraId != null && specificCameraId <= mCameraId) {
                mCameraId = specificCameraId;
            }

            //没有相机
            if (mCameraId == -1) {
                if (cameraListener != null) {
                    cameraListener.onCameraError(new Exception("camera not found"));
                }
                return;
            }
            mCamera = Camera.open(mCameraId);
            displayOrientation = getCameraOri(rotation);
            mCamera.setDisplayOrientation(displayOrientation);

            try {
                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setPreviewFormat(ImageFormat.NV21);

                //预览大小设置
                previewSize = parameters.getPreviewSize();
                List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
                if (supportedPreviewSizes != null && supportedPreviewSizes.size() > 0) {
                    previewSize = getBestSupportedSize(supportedPreviewSizes, metrics);
                }
                parameters.setPreviewSize(previewSize.width, previewSize.height);

                //对焦模式设置
                List<String> supportedFocusModes = parameters.getSupportedFocusModes();
                if (supportedFocusModes != null && supportedFocusModes.size() > 0) {
                    if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    } else if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                    } else if (supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    }
                }
                mCamera.setParameters(parameters);
                if (previewView instanceof TextureView) {
                    mCamera.setPreviewTexture(((TextureView) previewView).getSurfaceTexture());
                } else {
                    mCamera.setPreviewDisplay(((SurfaceView) previewView).getHolder());
                }
                mCamera.setPreviewCallback(this);
                mCamera.startPreview();
                if (cameraListener != null) {
                    cameraListener.onCameraOpened(mCamera, mCameraId, displayOrientation, isMirror);
                }
            } catch (Exception e) {
                if (cameraListener != null) {
                    cameraListener.onCameraError(e);
                }
            }
            onPreview = true;
        }
    }

    private int getCameraOri(int rotation) {
        int degrees = rotation * 90;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
            default:
                break;
        }
        int result;
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(mCameraId, info);
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else {
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }

    public void stop() {
        synchronized (this) {
            if (mCamera == null) {
                return;
            }
            try {
                mCamera.setPreviewCallback(null);
                mCamera.setPreviewDisplay(null);
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
                if (cameraListener != null) {
                    cameraListener.onCameraClosed();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            onPreview = false;
        }
    }

    public boolean isStopped() {
        synchronized (this) {
            return mCamera == null;
        }
    }

    public void release() {
        stop();
        previewView = null;
        specificCameraId = null;
        cameraListener = null;
        metrics = null;
        previewSize = null;
        stopStream();
    }

    private Camera.Size getBestSupportedSize(List<Camera.Size> sizes, DisplayMetrics metrics) {
        Camera.Size bestSize = sizes.get(0);
        float screenRatio = (float) metrics.widthPixels / (float) metrics.heightPixels;
        if (screenRatio > 1) {
            screenRatio = 1 / screenRatio;
        }

        for (Camera.Size s : sizes) {
            if (specificPreviewWidth == s.width && specificPreviewHeight == s.height) {
                return s;
            }
            if (Math.abs((s.height / (float) s.width) - screenRatio) < Math.abs(bestSize.height / (float) bestSize.width - screenRatio)) {
                bestSize = s;
            }
        }
        return bestSize;
    }


    /**
     * Basic auth developed to work with Wowza. No tested with other server
     *
     * @param user auth.
     * @param password auth.
     */
    public abstract void setAuthorization(String user, String password);

    /**
     * Call this method before use @startStream. If not you will do a stream without video. NOTE:
     * Rotation with encoder is silence ignored in some devices.
     *
     * @param width resolution in px.
     * @param height resolution in px.
     * @param fps frames per second of the stream.
     * @param bitrate H264 in kb.
     * @param hardwareRotation true if you want rotate using encoder, false if you want rotate with
     * software if you are using a SurfaceView or TextureView or with OpenGl if you are using
     * OpenGlView.
     * @param rotation could be 90, 180, 270 or 0. You should use CameraHelper.getCameraOrientation
     * with SurfaceView or TextureView and 0 with OpenGlView or LightOpenGlView. NOTE: Rotation with
     * encoder is silence ignored in some devices.
     * @return true if success, false if you get a error (Normally because the encoder selected
     * doesn't support any configuration seated or your device hasn't a H264 encoder).
     */
    public boolean prepareVideo(int width, int height, int fps, int bitrate, boolean hardwareRotation,
                                int iFrameInterval, int rotation) {
//        if (onPreview) {
//            stopStream();
//            onPreview = true;
//        }
        FormatVideoEncoder formatVideoEncoder = FormatVideoEncoder.YUV420Dynamical ;
        return videoEncoder.prepareVideoEncoder(width, height, fps, bitrate, rotation, hardwareRotation,
                iFrameInterval, formatVideoEncoder);
    }

    /**
     * backward compatibility reason
     */
    public boolean prepareVideo(int width, int height, int fps, int bitrate, boolean hardwareRotation,
                                int rotation) {
        return prepareVideo(width, height, fps, bitrate, hardwareRotation, 2, rotation);
    }

    protected abstract void prepareAudioRtp(boolean isStereo, int sampleRate);

    /**
     * Call this method before use @startStream. If not you will do a stream without audio.
     *
     * @param bitrate AAC in kb.
     * @param sampleRate of audio in hz. Can be 8000, 16000, 22500, 32000, 44100.
     * @param isStereo true if you want Stereo audio (2 audio channels), false if you want Mono audio
     * (1 audio channel).
     * @param echoCanceler true enable echo canceler, false disable.
     * @param noiseSuppressor true enable noise suppressor, false  disable.
     * @return true if success, false if you get a error (Normally because the encoder selected
     * doesn't support any configuration seated or your device hasn't a AAC encoder).
     */
    public boolean prepareAudio(int bitrate, int sampleRate, boolean isStereo, boolean echoCanceler,
                                boolean noiseSuppressor) {
        microphoneManager.createMicrophone(sampleRate, isStereo, echoCanceler, noiseSuppressor);
        prepareAudioRtp(isStereo, sampleRate);
        return audioEncoder.prepareAudioEncoder(bitrate, sampleRate, isStereo);
    }

    /**
     * Same to call: rotation = 0; if (Portrait) rotation = 90; prepareVideo(640, 480, 30, 1200 *
     * 1024, false, rotation);
     *
     * @return true if success, false if you get a error (Normally because the encoder selected
     * doesn't support any configuration seated or your device hasn't a H264 encoder).
     */
    public boolean prepareVideo() {
        //todo 若预览有问题可以试试修改这里
        int rotation = displayOrientation;
        return prepareVideo(previewSize.width, previewSize.height, 30, 1200 * 1024, true, rotation);
    }

    /**
     * Same to call: prepareAudio(64 * 1024, 32000, true, false, false);
     *
     * @return true if success, false if you get a error (Normally because the encoder selected
     * doesn't support any configuration seated or your device hasn't a AAC encoder).
     */
    public boolean prepareAudio() {
        return prepareAudio(128 * 1024, 44100, true, false, false);
    }

    /**
     * @param forceVideo force type codec used. FIRST_COMPATIBLE_FOUND, SOFTWARE, HARDWARE
     * @param forceAudio force type codec used. FIRST_COMPATIBLE_FOUND, SOFTWARE, HARDWARE
     */
    public void setForce(CodecUtil.Force forceVideo, CodecUtil.Force forceAudio) {
        videoEncoder.setForce(forceVideo);
        audioEncoder.setForce(forceAudio);
    }



    protected abstract void startStreamRtp(String url);


    public void startStream(String url) {
        if (!streaming) {
            startEncoders();
            startStreamRtp(url);
        }
        streaming = true;
    }

    private void startEncoders() {
        videoEncoder.start();
        audioEncoder.start();
        microphoneManager.start();
    }



    protected abstract void stopStreamRtp();

    /**
     * Stop stream started with @startStream.
     */
    public void stopStream() {
        if (streaming) {
            streaming = false;
            stopStreamRtp();
        }
        try {
            if (microphoneManager.isRunning())
                microphoneManager.stop();
        } catch (Exception e) {
            Log.e("RtmpCameraHelperBase", "microphoneManager关闭失败", e);
        }
        videoEncoder.stop();
        audioEncoder.stop();
        videoFormat = null;
        audioFormat = null;

    }


    /**
     * Mute microphone, can be called before, while and after stream.
     */
    public void disableAudio() {
        microphoneManager.mute();
    }

    /**
     * Enable a muted microphone, can be called before, while and after stream.
     */
    public void enableAudio() {
        microphoneManager.unMute();
    }

    /**
     * Get mute state of microphone.
     *
     * @return true if muted, false if enabled
     */
    public boolean isAudioMuted() {
        return microphoneManager.isMuted();
    }

    /**
     * Get video camera state
     *
     * @return true if disabled, false if enabled
     */
    public boolean isVideoEnabled() {
        return videoEnabled;
    }

    /**
     * Disable send camera frames and send a black image with low bitrate(to reduce bandwith used)
     * instance it.
     */
    public void disableVideo() {
        videoEncoder.startSendBlackImage();
        videoEnabled = false;
    }

    /**
     * Enable send camera frames.
     */
    public void enableVideo() {
        videoEncoder.stopSendBlackImage();
        videoEnabled = true;
    }

    public int getBitrate() {
        return videoEncoder.getBitRate();
    }

    public int getResolutionValue() {
        return videoEncoder.getWidth() * videoEncoder.getHeight();
    }

    public int getStreamWidth() {
        return videoEncoder.getWidth();
    }

    public int getStreamHeight() {
        return videoEncoder.getHeight();
    }


    /**
     * Set video bitrate of H264 in kb while stream.
     *
     * @param bitrate H264 in kb.
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void setVideoBitrateOnFly(int bitrate) {
        videoEncoder.setVideoBitrateOnFly(bitrate);
    }

    /**
     * Set limit FPS while stream. This will be override when you call to prepareVideo method. This
     * could produce a change in iFrameInterval.
     *
     * @param fps frames per second
     */
    public void setLimitFPSOnFly(int fps) {
        videoEncoder.setFps(fps);
    }

    /**
     * Get stream state.
     *
     * @return true if streaming, false if not streaming.
     */
    public boolean isStreaming() {
        return streaming;
    }

    /**
     * Get preview state.
     *
     * @return true if enabled, false if disabled.
     */
    public boolean isOnPreview() {
        return onPreview;
    }


    @Override
    public void onPreviewFrame(byte[] nv21, Camera camera) {
        lastOnPreviewTimestamp = System.currentTimeMillis();
        if (cameraListener != null) {
            cameraListener.onPreview(nv21, camera);
            //todo 通过修改flip（isFrontCamera && isPortrait）来调整流的预览方向
            inputYUVData(new Frame(nv21, rotation, true, ImageFormat.NV21));
        }
    }

    private TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
            start();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
            Log.i(TAG, "onSurfaceTextureSizeChanged: " + width + "  " + height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            stop();
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

        }
    };
    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            start();
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            stop();
        }
    };

    public void changeDisplayOrientation(int rotation) {
        if (mCamera != null) {
            this.rotation = rotation;
            displayOrientation = getCameraOri(rotation);
            mCamera.setDisplayOrientation(displayOrientation);
            if (cameraListener != null) {
                cameraListener.onCameraConfigurationChanged(mCameraId, displayOrientation);
            }
        }
    }

    public void setWhiteBalance(String value) {
        if (mCamera != null) {
            Camera.Parameters parameters = mCamera.getParameters();
            if (parameters.getSupportedWhiteBalance().contains(value)) {
                parameters.setWhiteBalance(value);
                mCamera.setParameters(parameters);
            }
        }
    }

    public void setPreviwSize(int width, int height) {
        if (mCamera != null) {
            //避免重复设置
            if (previewSize.height == height && previewSize.width == width) return;
            specificPreviewWidth = width;
            specificPreviewHeight = height;
            release();
            start();
        }
    }

    public void setExposureCompensation(int value) {
        if (mCamera != null) {
            Camera.Parameters parameters = mCamera.getParameters();
            if (value >= parameters.getMinExposureCompensation() && value <= parameters.getMaxExposureCompensation()) {
                parameters.setExposureCompensation(value);
                mCamera.setParameters(parameters);
            }
        }
    }


    protected abstract void getAacDataRtp(ByteBuffer aacBuffer, MediaCodec.BufferInfo info);

    @Override
    public void getAacData(ByteBuffer aacBuffer, MediaCodec.BufferInfo info) {
        if (streaming) getAacDataRtp(aacBuffer, info);
    }

    protected abstract void onSpsPpsVpsRtp(ByteBuffer sps, ByteBuffer pps, ByteBuffer vps);

    @Override
    public void onSpsPps(ByteBuffer sps, ByteBuffer pps) {
        if (streaming) onSpsPpsVpsRtp(sps, pps, null);
    }

    @Override
    public void onSpsPpsVps(ByteBuffer sps, ByteBuffer pps, ByteBuffer vps) {
        if (streaming) onSpsPpsVpsRtp(sps, pps, vps);
    }

    protected abstract void getH264DataRtp(ByteBuffer h264Buffer, MediaCodec.BufferInfo info);

    @Override
    public void getVideoData(ByteBuffer h264Buffer, MediaCodec.BufferInfo info) {

        if (streaming) getH264DataRtp(h264Buffer, info);
    }

    @Override
    public void inputPCMData(byte[] buffer, int size) {
        audioEncoder.inputPCMData(buffer, size);
    }

    @Override
    public void inputYUVData(Frame frame) {
        videoEncoder.inputYUVData(frame);
    }

    @Override
    public void onVideoFormat(MediaFormat mediaFormat) {
        videoFormat = mediaFormat;
    }

    @Override
    public void onAudioFormat(MediaFormat mediaFormat) {
        audioFormat = mediaFormat;
    }

    public static final class Builder {

        /**
         * 预览显示的view，目前仅支持surfaceView和textureView
         */
        private View previewDisplayView;

        /**
         * 是否镜像显示，只支持textureView
         */
        private boolean isMirror;
        /**
         * 指定的相机ID
         */
        private Integer specificCameraId;
        /**
         * 事件回调
         */
        private CameraListener cameraListener;
        /**
         * 屏幕的长宽，在选择最佳相机比例时用到
         */
        private DisplayMetrics metrics;
        /**
         * 传入getWindowManager().getDefaultDisplay().getRotation()的值即可
         */
        private int rotation;
        /**
         * 指定的预览宽高，若系统支持则会以这个预览宽高进行预览
         */
        private int previewWidth;

        private int previewHeight;

        private ConnectCheckerRtmp connectChecker;

        public Builder(ConnectCheckerRtmp connectChecker) {
            this.connectChecker = connectChecker;
        }


        public RtmpCameraHelperBase.Builder previewOn(View val) {
            if (val instanceof SurfaceView || val instanceof TextureView) {
                previewDisplayView = val;
                return this;
            } else {
                throw new RuntimeException("you must preview on a textureView or a surfaceView");
            }
        }


        public RtmpCameraHelperBase.Builder isMirror(boolean val) {
            isMirror = val;
            return this;
        }

        public RtmpCameraHelperBase.Builder previewWidth(int val) {
            previewWidth = val;
            return this;
        }

        public RtmpCameraHelperBase.Builder previewHeight(int val) {
            previewHeight = val;
            return this;
        }

        public RtmpCameraHelperBase.Builder metrics(DisplayMetrics val) {
            metrics = val;
            return this;
        }

        public RtmpCameraHelperBase.Builder rotation(int val) {
            rotation = val;
            return this;
        }

        public RtmpCameraHelperBase.Builder specificCameraId(Integer val) {
            specificCameraId = val;
            return this;
        }

        public RtmpCameraHelperBase.Builder cameraListener(CameraListener val) {
            cameraListener = val;
            return this;
        }

        public RtmpHelperCamera1 build() {
            return new RtmpHelperCamera1(this,connectChecker);
        }

    }

    /**
     * 相机是否卡死（一般是由于过热），根据当时间与上次时间差是否大于3000ms判定
     */
    public static boolean cameraIsStuck() {
        return System.currentTimeMillis() - lastOnPreviewTimestamp > 3000L;
    }
}
