package com.uglive.gate.face

import android.graphics.Bitmap

interface OnCaputureFace {
    fun setOnCaptureFace(onCapture:(Bitmap?)->Unit)
}