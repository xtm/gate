package com.uglive.gate.face

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Rect
import android.hardware.Camera
import android.support.annotation.MainThread
import android.support.annotation.WorkerThread
import android.widget.FrameLayout
import com.arcsoft.face.FaceFeature
import com.arcsoft.face.FaceInfo
import com.arcsoft.face.FaceSimilar
import com.blankj.utilcode.util.SPUtils
import com.uglive.gate.Config
import com.uglive.gate.face.domain.FacePosition
import com.uglive.gate.face.view.ArcFaceStreamCameraView


class FaceHelper {


    private var arcArcFace: ArcFaceStreamCameraView
    val pwidth =640
    val pheight = 480
    //        val pwidth = SPUtils.getInstance().getInt("cameraPreviewSizeWidth", 1920)
//        val pheight = SPUtils.getInstance().getInt("cameraPreviewSizeHeight", 1080)
//        val pwidth = 1920
//        val pheight = 1080
    @MainThread constructor(context: Context,container: FrameLayout, cameraMirror:Boolean, rectMirror:Boolean, initSuccess: (FaceHelper) -> Unit,
                            cameraSetSuccess:(paramters:Camera.Parameters) -> Unit) {
        val exposureCompensation = SPUtils.getInstance().getInt("exposureCompensation", 3) - 3
        val position = SPUtils.getInstance().getInt("whiteBalance", 0)
        var whiteBalance = "auto"
        when (position) {
            0 -> whiteBalance = "auto"
            1 -> whiteBalance = "incandescent"
            2 -> whiteBalance = "fluorescent"
            3 -> whiteBalance = "daylight"
            4 -> whiteBalance = "cloudy-daylight"
        }

        arcArcFace = ArcFaceStreamCameraView(context, validScope = Config.cameraScale, score = 1 - Config.faceDegree,
                previewWidth = pwidth, previewHeight = pheight, showCurrentFace = true,
                cameraMirror = cameraMirror, rectMirror = rectMirror)
        arcArcFace.onCameraSetSuccess = object : (Camera.Parameters) -> Unit {
            override fun invoke(paramters: Camera.Parameters) {
                cameraSetSuccess(paramters)
                //设置白平衡和曝光补偿
                setExposureCompensation(exposureCompensation)
                setWhiteBalance(whiteBalance)
            }
        }
        container.addView(arcArcFace, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        arcArcFace.initEngine()
        initSuccess(this@FaceHelper)
    }



    // 设置相机预览分辨率
    fun setPreviewSize(width: Int, height: Int) = arcArcFace.setPreviewSize(width, height)

    /**
     * @return <图像，特征>
     */
    @WorkerThread fun captureFace(): Triple<Bitmap, ByteArray,FacePosition>? {
        arcArcFace.captureFace()?.let {
            return Triple(it.first, it.second,it.third)
        }
        return null
    }

    /**
     * @return 对比成功时截取的人脸图，失败为空
     */
    @WorkerThread fun compareFace(bitmap: Bitmap): Bitmap? {
        val result = arcArcFace.compareFace(faceFuture = null,srcBitmap = bitmap)
        return result?.first
    }

    fun cropBitmap(data: ByteArray, item: Rect) = arcArcFace.cropBitmap(data,item,pwidth,pheight)

    @WorkerThread fun compareFutureData(futureData: List<ByteArray>) =  arcArcFace.compareFace(futureData,null)


    @WorkerThread fun compareIDCard(data:ByteArray): Bitmap? {
        return arcArcFace.compareIDCard1(data)?.first
    }

    @WorkerThread fun captureFrame():Bitmap = arcArcFace.captureFrame()
    /**
     * 设置曝光度
     * @param value 曝光值 -3 - 3
     */
    fun setExposureCompensation(value: Int) {
        arcArcFace.setExposureCompensation(value)
    }

    fun setOnCaptureFace(onCapture:(Bitmap?)->Unit){
        arcArcFace.let { it.setOnCaptureFace(onCapture) }
    }
    fun getFrameData() = arcArcFace.getFrameData()
    fun compareFeature(feature: FaceFeature, feature2: FaceFeature, faceSimilar: FaceSimilar) = arcArcFace.compareFeature(feature, feature2, faceSimilar)
    fun extractFaceFeature(data: ByteArray?, format: Int, faceInfo: FaceInfo?, feature: FaceFeature?) = arcArcFace.extractFaceFeature(data,format,faceInfo,feature)

    /**
     * 设置相机白平衡
     */
    fun setWhiteBalance(value: String) {
        arcArcFace.setWhiteBalance(value)
    }


    fun setWhiteBalance(position: Int){
        when (position) {
            0 -> setWhiteBalance("auto")
            1 -> setWhiteBalance("incandescent")
            2 -> setWhiteBalance("fluorescent")
            3 -> setWhiteBalance("daylight")
            4 -> setWhiteBalance("cloudy-daylight")
        }
    }

    fun close() {
        arcArcFace.close()
    }

    fun startStream(){
        arcArcFace.startStream()
    }

    fun stopStream(){
        arcArcFace.stopStream()
    }
}
