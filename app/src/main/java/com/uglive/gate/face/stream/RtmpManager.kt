package com.uglive.gate.face.stream

import com.uglive.gate.face.utils.RtmpHelperCamera1

class RtmpManager(val rtmpHelperCamera1: RtmpHelperCamera1) {

    fun startPushStream(url: String){
        if (rtmpHelperCamera1.prepareAudio()&&rtmpHelperCamera1.prepareVideo())
            rtmpHelperCamera1.startStream(url)
    }

    fun stopPushStream(){
        rtmpHelperCamera1.stopStream()
    }
}