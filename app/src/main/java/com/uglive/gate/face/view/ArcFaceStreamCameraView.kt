package com.uglive.gate.face.view

import android.content.Context
import android.graphics.*
import android.hardware.Camera
import android.os.Handler
import android.support.annotation.WorkerThread
import android.util.AttributeSet
import android.util.Log
import android.widget.RelativeLayout
import android.widget.Toast
import com.arcsoft.face.*
import com.blankj.utilcode.util.ConvertUtils
import com.blankj.utilcode.util.ToastUtils
import com.blankj.utilcode.util.Utils
import com.guo.android_extend.java.ExtByteArrayOutputStream
import com.uglive.gate.App
import com.uglive.gate.Config
import com.uglive.gate.face.OnCaputureFace
import com.uglive.gate.face.domain.DrawInfo
import com.uglive.gate.face.domain.FacePosition
import com.uglive.gate.face.utils.DrawHelper
import com.uglive.gate.face.utils.ImageUtil
import com.uglive.gate.model.SCAN_TYPE_IDCARD
import com.uglive.gate.model.SCAN_TYPE_IDCARD_AND_QRCODE
import com.uglive.gate.model.SCAN_TYPE_IDCARD_OR_RFID
import com.uglive.gate.model.SCAN_TYPE_IDCARD_OR_RFID_HIGH
import com.uglive.gate.widget.SquareScanView
import me.lake.librestreaming.client.CamareListener
import me.lake.librestreaming.core.listener.RESConnectionListener
import me.lake.librestreaming.ws.StreamAVOption
import me.lake.librestreaming.ws.StreamLiveCameraView
import java.util.concurrent.*
import java.util.concurrent.locks.ReentrantLock

/**
 * 带人脸识别的相机
 */
class ArcFaceStreamCameraView : RelativeLayout, OnCaputureFace {


    override fun setOnCaptureFace(onCapture: (Bitmap?) -> Unit) {
        this.onCapture = onCapture
    }

    private var onCapture: ((Bitmap?) -> Unit)? = null
    private val TAG = "ArcFaceCameraView"

    private val lock = ReentrantLock()

    //图片预览质量
    private var previewHeight = 720
    private var previewWidth = 1280

    //相机旋转度
    private var rotate = 0

    //是否显示截取的人脸图片
    private var showCurrentFace = false


    //截取的人脸图片视图
//    private val showFaceImageView: ImageView = ImageView(context)

    //范围限制
    private val mSquareView = SquareScanView(context, 1f)

    private val cameraView = StreamLiveCameraView(context)

    //人脸框的显示
    private var faceRectView: FaceRectView = FaceRectView(context)
    private var drawHelper: DrawHelper? = null


    //人脸跟踪引擎
    private lateinit var faceEngine: FaceEngine

    private var idFaceEngine: FaceEngine? = null


    //若设备处理太慢，过滤当前帧
    private lateinit var arcFaceQueue: LinkedBlockingDeque<Pair<ByteArray, List<FaceInfo>>>
    private lateinit var arcFaceRectQueue: LinkedBlockingDeque<Pair<ByteArray, Rect>>
    private lateinit var arcIdVariResultQueue: LinkedBlockingQueue<Boolean>


    //人脸有效范围
    private var validLeft = 0
    private var validTop = 0
    private var validBottom = previewHeight
    private var validRight = previewWidth
    //扫描人脸有效范围
    private var validScope = 1f

    private var score = 0.6f

    // 捕获人脸时间，
    private var captureFaceTime = 3000L


    private var searchThread: Thread? = null
    //用于获取人脸特征提取成功的帧
    private var singleExecutors = Executors.newSingleThreadExecutor()

    //截取的图片是否需要人脸
    @Volatile
    private var captureNeedFace = false

    var onCameraSetSuccess: ((paramters: Camera.Parameters) -> Unit)? = null

    var cameraMirror = true
    var rectMirror = true

    @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, def: Int = 0, validScope: Float = 0.7f,
                              score: Float = 0.6f, previewWidth: Int, previewHeight: Int, showCurrentFace: Boolean,
                              cameraMirror: Boolean, rectMirror: Boolean) : super(context, attrs, def) {
        this.cameraMirror = cameraMirror
        this.rectMirror = rectMirror
        this.validScope = validScope
        this.score = score
        mSquareView.n = validScope
        this.showCurrentFace = showCurrentFace
        this.previewWidth = previewWidth
        this.previewHeight = previewHeight
//        val afilp = RelativeLayout.LayoutParams(180, 180)
//        afilp.setMargins(12, 12, 12, 12)
//        addView(showFaceImageView, afilp)
        addView(cameraView, RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT))
        addView(faceRectView, RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT))
        addView(mSquareView, RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT))
    }

    //初始化人脸识别引擎
    fun initEngine() {
        arcFaceQueue = LinkedBlockingDeque(1)
        arcFaceRectQueue = LinkedBlockingDeque(1)
        arcIdVariResultQueue = LinkedBlockingQueue(1)
        faceEngine = FaceEngine()
        var afCode = faceEngine.active(context, Config.ARC_FACE_APP_ID, Config.ARC_FACE_SDK_KEY)

        if (afCode == ErrorInfo.MOK || afCode == ErrorInfo.MERR_ASF_ALREADY_ACTIVATED) {
            afCode = faceEngine.init(context.applicationContext, FaceEngine.ASF_DETECT_MODE_VIDEO, FaceEngine.ASF_OP_0_ONLY,
                    Config.faceScale, Config.faceSize,
                    FaceEngine.ASF_FACE_DETECT or FaceEngine.ASF_AGE or FaceEngine.ASF_GENDER or FaceEngine.ASF_FACE_RECOGNITION)
            val versionInfo = VersionInfo()
            faceEngine.getVersion(versionInfo)
            Log.i(TAG, "Face initEngine:  init: $afCode  version:$versionInfo")
            if (afCode != ErrorInfo.MOK) {
                ToastUtils.showShort("人脸引擎初始化失败", Toast.LENGTH_SHORT)
            } else {
                initCamera(context)
            }
        } else {
            ToastUtils.showShort("人脸引擎注册失败,注意：第一次注册需要联网", Toast.LENGTH_SHORT)
        }
        if (listOf(SCAN_TYPE_IDCARD_AND_QRCODE,
                        SCAN_TYPE_IDCARD,
                        SCAN_TYPE_IDCARD_OR_RFID_HIGH,
                        SCAN_TYPE_IDCARD_OR_RFID).contains(Config.scanType)) {
            captureFaceTime = 1000L
            idFaceEngine = FaceEngine()
            var afCode = idFaceEngine?.active(context.applicationContext, Config.ARC_FACE_APP_ID, Config.ARC_FACE_SDK_KEY)
            if (afCode == ErrorInfo.MOK || afCode == ErrorInfo.MERR_ASF_ALREADY_ACTIVATED) {
                afCode = idFaceEngine?.init(context.applicationContext, FaceEngine.ASF_DETECT_MODE_IMAGE, FaceEngine.ASF_OP_0_ONLY,
                        16, 5,
                        FaceEngine.ASF_FACE_DETECT or FaceEngine.ASF_FACE_RECOGNITION)
                val versionInfo = VersionInfo()
                idFaceEngine?.getVersion(versionInfo)
                Log.i(TAG, "FACE Engine:  init rltCode: $afCode  version:$versionInfo")
                if (afCode != ErrorInfo.MOK) {
                    ToastUtils.showShort("人脸引擎初始化失败", Toast.LENGTH_SHORT)
                }
            } else {
                ToastUtils.showShort("人脸引擎注册失败,注意：第一次注册需要联网", Toast.LENGTH_SHORT)
            }
        }

    }

    private fun initCamera(context: Context) {
        val cameraListener = object : CamareListener {

            override fun onCameraOpen(camera: Camera, displayOrientation: Int, isMirror: Boolean, cameraId: Int) {
                Log.i(TAG, "onCameraOpened: $cameraId  $displayOrientation $isMirror")
                previewWidth = camera.parameters.previewSize.width
                previewHeight = camera.parameters.previewSize.height
                onCameraSetSuccess?.let { it(camera.parameters) }
                setValidateRect()
                postDelayed({
                    drawHelper = DrawHelper(previewWidth, previewHeight, cameraView.width, cameraView.height,
                            displayOrientation, cameraId, rectMirror)
                    startStream()
                }, 2000)
            }


            override fun onPreviewFrame(data: ByteArray, camera: Camera) {
                (Utils.getApp() as App).lastOnPreviewTimestamp = System.currentTimeMillis()
                val copyData = ByteArray(data.size)
                System.arraycopy(data, 0, copyData, 0, data.size)
                faceRectView.clearFaceInfo()
                var faceInfoList: List<FaceInfo> = arrayListOf()
                if (lock.isLocked) return
                var code = faceEngine.detectFaces(data, previewWidth, previewHeight, FaceEngine.CP_PAF_NV21, faceInfoList)
                if (code == ErrorInfo.MOK) {
                    faceInfoList = faceInfoList.filter { it.rect.left >= validLeft && it.rect.right <= validRight && it.rect.top >= validTop && it.rect.bottom <= validBottom }
                    faceInfoList = faceInfoList.sortedByDescending { (it.rect.right - it.rect.left) * (it.rect.bottom - it.rect.top) }
                    if (faceInfoList.isNotEmpty()) {
                        code = faceEngine.process(data, previewWidth, previewHeight, FaceEngine.CP_PAF_NV21, faceInfoList, FaceEngine.ASF_AGE or FaceEngine.ASF_GENDER)
                        if (code == ErrorInfo.MOK) {
                            arcFaceQueue.offer(Pair(copyData, faceInfoList), 0, TimeUnit.MILLISECONDS)
                            drawHelper?.let {
                                val drawInfoList = arrayListOf<DrawInfo>()
                                for (i in faceInfoList.indices) {
                                    drawInfoList.add(DrawInfo(faceInfoList[i].rect, 0, 0, 0, null))
                                }
                                it.draw(faceRectView, drawInfoList)
                            }
                            //在左上角显示当前截取的人脸图片
                            if (showCurrentFace) {
                                val yuv = YuvImage(data, ImageFormat.NV21, previewWidth, previewHeight, null)
                                val ops = ExtByteArrayOutputStream()
                                yuv.compressToJpeg(faceInfoList.first().rect, 60, ops)
                                val bitmap = BitmapFactory.decodeByteArray(ops.byteArray, 0, ops.byteArray.size)
                                val matrix = Matrix()
                                //调整角度
                                matrix.setRotate(rotate.toFloat())
                                //水平翻转
                                matrix.postScale(-1f, 1f)
                                onCapture?.let { it(Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, false)) }
//                                    showFaceImageView.setImageBitmap(Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, false))
                            }
                            return
                            {
                                //不去识别年龄之类的可减少开销
//                                val ageInfoList = java.util.ArrayList<AgeInfo>()
//                                val genderInfoList = java.util.ArrayList<GenderInfo>()
//                                val ageCode = faceGetAgeMethod.invoke(faceEngine, ageInfoList) as Int
//                                val genderCode = faceGetGenderMethod.invoke(faceEngine, genderInfoList) as Int
//                                //追踪到人脸切且能识别该人脸的年龄和性别时再提取特征增加特征可用度
//                                if (ageCode or genderCode == ErrorInfo.MOK) {
//                                    arcFaceQueue.offer(Pair(copyData, faceInfoList), 0, TimeUnit.MILLISECONDS)
//                                    drawHelper?.let {
//                                        val drawInfoList = arrayListOf<DrawInfo>()
//                                        for (i in faceInfoList.indices) {
//                                            drawInfoList.add(DrawInfo(faceInfoList[i].rect, 0, 0, 0, null))
//                                        }
//                                        it.draw(faceRectView, drawInfoList)
//                                    }
//                                    //在左上角显示当前截取的人脸图片
//                                    if (showCurrentFace) {
//                                        val yuv = YuvImage(data, ImageFormat.NV21, previewWidth, previewHeight, null)
//                                        val ops = ExtByteArrayOutputStream()
//                                        yuv.compressToJpeg(faceInfoList.first().rect, 60, ops)
//                                        val bitmap = BitmapFactory.decodeByteArray(ops.byteArray, 0, ops.byteArray.size)
//                                        val matrix = Matrix()
//                                        //调整角度
//                                        matrix.setRotate(rotate.toFloat())
//                                        //水平翻转
//                                        matrix.postScale(-1f, 1f)
//                                        onCapture?.let { it(Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, false)) }
////                                    showFaceImageView.setImageBitmap(Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, false))
//                                    }
//                                    return
//                                }
                            }
                        }
                    }
                }

                //如果截取的帧不需要要人脸，将所有帧都放入队列中
                if (!captureNeedFace) {
                    arcFaceQueue.offer(Pair(copyData, listOf()), 0, TimeUnit.MILLISECONDS)
                }
                onCapture?.let { it(null) }
//                showFaceImageView.setImageBitmap(null)
            }

        }

        val streamAVOption = StreamAVOption()
        streamAVOption.rotation = rotate / 90
        streamAVOption.isMirror = cameraMirror
        streamAVOption.previewHeight = previewHeight
        streamAVOption.previewWidth = previewWidth
        cameraView.addStreamStateListener(object : RESConnectionListener {
            override fun onOpenConnectionResult(result: Int) {
                Log.i(TAG, "RESConnection->onOpenConnectionResult")
            }

            override fun onWriteError(errno: Int) {
                Log.i(TAG, "RESConnection->onWriteError")
            }

            override fun onCloseConnectionResult(result: Int) {
                Handler().postDelayed({ startStream() }, 3000)
            }
        })
        cameraView.init(context, streamAVOption, cameraListener)
    }


    //   通过特征提取的人脸
    @WorkerThread
    @Synchronized
    fun captureFace(): Triple<Bitmap, ByteArray, FacePosition>? {
        captureNeedFace = true
        var task: Future<Triple<FaceFeature, Rect, ByteArray>?>? = null
        try {
            task = singleExecutors.submit(Callable<Triple<FaceFeature, Rect, ByteArray>?> {
                //去掉队列里过时的帧
                arcFaceQueue.take()
                while (true) {
                    val (data, aftFaceList) = arcFaceQueue.take()
                    val faceFeature = FaceFeature()
                    val resultCode = extractFaceFeature(data, previewWidth, previewHeight, FaceEngine.CP_PAF_NV21, aftFaceList.first(), faceFeature)
                    if (resultCode == ErrorInfo.MOK) {
                        return@Callable Triple(faceFeature, aftFaceList.first().rect, data)
                    }
                }
                return@Callable null
            })
            val encodeResult = task.get(captureFaceTime, TimeUnit.MILLISECONDS)
            encodeResult?.let {
                val (cropBitmap, facePositon) = cropBitmap(it.third, it.second, previewWidth, previewHeight)
                return Triple(cropBitmap, it.first.featureData, facePositon)
            }
        } catch (e: TimeoutException) {
        } catch (e: ExecutionException) {
        } finally {
            task?.cancel(true)
        }

        return null
    }


    fun getFrameData() = arcFaceQueue.take()
    fun compareFeature(feature: FaceFeature, feature2: FaceFeature, faceSimilar: FaceSimilar) = faceEngine!!.compareFaceFeature(feature, feature2, faceSimilar)
    private fun extractFaceFeature(data: ByteArray?, width: Int, height: Int, format: Int, faceInfo: FaceInfo?, feature: FaceFeature?) = faceEngine!!.extractFaceFeature(data, width, height, format, faceInfo, feature)
    fun extractFaceFeature(data: ByteArray?, format: Int, faceInfo: FaceInfo?, feature: FaceFeature?) = faceEngine!!.extractFaceFeature(data, previewWidth, previewHeight, format, faceInfo, feature)

    @WorkerThread
    fun compareFace(faceFuture: List<ByteArray>?, srcBitmap: Bitmap?): Triple<Bitmap, Int, FacePosition>? {
        if (faceFuture == null && srcBitmap == null) throw NullPointerException(" faceFuture 和 bitmap 不能同时为空 ")
        captureNeedFace = true
        val features = mutableListOf<FaceFeature>()
        if (faceFuture == null) {
            val faceFeature1 = FaceFeature()
            val bitmap = resizeBitmap(srcBitmap!!)
            val data = ImageUtil.bitmapToNv21(bitmap, bitmap.width, bitmap.height)
            val faceList = arrayListOf<FaceInfo>()
            val detectRltCode = faceEngine!!.detectFaces(data, bitmap.width, bitmap.height, FaceEngine.CP_PAF_NV21, faceList)
            if (detectRltCode != ErrorInfo.MOK) {
                post { ToastUtils.showShort("无法识别的图片") }
                return null
            }
            val resultCode = faceEngine!!.extractFaceFeature(data, previewWidth, previewHeight, FaceEngine.CP_PAF_NV21, faceList.first(), faceFeature1)
            if (resultCode != ErrorInfo.MOK) {
                post { ToastUtils.showShort("无法识别的图片") }
                return null
            }
            features.add(faceFeature1)
        } else {
            faceFuture.forEach { features.add(FaceFeature(it)) }
        }
        var task: Future<Triple<ByteArray, Rect, Int>?>? = null
        try {
            task = singleExecutors.submit(Callable<Triple<ByteArray, Rect, Int>?> {
                //去掉队列里过时的帧
                arcFaceQueue.take()
                while (true) {
                    val (data, aftFaceList) = arcFaceQueue.take()
                    val faceFeature2 = FaceFeature()
                    val resultCode = extractFaceFeature(data, previewWidth, previewHeight,
                            FaceEngine.CP_PAF_NV21, aftFaceList.first(), faceFeature2)
                    if (resultCode == ErrorInfo.MOK) {
                        val faceSimilar = FaceSimilar()
                        for ((index, feature) in features.withIndex()) {
                            val similarResultCode = faceEngine!!.compareFaceFeature(feature, faceFeature2, faceSimilar)
                            if (similarResultCode == ErrorInfo.MOK && faceSimilar.score > score) {
                                return@Callable Triple(data, aftFaceList.first().rect, index)
                            }
                        }
                    }
                }
                return@Callable null
            })
            task.get(captureFaceTime, TimeUnit.MILLISECONDS)?.let {
                val (bitmap, position) = cropBitmap(it.first, it.second, previewWidth, previewHeight)
                return@compareFace Triple(bitmap, it.third, position)
            }
        } catch (e: Exception) {
            //超时返回识别失败
            Log.i(TAG, "对比人脸超时")
        } finally {
            task?.cancel(true)
        }
        return null
    }

    @WorkerThread
    fun compareIDCard1(dataSrc: ByteArray): Pair<Bitmap, FacePosition>? {
        val bitmapSrc = ConvertUtils.bytes2Bitmap(dataSrc)
        val resizeSrc = resizeBitmap(bitmapSrc)
        val feature = getIdFeature(resizeSrc)
        var task: Future<Pair<ByteArray, Rect>?>? = null
        try {
            task = singleExecutors.submit(Callable<Pair<ByteArray, Rect>?> {

                //去掉队列里过时的帧
                arcFaceQueue.take()
                while (true) {
                    val (data, aftFaceList) = arcFaceQueue.take()
                    val faceFeature2 = FaceFeature()
                    val resultCode = extractFaceFeature(data, previewWidth, previewHeight,
                            FaceEngine.CP_PAF_NV21, aftFaceList.first(), faceFeature2)
                    if (resultCode == ErrorInfo.MOK) {
                        val faceSimilar = FaceSimilar()
                        val similarResultCode = idFaceEngine!!.compareFaceFeature(feature, faceFeature2, faceSimilar)
                        if (similarResultCode == ErrorInfo.MOK && faceSimilar.score > score) {
                            return@Callable Pair(data, aftFaceList.first().rect)
                        }
                    }
                }
                return@Callable null
            })
            task.get(captureFaceTime, TimeUnit.MILLISECONDS)?.let {
                val (bitmap, position) = cropBitmap(it.first, it.second, previewWidth, previewHeight)
                return@compareIDCard1 Pair(bitmap, position)
            }
        } catch (e: Exception) {
            //超时返回识别失败
            Log.i(TAG, "对比人脸超时")
        } finally {
            task?.cancel(true)
        }
        return null
    }


    @WorkerThread
    private fun getIdFeature(bitmap: Bitmap): FaceFeature {
        val faceFeature1 = FaceFeature()
        val data = ImageUtil.bitmapToNv21(bitmap, bitmap.width, bitmap.height)
        val faceList = arrayListOf<FaceInfo>()
        val detectRltCode = idFaceEngine!!.detectFaces(data, bitmap.width, bitmap.height, FaceEngine.CP_PAF_NV21, faceList)
        if (detectRltCode != ErrorInfo.MOK || faceList.isEmpty()) {
        }
        val resultCode = idFaceEngine!!.extractFaceFeature(data, bitmap.width, bitmap.height, FaceEngine.CP_PAF_NV21, faceList.first(), faceFeature1)
        if (resultCode != ErrorInfo.MOK) {
        }
        return faceFeature1
    }




    //获取一帧，不需要人脸
    @WorkerThread
    fun captureFrame(): Bitmap {
        //不需要人脸
        captureNeedFace = false
        arcFaceQueue.take()
        val data = arcFaceQueue.take().first
        val yuv = YuvImage(data, ImageFormat.NV21, previewWidth, previewHeight, null)
        val ops = ExtByteArrayOutputStream()
        yuv.compressToJpeg(Rect(0, 0, previewWidth, previewHeight), 80, ops)
        return BitmapFactory.decodeByteArray(ops.byteArray, 0, ops.byteArray.size)
    }


    //计算相机中人脸有效范围
    private fun setValidateRect() {
        val width = previewWidth
        val height = previewHeight
        var edgeWidth = (validScope * width).toInt()
        if (width > height) {
            edgeWidth = (validScope * height).toInt()
        }
        validLeft = (previewWidth - edgeWidth) / 2
        validRight = previewWidth - validLeft
        validTop = (previewHeight - edgeWidth) / 2
        validBottom = previewHeight - validTop
    }


    /**
     * 设置相机曝光补偿
     */
    fun setExposureCompensation(value: Int) {
        cameraView.setExposureCompensation(value)
    }

    /**
     * 设置相机白平衡
     */
    fun setWhiteBalance(value: String) {
        cameraView.setWhiteBalance(value)
    }

    // 设置相机预览分辨率
    fun setPreviewSize(width: Int, height: Int) {
        //避免重复设置
        if (previewHeight == height && previewWidth == width) return
        //todo 3
    }


    fun close() {
        faceEngine.unInit()
        idFaceEngine?.unInit()
        singleExecutors.shutdownNow()
        searchThread?.interrupt()
        cameraView.destroy()
    }

    fun startStream() {
        if (Config.isPushStream()) {
            cameraView.startStreaming(Config.getRtmpUrl())
        }
    }

    fun stopStream() {
        cameraView.stopStreaming()
    }


    @WorkerThread
    fun getFaceFeature(data: ByteArray?, width: Int, height: Int, format: Int): ByteArray? {
        val faceList = arrayListOf<FaceInfo>()
        val rltCode = faceEngine!!.detectFaces(data, width, height, format, faceList)
        if (rltCode == ErrorInfo.MOK && faceList.size > 0) {
            val feature = FaceFeature()
            val resultCode = faceEngine!!.extractFaceFeature(data, width, height, FaceEngine.CP_PAF_NV21, faceList.first(), feature)
            if (resultCode == ErrorInfo.MOK) {
                return feature.featureData
            }
            return null
        }
        return null
    }


    /**
     * 裁剪图片
     */
    fun cropBitmap(data: ByteArray, item: Rect, previewWidth: Int, previewHeight: Int): Pair<Bitmap, FacePosition> {
        val wOutLine = ((item.right - item.left) * 0.3 / 2).toInt()
        val hOutLine = ((item.bottom - item.top) * 0.7 / 2).toInt()
        val yuv = YuvImage(data, ImageFormat.NV21, previewWidth, previewHeight, null)
        val ops = ExtByteArrayOutputStream()
        val rLeft = if (item.left - wOutLine <= 0) 0 else item.left - wOutLine
        val rRight = if (item.right + wOutLine >= previewWidth) previewWidth else item.right + wOutLine
        val rTop = if (item.top - hOutLine <= 0) 0 else item.top - hOutLine
        val rBottom = if (item.bottom + hOutLine >= previewHeight) previewHeight else item.bottom + hOutLine
        val rof = Rect(rLeft, rTop, rRight, rBottom)
        yuv.compressToJpeg(rof, 100, ops)
        val bitmap = BitmapFactory.decodeByteArray(ops.byteArray, 0, ops.byteArray.size)
        //纠正角度
        val matrix = Matrix()
        matrix.setRotate(rotate.toFloat())
        val rltBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, false)
        val rf = RectF(item)
        val rorf = RectF(rof)
        matrix.mapRect(rf)
        matrix.mapRect(rorf)
        val facePosition = FacePosition()
        facePosition.height = rf.height().toInt()
        facePosition.width = rf.width().toInt()
        facePosition.x = (rf.left - rorf.left).toInt()
        facePosition.y = (rf.top - rorf.top).toInt()
        return Pair(rltBitmap, facePosition)
    }

    /**
     * 将不符合识别宽高的图片裁剪到适合识别
     */
    @WorkerThread
    private fun resizeBitmap(bitmap: Bitmap): Bitmap {
        val outOfWidth = bitmap.width % 4
        val outOfHeight = bitmap.height % 2
        if (outOfHeight == 0 && outOfWidth == 0) return bitmap
        val width = bitmap.width - outOfWidth
        val height = bitmap.height - outOfHeight
        return Bitmap.createBitmap(bitmap, 0, 0, width, height)
    }

    private fun resizeRect(rect: Rect): Rect {
        val left = if (rect.left < 0) 0 else rect.left
        val top = if (rect.top < 0) 0 else rect.top
        val right = if (rect.right > previewWidth) previewWidth else rect.right
        val bottom = if (rect.bottom > previewHeight) previewHeight else rect.bottom
        return Rect(left, top, right, bottom)
    }

}