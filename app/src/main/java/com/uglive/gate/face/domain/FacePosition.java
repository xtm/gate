package com.uglive.gate.face.domain;

import java.util.Objects;

public class FacePosition {


    /**
     * 左上角起点X坐标
     */
    private int x;

    /**
     * 左上角起点Y坐标
     */
    private int y;


    /**
     * 人脸宽度
     */
    private int width;


    /**
     * 人脸高度
     */
    private int height;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FacePosition)) return false;
        FacePosition that = (FacePosition) o;
        return getX() == that.getX() &&
                getY() == that.getY() &&
                getWidth() == that.getWidth() &&
                getHeight() == that.getHeight();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public String toString() {
        return "FacePosition{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
