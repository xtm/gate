package com.uglive.gate.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import com.uglive.gate.utils.ViewUtils

class SquareScanView : View {
    val mPaint = Paint()
    //方形占view 0-1
    var n = 1f
    private var mLeft = 0
    private var mRight = 0
    private var mTop = 0
    private var mBottom = 0
    val mTopRect = Rect()
    val mBottomRect = Rect()
    val mLeftRect = Rect()
    val mRightRect = Rect()
    val lineRect = Rect()

    @JvmOverloads constructor(context: Context, n: Float, attrs: AttributeSet? = null, def: Int = 0) : super(context, attrs, def) {
        this.n = n

        mPaint.style = Paint.Style.FILL_AND_STROKE
    }


    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            mPaint.color = Color.parseColor("#66000000")
            it.drawRect(mLeftRect, mPaint)
            it.drawRect(mRightRect, mPaint)
            it.drawRect(mTopRect, mPaint)
            it.drawRect(mBottomRect, mPaint)
//            mPaint.color = Color.GREEN
//            it.drawRect(lineRect, mPaint)
//            if (lineRect.bottom >= mBottom) {
//                lineRect.top = mTop
//                lineRect.bottom = mTop + 2
//            } else {
//                lineRect.top += 2
//                lineRect.bottom += 2
//            }
//            postInvalidateDelayed(20  )
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = ViewUtils.getMySize(100, widthMeasureSpec)
        val height = ViewUtils.getMySize(100, heightMeasureSpec)
        setMeasuredDimension(width, height)
        var edgeWidth = (height * n).toInt()
        if (width <= height) {
            edgeWidth = (width * n).toInt()
        }
        mLeft = (width - edgeWidth) / 2
        mTop = (height - edgeWidth) / 2
        mRight = width - mLeft
        mBottom = height - mTop
        mLeftRect.set(0, mTop, mLeft, mBottom)
        mTopRect.set(0, 0, width, mTop)
        mRightRect.set(mRight, mTop, width, mBottom)
        mBottomRect.set(0, mBottom, width, height)
        lineRect .set(mLeft, mTop, mRight, mTop + 2)
    }
}