package com.uglive.gate.widget

import android.content.Context
import android.hardware.Camera
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import java.io.IOException
import android.app.Activity
import android.view.Surface
import com.blankj.utilcode.util.LogUtils


/**
 * 相机预览控件。
 */
class CameraPreview constructor(context: Context, val pictureCallback: (ByteArray) -> Unit) : SurfaceView(context), SurfaceHolder.Callback {
    val TAG = "CameraPreview"
    var mCamera: Camera? = null


    val CAMERA_FACING = Camera.CameraInfo.CAMERA_FACING_BACK

    init {
        holder.addCallback(this)
    }


    /**
     * 初始化相机
     */
    private fun getCameraInstance(): Camera? {
        // 防止重复初始化
        var camera: Camera? = null
        try {
            camera = Camera.open(CAMERA_FACING )
        } catch (e: Exception) {
            e.printStackTrace()
            LogUtils.e("相机不存在！")
        }
        return camera
    }

    fun takePicture() {
        try {
            mCamera?.takePicture(null, null, null) { data, camera ->
                pictureCallback(data)
                //重新预览
                mCamera?.stopPreview()
                try {
                    mCamera?.startPreview()
                } catch (e: Exception) {
                    LogUtils.e("camera", "重写预览失败")
                }
            }
        } catch (e: Exception) {
            LogUtils.e("拍照失败。")
        }
    }

    private fun preview() {
        mCamera?.apply {
            val temp = parameters
            temp.jpegQuality = 25
            parameters = temp
//            setCameraDisplayOrientation(context as Activity, CAMERA_FACING, this)
            setPreviewDisplay(holder!!)
            startPreview()
        }
    }

    /**
     * 设置旋转角度
     */
    fun setCameraDisplayOrientation(activity: Activity,
                                    cameraId: Int, camera: android.hardware.Camera) {
        val info = android.hardware.Camera.CameraInfo()
        android.hardware.Camera.getCameraInfo(cameraId, info)
        val rotation = activity.windowManager.defaultDisplay.rotation
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
        }

        var result: Int
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360
            result = (360 - result) % 360  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360
        }
        camera.setDisplayOrientation(result)
    }

    override fun surfaceCreated(holder: SurfaceHolder?) {
        try {
            mCamera = getCameraInstance()
            preview()
        } catch (e: IOException) {
            Log.e(TAG, "打开相机失败: " + e.message)
        }

    }

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
        if (holder?.getSurface() == null) {
            return
        }
        try {
            mCamera?.stopPreview()
        } catch (e: Exception) {
        }
        try {
            preview()
        } catch (e: Exception) {
            Log.d(TAG, "相机预览失败: " + e.message)
        }

    }


    override fun surfaceDestroyed(holder: SurfaceHolder?) {
        try {
            mCamera?.apply {
                stopPreview()
                setPreviewDisplay(null)
                release()
            }
            mCamera = null
        } catch (e: Exception) {
        }
    }


}