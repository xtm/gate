package com.uglive.gate.repositroy

import com.blankj.utilcode.util.SDCardUtils
import com.blankj.utilcode.util.SPUtils
import com.blankj.utilcode.util.ToastUtils
import com.github.kittinunf.fuel.httpDownload
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.model.ApiModel
import java.io.File


class ConfigDataRepository(val apiModel: ApiModel) {
    val SP_ADV_FILE_PATH = "SP_ADV_FILE_PATH"
    val SP_ADV_PLACE_NAME = "SP_ADV_PLACE_NAME"
    val type = object : TypeToken<HashMap<String, String>>() {}.type
    val type1 = object : TypeToken<Map<String, Object>>() {}.type

    fun getAdvTitle(): String {
        val (title, _) = getDataFromServer()
        if (title!=null) return title
        else{
            val pathlist = SPUtils.getInstance().getString(SP_ADV_PLACE_NAME)
            if (!pathlist.isNullOrEmpty()) {
                val json = Gson().fromJson<Map<String, String>>(pathlist, type)
                val title = json.get(Config.projectId)
                if (!title.isNullOrEmpty()) return title!!
            }
        }
        return ""
    }

    fun getAdvImage(): String {
        val (_, imageUrl) = getDataFromServer()
        if (null!=imageUrl) return imageUrl
        else {
            val pathlist = SPUtils.getInstance().getString(SP_ADV_FILE_PATH)
            if (!pathlist.isNullOrEmpty()) {
                val json = Gson().fromJson<Map<String, String>>(pathlist, type)
                val path = json.get(Config.projectId)
                if (!path.isNullOrEmpty() && File(path).exists()) return path!!
            }
        }
        return ""
    }

    private fun getDataFromServer(): Pair<String?, String?> {
        val (result, error) = apiModel.getAdvInfo()
        if (error == null) {
            var json = mapOf<String, Object>()
            try {
                json = Gson().fromJson<Map<String, Object>>(result, type1)
            } catch (e: JsonSyntaxException) {
                AppExecutors.mainThread.execute { ToastUtils.showShort("服务器异常") }
                return Pair(null, null)
            }

            if (json["code"].toString() == "0") {
                AppExecutors.mainThread.execute { ToastUtils.showShort("未请求到广告活动地址数据") }
                return Pair(null, null)
            } else {
                if (json["list"] is CharSequence) return Pair(null, null)
                val info = json["list"] as Map<String, String>
                info["projectName"]?.let {
                    val str = SPUtils.getInstance().getString(SP_ADV_PLACE_NAME)
                    if (!str.isNullOrEmpty()) {
                        val map = Gson().fromJson<HashMap<String, String>>(str, type)
                        map.put(Config.projectId, it)
                        SPUtils.getInstance().put(SP_ADV_PLACE_NAME, Gson().toJson(map))
                    } else {
                        SPUtils.getInstance().put(SP_ADV_PLACE_NAME, Gson().toJson(mapOf(Config.projectId to it)))
                    }
                }
                info["picture"]?.let {
                    val name = it.split("""\/""".toPattern()).last()
                    val file = File(Config.advFileDir, "${Config.projectId}$name")
                    val (_, _, result) = it.httpDownload().destination { _, _ -> file }.responseString()
                    val (_, error) = result
                    if (error == null) {
                        val str = SPUtils.getInstance().getString(SP_ADV_FILE_PATH)
                        if (!str.isNullOrEmpty()) {
                            val map = Gson().fromJson<HashMap<String, String>>(str, type)
                            map.put(Config.projectId, file.absolutePath)
                            SPUtils.getInstance().put(SP_ADV_FILE_PATH, Gson().toJson(map))
                        } else {
                            SPUtils.getInstance().put(SP_ADV_FILE_PATH, Gson().toJson(mapOf(Config.projectId to it)))
                        }
                    }
                    return Pair(info["projectName"], file.absolutePath)
                }
                return Pair(info["projectName"], null)
            }
        } else {
            AppExecutors.mainThread.execute { ToastUtils.showShort("未请求到广告活动地址数据") }
            return Pair(null, null)
        }
        return Pair(null, null)
    }


}