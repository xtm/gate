package com.uglive.gate.repositroy

import android.text.TextUtils
import android.util.ArrayMap
import android.util.Log
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.blankj.utilcode.util.Utils
import com.github.kittinunf.fuel.httpDownload
import com.github.kittinunf.fuel.httpPost
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.uglive.gate.App
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.bean.*
import com.uglive.gate.db.localDB.LocalDB
import com.uglive.gate.db.workersDb.WorkersDB
import java.io.File
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class TicketRepository {

    private val TAG = "TicketRepository"
    private val app = Utils.getApp() as App
    private val ticketDao = LocalDB.get(app).ticketDao()
    private val localTicketDao = LocalDB.get(app).localTicketDao()
    private val workerDao  = WorkersDB.get(app).workerDao()
    private var workerTickets: List<Worker> = arrayListOf()
    var total = 0
    val type = object : TypeToken<Map<String, Object>>() {}.type
    val gson = Gson()
    val tickets: MutableList<TicketBean> = ArrayList<TicketBean>(8000)
    val localTickets: MutableSet<String> = HashSet<String>(8000)

    fun deleteLocalData() {
        AppExecutors.diskIO.execute {
            tickets.clear()
            ticketDao.deleteAll()
        }
    }

    fun updateItem(ticketBean: TicketBean) {
        AppExecutors.diskIO.execute { ticketDao.updateItem(ticketBean) }
    }

    fun getAllLocalData() = ticketDao.getAll()

    fun getTicketBeanSize() = ticketDao.countTickets()

    fun queryTickets(page: Int, size: Int, qrcode: String, rfid: String): List<TicketBean> {
        val offset = (page - 1) * size
        return ticketDao.queryTickets(pageSize = size, offset = offset, rfid = "$rfid%", qrCode = "$qrcode%")
    }

    init {
        AppExecutors.diskIO.execute {
            localTickets.addAll(localTicketDao.getAll())
            tickets.addAll(getAllLocalData())
            total = tickets.size
        }
    }


    /**
     * 获取本次活动获取票的地址和票总数
     */
    fun getTicketInfo(url: String, projectId: String, success: (String, Int) -> Unit) {

        url.httpPost(listOf("projectId" to projectId))
                .timeout(Config.REQUEST_TIME_OUT)
                .responseString { _, _, result ->
                    result.fold({
                        val type = object : TypeToken<Map<String, Object>>() {}.type
                        try {
                            val apiInfo = Gson().fromJson<Map<String, Object>>(it, type)
                            apiInfo["code"]?.let {
                                if ((it as Double) == 0.toDouble()) {
                                    val data = apiInfo["list"] as Map<String, String>
                                    val total = data["ticketNum"]
                                    val url = data["apiURL"]
                                    if (total.isNullOrBlank() || url.isNullOrBlank()) {
                                        ToastUtils.showShort("服务器返回了错误的信息")
                                    } else {
                                        total?.let {
                                            val totalInt = it.toInt()
                                            success(url!!, totalInt)
                                        }
                                    }
                                } else {
                                    success("", -1)
                                    ToastUtils.showShort("场次信息错误。")
                                }
                            }
                        } catch (e: Exception) {
                            success("", -1)
                            LogUtils.e(TAG, "无法解析文本,参数错误")
                            ToastUtils.showShort("请填写正确的场次信息")
                        }
                    }, {
                        success("", -1)
                        ToastUtils.showShort("获取信息失败，请检查网络")
                    })
                }
    }


    fun loadFaceImage(url: String, name: String, success: () -> Unit, failure: () -> Unit) {
        url.httpDownload()
                .destination { _, _ -> File(Config.fileDir, name) }
                .responseString { _, _, result ->
                    val (info, error) = result
                    if (error == null) {
                        Log.i(TAG, info)
                        success()
                    } else {
                        failure()
                    }
                }
    }

    /**
     * RFID搜索缓存
     */
    fun searchCacheTicketByRfid(rfid: String): TicketBean? {
        var tikect: TicketBean? = null
        tickets.forEach {
            if (rfid == it.soldChip) {
                tikect = it
            }
        }
        return tikect
    }

    /**
     * 二维码搜索缓存
     */
    fun searchCacheTicketByQrcode(qrCode: String): TicketBean? {
        var tikect: TicketBean? = null
        tickets.forEach {
            if (qrCode == it.soldCode) {
                tikect = it
            }
        }
        return tikect
    }

    /**
     * 二维码soldId
     */
    fun searchCacheTicketBysoldId(soldID: String): TicketBean? {
        var tikect: TicketBean? = null
        tickets.forEach {
            if (soldID == it.soldId) {
                tikect = it
            }
        }
        return tikect
    }

    /**
     * @process 返回下载进度，当前处理完成条数，总数，当下载进度为-1时下载失败
     */
    fun pullTickets(total: Int, url: String, process: (Int, Int) -> Unit, photoRequest: (RequestBean) -> Unit) {
        this.total = total
        var currentNum = 0
        AppExecutors.networkIO.execute {
            val num = 500
            val pageSize = total / num + 1
            for (page in 1.rangeTo(pageSize)) {
                val map = ArrayMap<String, String>()
                map["pageNum"] = "$page"
                map["dataNumber"] = "$num"
                val (_, _, result) = "http://${Config.IP}/$url".httpPost(listOf("pageNum" to page, "dataNumber" to num))
                        .timeout(3000)
                        .responseString()
                val (json, error) = result
                if (error == null) {
                    val apiInfo = gson.fromJson<Map<String, Object>>(json, type)
                    if ((apiInfo["code"] as Double) == 0.toDouble()) {
                        val data = apiInfo["list"] as List<Map<String, String>>
                        data.forEach {
                            val statusDesc = if (it["statusDesc"].isNullOrBlank()) 1 else 0
                            val soldUsed = if (it["soldUsed"].isNullOrBlank()) 0 else it["soldUsed"]?.toInt()
                                    ?: 0
                            try {
                                val bean = TicketBean(
                                        soldId = it["soldId"] ?: "0",
                                        soldCode = it["soldCode"], soldChip = it["soldChip"],
                                        soldPassID = it["soldPassId"], soldTimeStart = it["soldTimeStart"],
                                        soldTimeEnd = it["soldTimeEnd"],
                                        soldAvailableTimes = it["soldAvailability"] ?: "1",
                                        soldUsedTimes = it["soldUsedTimes"] ?: "1",
                                        statusDesc = statusDesc, soldUsed = soldUsed,
                                        imageFilePath = null)
                                if (!TextUtils.isEmpty(it["soldFacePhoto"])) {
                                    bean.imageFilePath = File(Config.fileDir, it["soldFacePhoto"]).absolutePath
                                    val request = RequestBean(
                                            id = 0,
                                            url = "http://${Config.IP}/Public/${it["soldFacePhoto"]}",
                                            params = gson.toJson(mapOf("name" to it["soldFacePhoto"])),
                                            filePath = null,
                                            requestType = REQUEST_TYPE_LOAD_FACE_IMAGE)
                                    photoRequest(request)
                                }
                                tickets.add(bean)
                            } catch (e: Exception) {
                                LogUtils.e(TAG, e.message)
                            }
                            currentNum++
                            process(currentNum, total)
                            if (currentNum == total) {
                                ticketDao.insert(tickets)
                            }
                        }
                    } else {
                        process(-1, 0)
                    }
                } else {
                    process(-1, 0)
                }
            }
        }

    }

    /**
     * 查询只用一次的票
     */
    fun searchLocalTicket(m1: String) = localTickets.contains(m1)

    fun insertLocalTicket(m1: String) {
        if (!localTickets.contains(m1)) {
            localTickets.add(m1)
            localTicketDao.insert(LocalTicket(m1))
        }
    }

    fun queryWorkerTickets(m1:String):Boolean{
        if (workerTickets.isEmpty()){
            workerTickets = workerDao.getWorkerTickets()
        }
        return workerTickets.contains(Worker(m1,0,null,""))
    }

    fun getWorkerTicket(m1:String): Worker?{
        return workerDao.getWorkerById(m1)
    }

    fun updateWorkerTicket(worker: Worker){
        workerDao.updateWorkerTickets(worker)
    }

    fun deleteData(){
        workerDao.queryReset()
    }
}