package com.uglive.gate.utils;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author fujie
 * @date: 2014年6月3日
 */
public class DateTimeUtil {

	/** 日期格式 ** */
	public static final String DATE_PATTERN = "yyyy-MM-dd";

	/** 日期格式 ** */
	public static final String TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 获得当前的系统时间
	 * 
	 * @return 当前的系统日期
	 */
	public static Date getCurrentTime() {
		return new Date();
	}

	/**
	 * 获得当前的系统日期，不带有分秒
	 * 
	 * @return 当前的系统日期
	 */
	public static Date getCurrentDate() {

		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		c.clear(Calendar.HOUR);
		c.clear(Calendar.MINUTE);
		c.clear(Calendar.SECOND);
		c.clear(Calendar.MILLISECOND);

		date = c.getTime();
		return date;
	}

	/**
	 * 获得当前的系统日期，不带有时分秒
	 * 
	 * @return 当前的系统日期
	 */
	public static Date getNowDate() {
		Date date = parseDate(getFormatCurrentDate(), DATE_PATTERN);
		return date;
	}

	/**
	 * 得到当前系统日期,格式："yyyy-MM-dd"
	 * 
	 * @return 当前系统日期
	 */
	public static String getFormatCurrentDate() {
		return format(getCurrentDate(), DATE_PATTERN);
	}

	/**
	 * 得到当前系统日期,格式："yyyy-MM-dd HH:mm:ss"
	 * 
	 * @return 当前系统日期
	 */
	public static String getFormatCurrentTime() {
		return format(getCurrentTime(), TIME_PATTERN);
	}

	/**
	 * 输出字符串类型的格式化日期
	 * 
	 * @param dt
	 *            Date
	 * @param pattern
	 *            时间格式
	 * @return sDate
	 */
	public static String format(Date dt, String pattern) {
		String sDate;
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		sDate = formatter.format(dt);
		return sDate;
	}

	/**
	 * 获取当前月份
	 * 
	 * @return 当前月份
	 */
	public static String getCurrentMonth() {
		return getDateMonth(getCurrentDate());
	}

	/**
	 * 得到指定日期的月份
	 * 
	 * @param date
	 *            指定日期
	 * @return 指定日期的月份
	 */
	public static String getDateMonth(Date date) {

		SimpleDateFormat format1 = new SimpleDateFormat(DATE_PATTERN);
		format1.setLenient(false);
		String dateStr = format1.format(date);
		int begin = dateStr.indexOf('-') + 1;
		int end = dateStr.lastIndexOf('-');
		String month = dateStr.substring(begin, end);
		return month;
	}

	/**
	 * 得到指定日期后若干天的日期
	 * 
	 * @param date
	 *            指定日期
	 * @param days
	 *            天数
	 * @return 指定天数后的日期
	 */
	public static Date afterDaysSinceDate(Date date, int days) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, days);
		date = c.getTime();
		return date;
	}

	/**
	 * 判断两个Date是否在同一天
	 * 
	 * @param date1
	 *            要比较的日期1
	 * @param date2
	 *            要比较的日期2
	 * @return 是否同一天
	 */
	public static boolean isTwoDatesInSameDay(Date date1, Date date2) {
		Date preDate1 = preDay(date1);
		Date nextDate1 = nextDay(date1);
		if (date2.after(preDate1) && date2.before(nextDate1)) {
			return true;
		}
		return false;
	}

	/**
	 * 得到指定日期的下一天
	 * 
	 * @param date
	 *            日期
	 * @return 指定日期的下一天
	 */
	public static Date nextDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		date = c.getTime();
		return date;
	}
	
	/**
	 * 得到指定日期的下一天
	 * 
	 * @param date 格式："yyyy-MM-dd"
	 *            日期
	 * @return 指定日期的下一天
	 */
	public static String nextDay(String date) {
		Date datetime=parseDate(date,"yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(datetime);
		c.add(Calendar.DATE, 1);
		datetime = c.getTime();
		return format(datetime,"yyyy-MM-dd");
	}

	/**
	 * 得到指定日期的前一天
	 * 
	 * @param date
	 *            日期
	 * @return 指定日期的前一天
	 */
	public static Date preDay(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, -1);
		date = c.getTime();
		return date;
	}

	/**
	 * 得到当前月份的下一个月份
	 * 
	 * @param date
	 *            日期
	 * @return 当前月份的下一个月份
	 */
	public static Date addMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, 1);
		date = c.getTime();
		return date;
	}

	/**
	 * 得到年份与月份
	 * 
	 * @param date
	 *            日期
	 * @return 年份与月份信息
	 */
	public static String getYearMonth(Date date) {
		String yearMonthStr = format(date, DATE_PATTERN);
		int index = yearMonthStr.lastIndexOf('-');
		yearMonthStr = yearMonthStr.substring(0, index);
		return yearMonthStr;
	}

	/**
	 * 得到当前月的最后一天
	 * 
	 * @param date
	 *            日期
	 * @return 最后一天日期
	 */
	public static Date getLastDayOfMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_MONTH, 1);
		c.add(Calendar.DAY_OF_MONTH, -1);
		date = c.getTime();
		return date;
	}

	/**
	 * 得到当前月的第一天
	 * 
	 * @param date
	 *            日期
	 * @return 当前月的第一天
	 */
	public static Date getFirstDayOfMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, 1);
		date = c.getTime();
		return date;
	}

	/**
	 * 得到当前周的第一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getFirstDayOfWeek(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_WEEK, 2);
		date = c.getTime();
		return date;
	}

	/**
	 * 判断一个日期是否在指定的时间段内
	 * 
	 * @param start
	 *            时间段的起始时间
	 * @param end
	 *            时间段的结束时间
	 * @param date
	 *            要判断的日期
	 * @return 是否在指定的时间段内
	 */
	public static boolean inTimeSegment(Date start, Date end, Date date) {
		start = preDay(start);
		end = nextDay(end);
		if (date.after(start) && date.before(end)) {
			return true;
		}
		return false;
	}

	/**
	 * 判断当前日期是否在指定的时间段内
	 * 
	 * @param start
	 *            时间段开始时间
	 * @param end
	 *            时间段结束时间
	 * @return 如果当前日期在指定时间段内，则为true，否则为false
	 */
	public static boolean isCurrentDateInTimeSegment(Date start, Date end) {
		Date date = getCurrentDate();
		if (inTimeSegment(start, end, date)) {
			return true;
		}
		return false;
	}

	/**
	 * 得到两个日期的间隔天数
	 * 
	 * @param start
	 *            起始时间
	 * @param end
	 *            结束时间
	 * @return 间隔天数
	 */
	public static int getBetweenDays(Date start, Date end) {
		if (start.after(end)) {
			return -1;
		}

		Calendar startC = Calendar.getInstance();
		startC.setTime(start);
		Calendar endC = Calendar.getInstance();
		endC.setTime(end);
		endC.add(Calendar.DAY_OF_YEAR, 1);
		int days = 0;
		do {
			days++;
			startC.add(Calendar.DAY_OF_YEAR, 1);
		}
		while (startC.before(endC));
		return days;
	}

	/**
	 * 计算两个时间之间相隔秒数
	 * 
	 * @param start
	 *            开始时间
	 * @param end
	 *            结束时间
	 * @return 相隔秒数
	 */
	public static int getIntervalSeconds(Date start, Date end) {
		// 分别得到两个时间的毫秒数
		long sl = start.getTime();
		long el = end.getTime();

		long ei = el - sl;
		return (int) (ei / 1000);
	}
	
	/**
	 * 计算两个时间之间相隔小时数
	 * 
	 * @param start
	 *            开始时间
	 * @param end
	 *            结束时间
	 * @return 相隔小时数
	 */
	public static int getIntervalHours(String start, String end) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss",
				Locale.CHINA);
		try {
			Date d1 = simpleDateFormat.parse(start);
			Date d2 = simpleDateFormat.parse(end);
		
		// 分别得到两个时间的毫秒数
		long sl = d1.getTime();
		long el = d2.getTime();

		long ei = el - sl;
		return (int) (ei /1000/3600);
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * 得到指定月份的天数
	 * 
	 * @param date
	 *            日期
	 * @return 天数
	 */
	public static int daysInMonth(Date date) {
		Date start = getFirstDayOfMonth(date);
		Date end = getLastDayOfMonth(date);
		String startStr = format(start, "yyyyMMdd");
		String endStr = format(end, "yyyyMMdd");
		return Integer.parseInt(endStr) - Integer.parseInt(startStr) + 1;
	}

	/**
	 * 比较日期大小
	 * 
	 * @param dt1
	 *            要比较的日期
	 * @param dt2
	 *            要比较的日期
	 * @return 0:相等，1：dt1大于dt2，-1：dt1小于dt2
	 */
	public static int compareDate(Date dt1, Date dt2) {
		if (dt1.getTime() > dt2.getTime()) {
			return 1;
		} else if (dt1.getTime() < dt2.getTime()) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * 比较日期大小
	 * 
	 * @param date1
	 *            要比较的日期
	 * @param date2
	 *            要比较的日期
	 * @return 0:相等，1：date1大于date2，-1：date1小于date2
	 */
	public static int compareDate(String date1, String date2) {
		DateFormat df = new SimpleDateFormat(TIME_PATTERN);
		try {
			Date dt1 = df.parse(date1);
			Date dt2 = df.parse(date2);
			return compareDate(dt1, dt2);
		}
		catch (Exception e) {
		}
		return 0;
	}

	/**
	 * 格式化日期
	 * 
	 * @param date
	 *            日期
	 * @param formart
	 *            格式
	 * @return 格式化后的日期
	 * @throws ParseException
	 */
	public static Date parseDate(String date, String formart) {
		SimpleDateFormat sdf = new SimpleDateFormat(formart);
		try {
			return sdf.parse(date);
		}
		catch (ParseException e) {
			return null;
		}
	}

	public static String formatterDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
		try {
			return sdf.format(date);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * 获取指定秒数后的日期，精确饿到秒
	 * 
	 * @param second
	 *            秒数
	 * @return 指定秒数后的日期
	 */
	public static Date lastDate(Long second) {
		return second != null ? new Date(getCurrentTime().getTime() + second * 1000) : getCurrentTime();
	}

	/**
	 * 毫秒转小时
	 * 
	 * @param mseconds
	 * @return
	 */
	public static Long ms2hour(Long mseconds) {
		if (null == mseconds) {
			return null;
		} else {
			return mseconds / (1000 * 3600);
		}

	}

	/**
	 * 获取日期星期几
	 */
	public static String getWeek(Date date) {
		SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
		try {
			return dateFm.format(date);
		}
		catch (Exception e) {
			return null;
		}

	}

	public static int getWeekNum(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(sdf.parse(date));
		}
		catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
		return cal.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 得到当前系统日期,给定格式格式
	 * 
	 * @return 当前系统日期
	 */
	public static String getFormatCurrentDate(String format) {
		return format(getCurrentDate(), format);
	}

	/**
	 * 两个时间相减获得相差天，时，分
	 */
	public static String getBetweenTime(String endDate, String beginDate) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String a = "";
		try {
			Date d1 = df.parse(endDate);
			Date d2 = df.parse(beginDate);
			long diff = d1.getTime() - d2.getTime();// 这样得到的差值是微秒级别
			long days = diff / (1000 * 60 * 60 * 24);
			long hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
			long minutes = (diff - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60)) / (1000 * 60);
			a = "" + days + "天" + hours + "小时" + minutes + "分";
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return a;
	}
	/**
	 * 比较二个字符串时间的大小，返回1,s1>s2;否则 ,s1<s2
	 * @param s1 时间一
	 * @param s2 时间二
	 * @return
	 */
	public int compare(String s1, String s2) {
		boolean flag = false;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd",
				Locale.CHINA);
		try {
			Date d1 = simpleDateFormat.parse(s1);
			Date d2 = simpleDateFormat.parse(s2);
			if (d1.getTime() > d2.getTime()) {
				flag = true;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		if (flag) {
			return 1;
		} else {
			return -1;
		}
	}
	
	public int compare2(String s1, String s2) {
		boolean flag = false;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss",
				Locale.CHINA);
		try {
			Date d1 = simpleDateFormat.parse(s1);
			Date d2 = simpleDateFormat.parse(s2);
			if (d1.getTime() > d2.getTime()) {
				flag = true;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		if (flag) {
			return 1;
		} else {
			return -1;
		}
	}
	
	
	public int compare4(String s1, String s2) {
		boolean flag = false;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss",
				Locale.CHINA);
		try {
			Date d1 = simpleDateFormat.parse(s1);
			Date d2 = simpleDateFormat.parse(s2);
			if (d1.getTime() > d2.getTime()) {
				flag = true;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		if (flag) {
			return 1;
		} else {
			return -1;
		}
	}
	/**
	 * 获取指定时间一年后的时间
	 * @param time 指定的时间
	 * @return
	 */
	public String getYear(String time){
		String arr[]=time.split("-");
		Format f = new SimpleDateFormat("yyyy-MM-dd");
        Calendar thisDay = Calendar.getInstance();
        thisDay.set(Calendar.YEAR, Integer.parseInt(arr[0]));
        thisDay.set(Calendar.MONTH, Integer.parseInt(arr[1]));// 月份,从0开始
        thisDay.set(Calendar.DAY_OF_MONTH, Integer.parseInt(arr[2]));
        thisDay.add(Calendar.YEAR,1);
        return f.format(thisDay.getTime());
	}

	/**
	 * 得到导航栏时间条
	 * @return
     */
	public static String getDate(){
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
		int w = c.get(Calendar.DAY_OF_WEEK) - 1 ;
		if (w < 0) {
			w = 0;
		}
		String mDate = c.get(Calendar.YEAR)+"年" + c.get(Calendar.MONTH) + "月" + c.get(Calendar.DATE) + "日  " + weekDays[w];
		return mDate;
	}
}
