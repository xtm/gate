package com.uglive.gate.utils

import com.blankj.utilcode.util.LogUtils
import com.github.kittinunf.fuel.httpDownload
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.uglive.gate.AppExecutors
import com.vector.update_app.HttpManager
import java.io.File


/**
 * 版本更新管理
 */
class AppUpgradeHttpManager : HttpManager {
    override fun download(url: String, path: String, fileName: String, callback: HttpManager.FileCallback) {

        url.httpDownload()
                .destination{ response, url ->
                    val path = File(path).apply { if (!exists()) mkdirs() }
                    LogUtils.i("fuelDownload","onDestination")
                    return@destination File(path,fileName)
                }
                .progress { readBytes, totalBytes ->
                    AppExecutors.mainThread.execute { callback.onProgress(readBytes.div(totalBytes.toFloat()), totalBytes) }
                }
                .response { _, _, result ->
                    LogUtils.i("fuelDownload","onResponse")
                    result.fold({
                        callback.onResponse(File(path,fileName))
                    }, {
                        callback.onError(it.toString())
                    })
                }
        callback.onBefore()

    }

    override fun asyncGet(url: String, params: MutableMap<String, String>, callBack: HttpManager.Callback) {
        url.httpGet(params.toList())
                .responseString { _, _, result ->
                    result.fold({
                        callBack.onResponse(it)
                    }, {
                        callBack.onError(it.toString())
                    })
                }
    }

    override fun asyncPost(url: String, params: MutableMap<String, String>, callBack: HttpManager.Callback) {
        url.httpPost(params.toList())
                .responseString {  _, _, result ->
                    result.fold({
                        callBack.onResponse(it)
                    }, {
                        callBack.onError(it.toString())
                    })
                }
    }
}