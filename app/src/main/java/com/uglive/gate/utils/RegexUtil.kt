package com.uglive.gate.utils

/**
 * Created by xiantm on 2018/4/27.
 * 正则工具
 */
object RegexUtil {
    //验证IP地址格式是否正确
    fun trueIp(str: String) = Regex("""^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$""").matches(str)
}