package com.uglive.gate.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.uglive.gate.R;


public class SoundPoolUtil {
    private static SoundPoolUtil soundPoolUtil;
    private static SoundPool soundPool;
    public static int CXPZ;//请对准摄像头，再次拍照
    public static int CFSK;//重复刷卡，请退出通道
    public static int WXPZ;//无效票证，请退出通道
    public static int YZTG;//验证通过，请通行
    public static int RCSJCW;//入场时间错误
    public static int WLYC;//网络异常
    public static int RCCSYW;//入场次数已用完
    public static int RCKCW;//入场口错误
    public static int WARNING;//检测到嫌疑犯
    public static int ZWWLR;//指纹未录入
    public static int RLDBSB;//人脸对比失败

    public SoundPoolUtil(Context context) {
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 10);
        //加载音频文件
        CXPZ = soundPool.load(context, R.raw.please_remake, 1);
        CFSK = soundPool.load(context, R.raw.repeat_enter, 2);
        WXPZ = soundPool.load(context, R.raw.ticket_error, 3);
        YZTG = soundPool.load(context, R.raw.ticket_right, 4);
        RCSJCW = soundPool.load(context, R.raw.enter_time_error, 5);
        WLYC = soundPool.load(context, R.raw.net_error, 6);
        RCCSYW = soundPool.load(context, R.raw.error_num, 7);
        RCKCW =soundPool.load(context, R.raw.error_door, 8);
        WARNING=soundPool.load(context, R.raw.warning, 9);
        ZWWLR=soundPool.load(context, R.raw.finger_not_input, 10);
        RLDBSB=soundPool.load(context, R.raw.face_validate_fail, 11);
    }

    //播放音频
    public void play(int number) {
        soundPool.play(number, 0.9f, 0.9f, 0, 0, 1);
    }
}
