package com.uglive.gate.utils

import android.view.View

object ViewUtils {
    fun getMySize(defaultSize: Int, measureSpec: Int): Int {
        var mySize = defaultSize

        val mode = View.MeasureSpec.getMode(measureSpec)
        val size = View.MeasureSpec.getSize(measureSpec)

        when (mode) {
            View.MeasureSpec.UNSPECIFIED -> {//如果没有指定大小,就设置为默认大小
                mySize = defaultSize
            }
            View.MeasureSpec.AT_MOST -> {//如果测量模式是最大取值为size
                //我们将大小取最大值,你也可以取其他值
                mySize = size
            }
            View.MeasureSpec.EXACTLY -> {//如果是固定的大小,那就不要去改变它
                mySize = size
            }
        }
        return mySize
    }
}