package com.uglive.gate.utils;



import com.uglive.gate.Config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.Arrays;

import android_serialport_api.SerialPort;

/**
 * 该类是厂家提供的,用于控制二维码扫描的
 */
public abstract class SerialHelper {
	private SerialPort mSerialPort;
	private OutputStream mOutputStream;
	private InputStream mInputStream;
	private ReadThread mReadThread;
	private String sPort="/dev/s3c2410_serial0";
	private int iBaudRate=9600;
	private boolean _isOpen=false;
	private byte[] _bLoopData=new byte[]{0x30};
	private int iDelay=500;
	//----------------------------------------------------
	public SerialHelper(String sPort, int iBaudRate){
		this.sPort = sPort;
		this.iBaudRate=iBaudRate;
	}
	public SerialHelper(){
		this("/dev/s3c2410_serial0",9600);
	}
	public SerialHelper(String sPort){
		this(sPort,9600);
	}
	public SerialHelper(String sPort, String sBaudRate){
		this(sPort, Integer.parseInt(sBaudRate));
	}
	//----------------------------------------------------
	public void open() throws SecurityException, IOException,InvalidParameterException {
		mSerialPort =  new SerialPort(new File(sPort), iBaudRate, 0);
		mOutputStream = mSerialPort.getOutputStream();
		mInputStream = mSerialPort.getInputStream();
		mReadThread = new ReadThread();
		mReadThread.start();
		_isOpen=true;
	}
	//----------------------------------------------------
	public void close(){
		if (mReadThread != null)
			mReadThread.interrupt();
		if (mSerialPort != null) {
			mSerialPort.close();
			mSerialPort = null;
		}
		_isOpen=false;
	}
	//----------------------------------------------------
	public void send(byte[] bOutArray){
		try
		{
			mOutputStream.write(bOutArray);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}


	//----------------------------------------------------
	public void sendHex(String sHex){
		byte[] bOutArray = MyFunc.HexToByteArr(sHex);
		send(bOutArray);
	}
	//----------------------------------------------------
	public void sendTxt(String sTxt){
		byte[] bOutArray =sTxt.getBytes();
		send(bOutArray);
	}










	//----------------------------------------------------
	private class ReadThread extends Thread {
		@Override
		public void run() {
			int allSize =0;
				byte[] buffer_ALL = new byte[512];
				boolean isSetbuffer_1 = false;
				while (true) {
					try {
						if (mInputStream == null) return;
						byte[] buffer = new byte[512];
						int size = mInputStream.read(buffer);
						allSize +=size;
						if (size == 1) {
							buffer_ALL[0] = buffer[0];
							isSetbuffer_1 = true;
						}
						if (size > 1) {
							for (int i = 0; i < size; i++) {
								buffer_ALL[i + 1] = buffer[i];
							}
							if (isSetbuffer_1) {
								if (!Config.INSTANCE.isTest()) {
									onDataReceived(new String(buffer_ALL).trim());
								}else {
									onDataReceived(ByteUtil.toHexString(Arrays.copyOfRange(buffer_ALL,0,allSize)));
								}
								allSize=0;
							}
							isSetbuffer_1 = false;
							buffer_ALL = new byte[512];
						}
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					} catch (Throwable e) {
						e.printStackTrace();
						return;
					}
				}
			}
	}


	public int getBaudRate()
	{
		return iBaudRate;
	}


	public boolean setBaudRate(int iBaud)
	{
		if (_isOpen)
		{
			return false;
		} else
		{
			iBaudRate = iBaud;
			return true;
		}
	}
	public boolean setBaudRate(String sBaud)
	{
		int iBaud = Integer.parseInt(sBaud);
		return setBaudRate(iBaud);
	}
	//----------------------------------------------------
	public String getPort()
	{
		return sPort;
	}
	public boolean setPort(String sPort)
	{
		if (_isOpen)
		{
			return false;
		} else
		{
			this.sPort = sPort;
			return true;
		}
	}
	//----------------------------------------------------
	public boolean isOpen()
	{
		return _isOpen;
	}
    protected abstract void onDataReceived(String text);
}