package com.uglive.gate.keepAlive

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.uglive.gate.ui.main.MainActivity

class KeepAliveService : Service() {

    private val TAG = "KeepAliveService"

    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand()")
        // 在API11之后构建Notification的方式，保证程序不死
        val builder = Notification.Builder(this.applicationContext)
        val intent = Intent(this, MainActivity::class.java)
        builder.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0))
                .setContentText("保证程序死后重启")
                .setContentTitle("监视服务")
                .setWhen(System.currentTimeMillis())
        val notification = builder.build()
        startForeground(101, notification)
        return super.onStartCommand(intent, flags, startId)
    }
}
