package com.uglive.gate.ui.main

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.decard.NDKMethod.BasicOper
import com.uglive.gate.AppExecutors
import com.uglive.gate.R
import com.uglive.gate.device.scanCard.TabletIScanCardDevice
import com.uglive.gate.utils.ByteUtil
import com.uglive.gate.utils.MDSEUtils
import com.uglive.gate.utils.SoundPoolUtil
import com.uglive.gate.utils.SoundPoolUtil.CXPZ
import com.uglive.gate.utils.SoundPoolUtil.WARNING
import kotlinx.android.synthetic.main.activity_card_test.*
import kotlin.concurrent.thread

class CardTestActivity : AppCompatActivity() {
    lateinit var tabletScanCardDevice: TabletIScanCardDevice
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_test)
//        tabletScanCardDevice = TabletIScanCardDevice()
//        tabletScanCardDevice.openTest(this)
//        idRead.setOnClickListener {
//            val result = tabletScanCardDevice.readCard()
//            Log.e("writCard 读", result)
//        }
//        readm1id.setOnClickListener {
//            val s = BasicOper.dc_card_hex(0)
//            var ticket = MDSEUtils.returnResult(s)
//            Log.e("CARD", "dc_getuid_i_d: $s")
//        }
//        idWrite.setOnClickListener {
//            val result = tabletScanCardDevice.writCard(text = idTestInput.text.toString(), secret = testsecretInput.text.toString())
//            Log.e("writCard 写", result)
//        }
        val mSoundPoolUtil = SoundPoolUtil(this)
        play1.setOnClickListener {
            mSoundPoolUtil.play(CXPZ)
        }
        play2.setOnClickListener {
            mSoundPoolUtil.play(WARNING)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        AppExecutors.diskIO.execute { tabletScanCardDevice.close() }
    }
}
