package com.uglive.gate.ui.settings

import com.uglive.gate.BasePresenter
import hxzy.ptb.hxzyappkit.BaseView

/**
 *
 */
interface SettingContract {

    interface View : BaseView<Presenter> {
        fun initViewData(ip: String, type: Int, secret: String, deviceName: String, uploadValidateInfo: Boolean, projectId: String,
                         faceValidateType: Int, faceDegree: Float,  cameraScale: Float,gonganIp:String,compareIdCadFace:Boolean,
                         captureNeedFace:Boolean,uploadChenduPolice:Boolean,needSecret:Boolean,
                         useKeyboardQrcodeDevice: Boolean,encByFactory:Boolean,pushStream:Boolean,
                         faceScale:Int, faceSize:Int,cameraMirror: Boolean,rectMirror: Boolean
        )
    }

    interface Presenter : BasePresenter {
        fun saveConfigs(ip: String, type: Int, secret: String, deviceName: String,
                        uploadValidateInfo: Boolean, projcetId: String, faceValidateType: Int,
                        faceDegree: Float,  cameraScale: Float, gonganIp:String,
                        compareIdCadFace:Boolean,captureNeedFace:Boolean,
                        uploadChenduPolice:Boolean,needSecret:Boolean,useKeyboardQrcodeDevice: Boolean,
                        encByFactory:Boolean,pushStream:Boolean,
                        faceScale:Int, faceSize:Int,cameraMirror: Boolean,rectMirror: Boolean)
    }
}