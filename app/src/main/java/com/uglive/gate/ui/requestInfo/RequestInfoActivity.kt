package com.uglive.gate.ui.requestInfo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import com.uglive.gate.R
import kotlinx.android.synthetic.main.activity_request_info.*

class RequestInfoActivity : AppCompatActivity(), RequestInfoContract.View {

    lateinit var mPresenter: RequestInfoContract.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_info)
        initView()
        setPresenter(RequestInfoPresenter(this))
    }

    private fun initView() {
        setSupportActionBar(req_info_tb)
        req_info_tb.setNavigationOnClickListener { onBackPressed() }
        req_info_tb.setOnMenuItemClickListener { mPresenter.querySize();return@setOnMenuItemClickListener true }
    }

    override fun setInfo(size: Int, total: Int) {
        req_info_msg.text = "已经上传数：$size\n总数：$total"
    }

    override fun setPresenter(presenter: RequestInfoContract.Presenter) {
        mPresenter = presenter
        presenter.start()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_request_info,menu)
        return true
    }
}
