package com.uglive.gate.ui.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.view.View
import com.blankj.utilcode.util.*
import com.decard.entitys.IDCard
import com.google.gson.Gson
import com.uglive.gate.App
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.R
import com.uglive.gate.bean.Ticket
import com.uglive.gate.bean.TicketBean
import com.uglive.gate.device.DeviceType
import com.uglive.gate.device.GateControllerDevice
import com.uglive.gate.device.UdpDevice
import com.uglive.gate.device.finger.FingerDeviceController
import com.uglive.gate.device.finger.FingerDeviceFactory
import com.uglive.gate.device.scanCard.HighFrequencyRfidDevice
import com.uglive.gate.device.scanCard.IScanCardDevice
import com.uglive.gate.device.scanCard.M1ScanType
import com.uglive.gate.device.scanCard.ScanCardFactory
import com.uglive.gate.device.scanQrcode.IScanQrDevice
import com.uglive.gate.device.scanQrcode.ScanQrcodeFactory
import com.uglive.gate.face.FaceHelper
import com.uglive.gate.model.*
import com.uglive.gate.repositroy.ConfigDataRepository
import com.uglive.gate.ui.settings.SettingActivity
import com.uglive.gate.utils.SoundPoolUtil
import com.uglive.gate.utils.SoundPoolUtil.*
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import kotlin.concurrent.timerTask


class MainPresenter(val view: MainContract.View, val configModel: ConfigModel) : MainContract.Presenter {


    override fun restTestData() {
        AppExecutors.diskIO.execute { ticketRepository.deleteData() }
    }

    var mFaceHelper: FaceHelper? = null
    var validateFace: ValidateFace? = null
    var validateTicket: ValidateTicket? = null
    val TAG = "MainPresenter"
    val gson = Gson()
    var gateControllerDevice: GateControllerDevice? = null
    var scanCardDevice: IScanCardDevice? = null
    var scanCardDeviceHigh: IScanCardDevice? = null
    var scanQrDevice: IScanQrDevice? = null
    var udpDevice: UdpDevice? = null
    val mSoundPoolUtil = SoundPoolUtil(view as Context)
    val ticketRepository = (Utils.getApp() as App).getTicketRepository()
    val apiModel = (Utils.getApp() as App).getApiModel()
    val configDataRepository = ConfigDataRepository(apiModel)

    var fingerService: ScheduledExecutorService? = null
    var fingerControl : FingerDeviceController? = null
    /**
     * 只传人脸照片时，通过3秒改变这个变量来控制对比人脸频率
     */
    var readyCapture = true

    /**
     * 发送请求间隔
     */
    var resetDuration = if (listOf(
                    SCAN_TYPE_FACE,
                    SCAN_TYPE_IDCARD_OR_RFID_HIGH,
                    SCAN_TYPE_IDCARD, SCAN_TYPE_IDCARD_OR_RFID,
                    SCAN_TYPE_IDCARD_AND_QRCODE).contains(Config.scanType) ) 1500L else 3000L
    //  用来重置扫描信息
    //  扫码信息
    var idCard: IDCard? = null
    var rfid: String? = null
    var qrCode: String? = null

    override fun start() {
        if (!Config.facePictureDir.exists()) Config.facePictureDir.mkdirs()
        if (!Config.advFileDir.exists()) Config.advFileDir.mkdirs()
        if (!Config.faceSourceDir.exists()) Config.faceSourceDir.mkdirs()
        //删除保存的人脸图片
        Config.facePictureDir.listFiles().forEach { it.delete() }
        //删除广告图片
        Config.advFileDir.listFiles().forEach { it.delete() }
        initConfig()
        view.initView()
        //获取海报和活动地址
        AppExecutors.networkIO.execute {
            val title = configDataRepository.getAdvTitle()
            AppExecutors.mainThread.execute { view.updateAdTitle(title) }
            val imagePath = configDataRepository.getAdvImage()
            AppExecutors.mainThread.execute { view.updateAdImage(imagePath) }
        }
    }

    override fun setFaceHelper(faceHelper: FaceHelper) {
        mFaceHelper = faceHelper
        if (Config.scanType == SCAN_TYPE_FACE){
            setValidateFace(faceHelper)
        }else setValidateTicket(faceHelper)
    }

    private fun setValidateFace(faceHelper: FaceHelper){
        validateFace = ValidateFace(view as Context,faceHelper,{pass,bitmap->
            bitmap?.let {  AppExecutors.mainThread.execute {  view.updateCapturePictureImageView(it) } }
            resetScanInfoDelay()
            if (pass) {
                mSoundPoolUtil.play(YZTG)
                gateControllerDevice?.openGate()
            }else{
                if (bitmap==null) mSoundPoolUtil.play(CXPZ)
                else mSoundPoolUtil.play(RLDBSB)
            }
        }, {status,message->
            AppExecutors.mainThread.execute { view.updateMessage(message, 2) }
            when (status) {
                "0" -> mSoundPoolUtil.play(SoundPoolUtil.WXPZ)
                "2" -> mSoundPoolUtil.play(SoundPoolUtil.RCCSYW)
                "3" -> mSoundPoolUtil.play(SoundPoolUtil.RCKCW)
                "4" -> mSoundPoolUtil.play(SoundPoolUtil.CFSK)
                "7" -> mSoundPoolUtil.play(SoundPoolUtil.RCSJCW)
                else -> ToastUtils.showShort("无法处理的返回值 $status")
            }
            resetScanInfoDelay()
        })
        validateFace?.start()
    }

    private fun setValidateTicket(faceHelper: FaceHelper) {
        //设置验证器
        validateTicket = ValidateTicket(udpDevice, faceHelper) { status, message, path, name, currentTicket, time ->
            //显示图片
            if (!path.isNullOrBlank() && status != "10") {
                view.updateCapturePictureImageView(path!!)
            } else if (!listOf(SCAN_TYPE_IDCARD, SCAN_TYPE_IDCARD_AND_QRCODE).contains(Config.scanType)
                    && status != "-1" && status != "-2" && status != "-3") {
                view.updateCapturePictureImageView(R.drawable.icon_no_img)
            }
            time?.let { AppExecutors.mainThread.execute { view.updateLastInTime(time) } }
            //显示名字
            name?.let { AppExecutors.mainThread.execute { view.updateName(it) } }
            if ("1" != status) {//验证不通过
                if (status == "-2") // 信息不完整
                    AppExecutors.mainThread.execute {
                        view.updateQrcode(currentTicket.qrCode ?: "")
                        view.updateIdCard(currentTicket.idCard ?: IDCard())
                        view.updateTicketInfo(currentTicket.m1 ?: "")
                        view.updateMessage(message, 1)
                    }
                else
                    AppExecutors.mainThread.execute { view.updateMessage(message, 2) }
                when (status) {
                    "-2" -> {
                    }
                    "-1" -> mSoundPoolUtil.play(CXPZ) //验证不通过
                    "0" -> mSoundPoolUtil.play(SoundPoolUtil.WXPZ)
                    "2" -> mSoundPoolUtil.play(SoundPoolUtil.RCCSYW)
                    "3" -> mSoundPoolUtil.play(SoundPoolUtil.RCKCW)
                    "4" -> mSoundPoolUtil.play(SoundPoolUtil.CFSK)
                    "5" -> mSoundPoolUtil.play(SoundPoolUtil.CXPZ)
                    "6" -> mSoundPoolUtil.play(SoundPoolUtil.CXPZ)
                    "7" -> mSoundPoolUtil.play(SoundPoolUtil.RCSJCW)
                    "10" -> mSoundPoolUtil.play(SoundPoolUtil.WARNING)
                    "99" -> mSoundPoolUtil.play(SoundPoolUtil.CXPZ)
                    else -> ToastUtils.showShort("无法处理的返回值 $status")
                }

                resetScanInfoDelay()
            } else {//验证通过
                validateSuccess(null, null, null)
                val count = SPUtils.getInstance().getInt(Config.IN_COUNT) + 1
                SPUtils.getInstance().put(Config.IN_COUNT, count)
                AppExecutors.mainThread.execute { view.setInCount(count) }
                //重置信息
                resetScanInfoDelay()
            }

        }
    }

    private fun initConfig() {
        Config.isTest = SPUtils.getInstance().getBoolean("istest")
        Config.faceSize = configModel.faceSize
        Config.faceScale = configModel.faceScale
        Config.scanType = configModel.scanType
        Config.secretKey = configModel.secret
        Config.projectId = configModel.projectId
        Config.faceDegree = configModel.faceDegree
        Config.uploadValidateInfo = configModel.uploadValidateInfo
        Config.cameraScale = configModel.cameraScale
        Config.gonganIp = configModel.gonganIp
        Config.compareIdcardFace = configModel.compareIdcardFace
        Config.captureNeedFace = configModel.captureNeedFace
        Config.uploadChenduPolice = configModel.uploadChenduPolice
        Config.needSecret = configModel.needSecret
        Config.useKeyboardScanQrcodeDevice = configModel.useKeyboardScanQrcodeDevice
        Config.encByFactory = configModel.encByFactory
        Config.deviceName = configModel.deviceName
        Config.faceValidateType = configModel.faceValidateType
        Config.cameraMirror = configModel.cameraMirror
        Config.rectMirror = configModel.rectMirror
        val needSetConfig = Config.scanType == -1 || Config.secretKey.isNullOrBlank() ||
                Config.deviceName.isNullOrBlank() || Config.faceValidateType == -1
        if (needSetConfig) {
            (view as Context).startActivity(Intent(view, SettingActivity::class.java))
        }
    }

    override fun getTicketInfo(activeId: String) {
        configModel.projectId = activeId
        Config.projectId = activeId

        //先删除本地数据
        ticketRepository.deleteLocalData()
        apiModel.deleteAllRequests()

        AppExecutors.diskIO.execute {
            //删除文件
            Config.fileDir.listFiles().forEach { it.delete() }
            ticketRepository.getTicketInfo("http://${Config.IP}/Api/index.php", activeId) { url, total ->
                if (total == -1) {
                    view.showProcess(-1, total)
                } else {
                    ticketRepository.pullTickets(total, url, { nowNum, total ->
                        AppExecutors.mainThread.execute { view.showProcess(nowNum, total) }
                    }, { apiModel.putRequest(it) })
                }
            }
        }
    }


    override fun openDevice(textview: View) {
        val isTablet = (view as MainActivity).isTablet
        var osType = DeviceType.PHONE
        if (isTablet) {
            osType = if (Config.useKeyboardScanQrcodeDevice) DeviceType.TABLET_KEYBOARD else DeviceType.TABLET
            //开启开关门设备
            gateControllerDevice = GateControllerDevice(false, DeviceUtils.getSDKVersionCode() >= 23)
            (Utils.getApp() as App).gateControllerDevice = gateControllerDevice
        }
        if (DeviceUtils.getSDKVersionCode() >= 22) udpDevice = UdpDevice(ticketRepository)
        //根据需要开启响应设备
        if (listOf(SCAN_TYPE_RFID_ONLY, SCAN_TYPE_RFID_ONLY_HIGH,
                        SCAN_TYPE_IDCARD,
                        SCAN_TYPE_RFID_AND_QRCODE, SCAN_TYPE_RFID_AND_QRCODE_HIGH,
                        SCAN_TYPE_RFID_OR_QRCODE, SCAN_TYPE_RFID_OR_QRCODE_HIGH,
                        SCAN_TYPE_IDCARD_OR_RFID, SCAN_TYPE_IDCARD_OR_RFID_HIGH,
                        SCAN_TYPE_FACE, SCAN_TYPE_IDCARD_AND_QRCODE).contains(Config.scanType)) {
            var needScanIDCard = false
            var m1ScanType = M1ScanType.USE_NOT
            when (Config.scanType) {
                SCAN_TYPE_RFID_ONLY, SCAN_TYPE_RFID_AND_QRCODE, SCAN_TYPE_RFID_OR_QRCODE,SCAN_TYPE_FACE,
                SCAN_TYPE_RFID_ONLY_HIGH, SCAN_TYPE_RFID_AND_QRCODE_HIGH, SCAN_TYPE_RFID_OR_QRCODE_HIGH -> {
                    if (Config.needSecret) m1ScanType = M1ScanType.USE_SECRET
                    else m1ScanType = M1ScanType.USE_ID
                }
                SCAN_TYPE_IDCARD_AND_QRCODE, SCAN_TYPE_IDCARD, SCAN_TYPE_IDCARD_OR_RFID_HIGH-> needScanIDCard = true
                SCAN_TYPE_IDCARD_OR_RFID-> {
                    if (Config.needSecret) m1ScanType = M1ScanType.USE_SECRET
                    else m1ScanType = M1ScanType.USE_ID
                    needScanIDCard = true
                }
            }
            scanCardDevice = ScanCardFactory.create(osType)
            //芯片检测的逻辑
            scanCardDevice?.open(context = view as Context, needScanIDCard = needScanIDCard, needScanM1 = m1ScanType, onScanM1 = {
                this.rfid = it
                AppExecutors.mainThread.execute { view.updateTicketInfo(it) }
                validateTicket?.putTicket(Ticket(m1 = rfid))
                validateFace?.putTicket(ticketId = it)
                return@open

            }, onScanIDCard = {
                idCard = it
                AppExecutors.mainThread.execute {
                    view.updateIdCard(it)
                    view.updateCapturePictureImageView(ConvertUtils.bytes2Bitmap(it.photoData))
                }
                validateTicket?.putTicket(Ticket(idCard = idCard))
                return@open
            })
        }

        if(Config.scanType==SCAN_TYPE_IDCARD_OR_RFID_HIGH){
            scanCardDeviceHigh = HighFrequencyRfidDevice()
            scanCardDeviceHigh?.open(context = view as Context, needScanIDCard = false, needScanM1 = M1ScanType.USE_NOT, onScanM1 = {
                this.rfid = it
                AppExecutors.mainThread.execute { view.updateTicketInfo(it) }
                validateTicket?.putTicket(Ticket(m1 = rfid))
                validateFace?.putTicket(ticketId = it)
                return@open

            }, onScanIDCard = {})
        }

        //根据需要二维码设备
        if (listOf(SCAN_TYPE_QRCODE_ONLY, SCAN_TYPE_FACE,
                        SCAN_TYPE_RFID_AND_QRCODE, SCAN_TYPE_RFID_AND_QRCODE_HIGH,
                        SCAN_TYPE_RFID_OR_QRCODE, SCAN_TYPE_RFID_OR_QRCODE_HIGH,
                        SCAN_TYPE_IDCARD_AND_QRCODE).contains(Config.scanType)) {
            scanQrDevice = ScanQrcodeFactory.create(osType, view = textview)
            scanQrDevice?.setDataCallBack {
                qrCode = it
                AppExecutors.mainThread.execute { view.updateQrcode(it) }
                validateTicket?.putTicket(Ticket(qrCode = qrCode))
                validateFace?.putQrCode(qrCode!!)
                return@setDataCallBack
            }
        }
        if (SCAN_TYPE_FINGER == Config.scanType) {
            fingerControl = FingerDeviceFactory.geFingerDevice {
                ToastUtils.showShort(it)
            }
            fingerControl?.open(view as Activity)
            fingerService = Executors.newSingleThreadScheduledExecutor()
            fingerService?.scheduleAtFixedRate({
                if (fingerControl?.identify()!=-1){
                    mSoundPoolUtil.play(YZTG)
                    gateControllerDevice?.openGate()
                }else{
                    mSoundPoolUtil.play(ZWWLR)
                }
                fingerControl?.cancelTask()
            },1000,200,TimeUnit.MILLISECONDS)
        }
    }

    /**
     * 删除人脸
     */
    override fun deleteFace(rfid: String) {
        ValidateFace.deleteFaceByRfid(rfid,view as Context)
    }
    override fun closeDevice() {
        gateControllerDevice?.close()
        scanCardDevice?.close()
        scanQrDevice?.close()
        udpDevice?.close()
        validateTicket?.release()
        validateFace?.close()
        fingerService?.shutdownNow()
        fingerControl?.close()
    }

    private var resetUITask: TimerTask? = null
    /**
     * 2500ms后重置信息
     */
    @Synchronized
    private fun resetScanInfoDelay() {
        resetUITask?.cancel()
        resetUITask = timerTask {
            synchronized(MainPresenter::class) {
                resetScanInfoNow()
            }
        }
        Timer().schedule(resetUITask, resetDuration)
    }

    /**
     * 重置信息
     */
    @Synchronized
    private fun resetScanInfoNow() {
        idCard = null
        rfid = null
        qrCode = null
        readyCapture = true
        AppExecutors.mainThread.execute { view.resetUI() }
    }


    /**
     * 验证通过
     */
    private fun validateSuccess(bitmap: Bitmap?, ticket: TicketBean?, data: ByteArray?) {
        //验证通过开门
        gateControllerDevice?.openGate()

        //提示
        AppExecutors.mainThread.execute {
            view.updateMessage("验证通过", 0)
            view.updateStatusTip(true)
        }
        mSoundPoolUtil.play(YZTG)

    }


}