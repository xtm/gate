package com.uglive.gate.ui.fingerDemo

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.uglive.gate.R
import com.uglive.gate.device.finger.FingerDeviceFactory
import kotlinx.android.synthetic.main.activity_finger_demo.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future
import kotlin.concurrent.thread

class FingerDemoActivity : AppCompatActivity() {


    private val fingerDeviceController = FingerDeviceFactory.geFingerDevice {
//        runOnUiThread { message.text = it }
    }

    private val executorService  = Executors.newSingleThreadExecutor()
    private val list = ArrayList<Future<Boolean>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finger_demo)
        init()
    }

    private fun init() {
        if (fingerDeviceController.open(this)){
            state.text = "设备已连接"
        }else{
            state.text = "设备连接失败"
        }
        regist.setOnClickListener {
            disAbleBtn()
            val task = executorService.submit(Callable<Boolean> {
                val id = fingerDeviceController.enrollFinger { msg, _ ->
                    runOnUiThread { message.text = msg }
                }
                runOnUiThread {
                    when {
                        id > 0 -> message.text = "指纹注册成功"
                        id == -1 -> message.text = "指纹已存在"
                        else -> message.text = "指纹注册失败"
                    }
                }
                cancelTask()
                return@Callable  true
            })
            list.add(task)
        }
        delete.setOnClickListener {
            disAbleBtn()
            val task = executorService.submit(Callable<Boolean> {
                val id = fingerDeviceController.identify()
                runOnUiThread {
                    if (id > 0){
                        message.text = if(fingerDeviceController.deleteFinger(id)) "指纹删除成功" else "指纹删除失败"
                        cancelTask()
                    }else {
                        message.text = "无法删除未录入的指纹"
                        cancelTask()
                    }
                }

                return@Callable  true
            })
            list.add(task)
        }
        match.setOnClickListener {
           disAbleBtn()
            val task = executorService.submit(Callable<Boolean> {
                val id = fingerDeviceController.identify()
                runOnUiThread {
                    if (id > 0){
                        message.text = "指纹ID：$id"
                    }else message.text = "未找到你的指纹"
                }
                cancelTask()
                return@Callable  true
            })
            list.add(task)
        }
        cancel.setOnClickListener {
            cancelTask()
        }
    }

    private fun cancelTask(){
        list.forEach { it.cancel(true) }
        list.clear()
        fingerDeviceController.cancelTask()
        message.postDelayed({
            enableBtn()
            message.text = "选择操作类型"
        },3000)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!isBackPressed){
            restatApp()
        }
    }

    private fun restatApp() {
        fingerDeviceController.close()
        //2S后重启程序,因为需要重新配置日志发送模块
        val intent = packageManager.getLaunchIntentForPackage(application.packageName)
        val restartIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, restartIntent)
        thread {
            Thread.sleep(1000)
            System.exit(0)
        }
    }
    var isBackPressed = false
    override fun onBackPressed() {
        super.onBackPressed()
        isBackPressed = true
        restatApp()
    }

    private fun enableBtn(){
        cancel.isEnabled = false
        regist.isEnabled = true
        delete.isEnabled = true
        match.isEnabled = true
    }

    private fun disAbleBtn(){
        cancel.isEnabled = true
        regist.isEnabled = false
        delete.isEnabled = false
        match.isEnabled = false
    }


}
