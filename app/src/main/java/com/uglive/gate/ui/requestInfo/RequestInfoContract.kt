package com.uglive.gate.ui.requestInfo

import com.uglive.gate.BasePresenter
import hxzy.ptb.hxzyappkit.BaseView


interface RequestInfoContract {

    interface View : BaseView<Presenter> {
        /**
         * 更新上传数/总数
         * @param size 已经上传数
         * @param total 总数
         */
        fun setInfo(size:Int,total:Int)
    }

    interface Presenter : BasePresenter {

        /**
         * 查询未上传数/总数
         */
        fun querySize()

    }
}