package com.uglive.gate.ui.ticketinfo.dlDataFragment

import com.blankj.utilcode.util.Utils
import com.uglive.gate.App

/**
 * Created by xiantm on 2018/5/21.
 */
class DLFPresenter(val view: DLFContract.View) : DLFContract.Presenter {

    val ticketRepository =  (Utils.getApp() as App).getTicketRepository()

    override fun start() {
    }

    override fun queryData(page: Int, size: Int, qrcode: String, rfid: String) = ticketRepository.queryTickets(page, size, qrcode, rfid)
}
