package com.uglive.gate.ui.ticketinfo

import com.uglive.gate.BasePresenter
import hxzy.ptb.hxzyappkit.BaseView


interface TicketDataContract {

    interface View : BaseView<Presenter> {

        //数据总数
        fun showTotal(nowNum: Int, total: Int)


    }

    interface Presenter : BasePresenter {
        fun getTotal()
    }
}