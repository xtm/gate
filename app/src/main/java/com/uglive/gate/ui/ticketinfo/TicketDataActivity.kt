package com.uglive.gate.ui.ticketinfo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import com.uglive.gate.R
import com.uglive.gate.ui.ticketinfo.dlDataFragment.DownLoadDataFragment
import kotlinx.android.synthetic.main.activity_ticket_data.*

class TicketDataActivity : AppCompatActivity(), TicketDataContract.View {
    lateinit var mPresenter: TicketDataContract.Presenter
    lateinit var mDownLoadDataFragment: DownLoadDataFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_data)
        setSupportActionBar(data_act_tb)
        setPresenter(TicketDataPresenter(view = this))
        mPresenter.start()

        data_act_tb.setOnMenuItemClickListener {

            return@setOnMenuItemClickListener true
        }

        ticketReset.setOnClickListener {
            ticket_search_qrcode.setText("")
            ticket_search_rfid.setText("")
            ticket_search_drawer.closeDrawer(Gravity.END)
        }

        ticketsSearch.setOnClickListener {
            mDownLoadDataFragment.query(
                    qrcode = ticket_search_qrcode.text.toString().trim(),
                    rfid = ticket_search_rfid.text.toString().trim())

            ticket_search_drawer.closeDrawer(Gravity.END);
        }
        data_act_tb.setNavigationOnClickListener { onBackPressed() }
        data_act_tb.setOnMenuItemClickListener {
            ticket_search_drawer.openDrawer(Gravity.END)
            return@setOnMenuItemClickListener true
        }

    }

    override fun showTotal(nowNum: Int, total: Int) {
        data_act_tb.subtitle = "下载数/总数:$nowNum/$total"
        mDownLoadDataFragment = DownLoadDataFragment()
        mDownLoadDataFragment.totalSize = total
        supportFragmentManager
                .beginTransaction()
                .add(R.id.data_fragment_container, mDownLoadDataFragment, "download")
                .commit()
    }

    override fun setPresenter(presenter: TicketDataContract.Presenter) {
        mPresenter = presenter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_ticket_data, menu)
        return true
    }
}
