package com.uglive.gate.ui.ticketinfo.dlDataFragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.uglive.gate.AppExecutors
import com.uglive.gate.R
import com.uglive.gate.bean.TicketBean
import kotlinx.android.synthetic.main.fragment_load_data_info.*


class DownLoadDataFragment : Fragment(), DLFContract.View {

    lateinit var mPresenter: DLFContract.Presenter
    lateinit var mAdapter: BaseQuickAdapter<TicketBean, BaseViewHolder>

    private var dataPage = 1
    var totalSize = 0
    private val pageSize = 15
    var qrcode = ""
    var rfid = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_load_data_info, null)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setPresenter(DLFPresenter(this))
        download_data_rv.layoutManager = LinearLayoutManager(context)
        download_data_rv.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        mAdapter = QuickAdapterQuickAdapter()
        AppExecutors.diskIO.execute {
            mAdapter.setNewData(mPresenter.queryData(dataPage, pageSize, qrcode, rfid))
        }
        mAdapter.setEnableLoadMore(true)
        download_data_rv.adapter = mAdapter
        mAdapter.setOnLoadMoreListener({
            AppExecutors.diskIO.execute {
                val data = mPresenter.queryData(++dataPage, pageSize, qrcode, rfid)
                AppExecutors.mainThread.execute {
                    mAdapter.addData(data)
                    mAdapter.loadMoreComplete()
                }
            }
        }, download_data_rv)

    }

    /**
     * 搜索本地数据
     */
    fun query(qrcode: String, rfid: String) {
        this.qrcode = qrcode
        this.rfid = rfid
        dataPage = 1
        mAdapter.loadMoreEnd(false)
        AppExecutors.diskIO.execute {
            val data = mPresenter.queryData(dataPage, pageSize, qrcode, rfid)
            AppExecutors.mainThread.execute {
                mAdapter.setNewData(data)
                mAdapter.loadMoreComplete()
            }
        }
    }


    class QuickAdapterQuickAdapter : BaseQuickAdapter<TicketBean, BaseViewHolder>(R.layout.ticket_data_item) {
        override fun convert(helper: BaseViewHolder, item: TicketBean) {
            helper.setText(R.id.ticket_data_rfid, "票号：${item.soldChip}")
            helper.setText(R.id.ticket_data_qrcode, "二维码：${item.soldCode}")
        }
    }

    override fun setPresenter(presenter: DLFContract.Presenter) {
        mPresenter = presenter
    }
}