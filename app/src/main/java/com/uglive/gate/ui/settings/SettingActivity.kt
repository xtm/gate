package com.uglive.gate.ui.settings

import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.blankj.utilcode.util.SPUtils
import com.blankj.utilcode.util.SnackbarUtils
import com.google.common.collect.Range
import com.uglive.gate.Config
import com.uglive.gate.R
import com.uglive.gate.bean.ValidateType
import com.uglive.gate.model.*
import com.uglive.gate.remoteControl.AppManager
import com.uglive.gate.remoteControl.connect.RECEIVER_ACTION_UPGRADE_APP
import com.uglive.gate.remoteControl.connect.RemoteControlService
import com.uglive.gate.utils.AppUpgradeHttpManager
import com.vector.update_app_kotlin.updateApp
import kotlinx.android.synthetic.main.activity_setting.*
import kotlin.concurrent.thread


class SettingActivity : AppCompatActivity(), SettingContract.View {

    lateinit var mPresenter: SettingContract.Presenter
    lateinit var mValidationTextInput: AwesomeValidation
    lateinit var mValidation: AwesomeValidation
    var mConnBinder: RemoteControlService.MyBinder? = null
    val mServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            Log.e("MainActivity","disconnect RemoteControlService Maybe Dead")
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            mConnBinder = service as RemoteControlService.MyBinder
            mConnBinder?.sMsgChangePage("settings")
        }
    }

    val updateReceiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            updateApp("http://${Config.IP}:60000/apk/checkAndUpdateApp", AppUpgradeHttpManager()).update()
        }
    }
    override fun initViewData(ip: String, type: Int, secret: String, deviceName: String, uploadValidateInfo: Boolean,
                              projcetId: String, faceValidateTp: Int, faceDegree: Float, cameraScale: Float, gonganIp: String,
                              compareIdCadFace: Boolean, captureNeedFace: Boolean, uploadChenduPolice: Boolean,
                              needSecre: Boolean, useKeyboardQrcodeDevice: Boolean, encByFactory: Boolean,
                              pushStream: Boolean, faceScale: Int, faceSize: Int, cameraMirr: Boolean, rectMirr: Boolean) {
        ipInput.setText(ip)
        secretInput.setText(secret)

        val validateTypes = arrayOf(
                ValidateType(-1, "请选择票证验证类型"),
                ValidateType(SCAN_TYPE_RFID_ONLY, "只验电子标签"),
                ValidateType(SCAN_TYPE_QRCODE_ONLY, "只验二维码"),
                ValidateType(SCAN_TYPE_RFID_AND_QRCODE, "验电子标签+二维码"),
                ValidateType(SCAN_TYPE_FACE, "通过对比人脸验证"),
                ValidateType(SCAN_TYPE_RFID_OR_QRCODE, "电子标签或二维码"),
                ValidateType(SCAN_TYPE_RFID_ONLY_HIGH, "只验电子标签(高频)"),
                ValidateType(SCAN_TYPE_RFID_AND_QRCODE_HIGH, "验电子标签+二维码(高频)"),
                ValidateType(SCAN_TYPE_RFID_OR_QRCODE_HIGH, "电子标签或二维码(高频)"),
                ValidateType(SCAN_TYPE_FINGER, "指纹验证"),
                ValidateType(SCAN_TYPE_IDCARD, "验身份证"),
                ValidateType(SCAN_TYPE_IDCARD_AND_QRCODE, "身份证+二维码"),
                ValidateType(SCAN_TYPE_IDCARD_OR_RFID, "身份证或电子标签"),
                ValidateType(SCAN_TYPE_IDCARD_OR_RFID_HIGH, "身份证或电子标签(高频)")
        )
        scanType.adapter = ArrayAdapter<ValidateType>(this, R.layout.dropdown_item, validateTypes)
        val selection = if (-1 == type) 0 else {
            validateTypes.indexOf(ValidateType(type,""))
        }
        scanType.setSelection(selection)
        faceValidateType.adapter = ArrayAdapter<ValidateType>(this, R.layout.dropdown_item, arrayOf(
                ValidateType(-1, "请选择人脸验证类型"),
                ValidateType(VALIDATE_FACE_TYPE_NO, "不验证"),
                ValidateType(VALIDATE_FACE_TYPE_LOCAL, "通过局域网验证"),
                ValidateType(VALIDATE_FACE_TYPE_ALY, "通过服务器验证")
        ))
        projectIdInput.setText(projcetId)
        istext.isChecked = Config.isTest
        uploadValidateInfoSw.isChecked = uploadValidateInfo
        compareIdCardFace.isChecked = compareIdCadFace
        captureNeedFaceSw.isChecked = captureNeedFace
        scCdGaSw.isChecked = uploadChenduPolice
        swNeedSecret.isChecked = needSecre
        useUsbQrcodeDevice.isChecked = useKeyboardQrcodeDevice
        encByFactorySw.isChecked = encByFactory
        faceDegreeInput.setText(faceDegree.toString())
        faceValidateType.setSelection(if (-1 == faceValidateTp) 0 else faceValidateTp + 1)
        deviceNameInput.setText(deviceName)
        gaipinput.setText(gonganIp)
        cameraScaleInput.setText(cameraScale.toString())
        swPushStream.isChecked = pushStream
        cameraMirror.isChecked = cameraMirr
        rectMirror.isChecked = rectMirr
        faceSizeInput.setText("$faceSize")
        faceScaleInput.setText("$faceScale")
    }

    override fun setPresenter(presenter: SettingContract.Presenter) {
        this.mPresenter = presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        val toolbar = findViewById<Toolbar>(R.id.title)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        setValidator()
        testError.setOnClickListener { throw RuntimeException() }
        setPresenter(SettingPresenter(this, ConfigModel()))
        mPresenter.start()
        bindService(Intent(this, RemoteControlService::class.java),mServiceConnection,BIND_AUTO_CREATE)
        //接受升级推送
        registerReceiver(updateReceiver, IntentFilter(RECEIVER_ACTION_UPGRADE_APP))
    }

    private fun setValidator() {
        mValidation = AwesomeValidation(ValidationStyle.BASIC)
        mValidationTextInput = AwesomeValidation(ValidationStyle.TEXT_INPUT_LAYOUT)
        mValidation.addValidation(this, R.id.scanType, {
            val item = (it.view as Spinner).selectedItem as ValidateType
            if (item.type == -1) {
                return@addValidation false
            }
            return@addValidation true
        }, {
            val itemView = (it.view as Spinner).selectedView as TextView
            itemView.setTextColor(Color.RED)
        }, {
            val itemView = (it.view as Spinner).selectedView as TextView
            itemView.setTextColor(Color.BLACK)
        }, R.string.setting_err_type)
        mValidation.addValidation(this, R.id.faceValidateType, {
            val item = (it.view as Spinner).selectedItem as ValidateType
            if (item.type == -1) {
                return@addValidation false
            }
            return@addValidation true
        }, {
            val itemView = (it.view as Spinner).selectedView as TextView
            itemView.setTextColor(Color.RED)
        }, {
            val itemView = (it.view as Spinner).selectedView as TextView
            itemView.setTextColor(Color.BLACK)
        }, R.string.setting_err_type)
        mValidation.addValidation(this, R.id.faceDegreeInput, Range.closed(0.1f, 1.0f), R.string.setting_err_degree)
        mValidation.addValidation(this, R.id.cameraScaleInput, Range.closed(0.1f, 1.0f), R.string.setting_err_camera_scale)
        mValidation.addValidation(this, R.id.projectIdInput, Range.closed(0, 100000), R.string.setting_err_projectId)
        mValidationTextInput.addValidation(ipLayout, Regex("""^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$""").toPattern(), "IP格式不对")
        mValidationTextInput.addValidation(gaiplayout, Regex("""^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$""").toPattern(), "IP格式不对")
        mValidationTextInput.addValidation(secretLayout, { str: String -> !str.isNullOrBlank() }, "密钥不能为空")
//        mValidationTextInput.addValidation(deviceNameLayout, Regex("""^[a-zA-Z]+[0-9]+$""").toPattern(), "设备编号格式不对")
        btnSaveSettings.setOnClickListener {
            if (istext.isChecked) {
                SPUtils.getInstance().put("istest", true)
            } else {
                SPUtils.getInstance().put("istest", false)
            }
            if (mValidation.validate() && mValidationTextInput.validate()) {
                mPresenter.saveConfigs(
                        ip = ipInput.text.toString(),
                        type = (scanType.selectedItem as ValidateType).type,
                        secret = secretInput.text.toString(),
                        deviceName = deviceNameInput.text.toString(),
                        uploadValidateInfo = uploadValidateInfoSw.isChecked,
                        faceValidateType = (faceValidateType.selectedItem as ValidateType).type,
                        faceDegree = faceDegreeInput.text.toString().toFloat(),
                        projcetId = projectIdInput.text.toString(),
                        gonganIp = gaipinput.text.toString().trim(),
                        cameraScale = cameraScaleInput.text.toString().toFloat(),
                        compareIdCadFace = compareIdCardFace.isChecked,
                        captureNeedFace = captureNeedFaceSw.isChecked,
                        uploadChenduPolice = scCdGaSw.isChecked,
                        needSecret = swNeedSecret.isChecked,
                        useKeyboardQrcodeDevice = useUsbQrcodeDevice.isChecked,
                        encByFactory = encByFactorySw.isChecked,
                        pushStream = swPushStream.isChecked,
                        faceScale = faceScaleInput.text.toString().toInt(),
                        faceSize = faceSizeInput.text.toString().toInt(),
                        cameraMirror = cameraMirror.isChecked,
                        rectMirror = rectMirror.isChecked
                )
                mConnBinder?.sMsgDeviceConfig(AppManager.genDevice())
                //2S后重启程序,因为需要重新配置日志发送模块
                val intent = packageManager.getLaunchIntentForPackage(application.packageName)
                val restartIntent = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
                val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 2500, restartIntent)
                val dialog = AlertDialog.Builder(this).setTitle("提示").setMessage("程序2s后重启").show()

                thread {
                    Thread.sleep(500)
                    unbindService(mServiceConnection)
                    mConnBinder?.stopService()
                    //等待配置保存好同时同时提醒用户,1s后关闭程序
                    runOnUiThread { dialog.dismiss() }
                    System.exit(0)
                }


            } else {
                SnackbarUtils.with(settingsContainer).setMessage("必须填写所有配置信息").show()
            }
        }
    }

    override fun onBackPressed() {
        if (mValidation.validate()) {
            SnackbarUtils.with(settingsContainer).setMessage("请点击保存").show()
        } else {
            SnackbarUtils.with(settingsContainer).setMessage("必须填写所有配置信息").show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(mServiceConnection)
        unregisterReceiver(updateReceiver)
    }
}
