package com.uglive.gate.ui.main

import android.graphics.Bitmap
import android.util.Log
import com.blankj.utilcode.util.ConvertUtils
import com.blankj.utilcode.util.NetworkUtils
import com.blankj.utilcode.util.PathUtils
import com.blankj.utilcode.util.ToastUtils
import com.decard.entitys.IDCard
import com.uglive.gate.Config
import com.uglive.gate.bean.Ticket
import com.uglive.gate.device.UdpDevice
import com.uglive.gate.device.scanCard.STATUS_0
import com.uglive.gate.device.scanCard.STATUS_4
import com.uglive.gate.face.FaceHelper
import com.uglive.gate.face.domain.FacePosition
import com.uglive.gate.model.*
import com.uglive.gate.repositroy.TicketRepository
import com.uglive.gate.shareData.PhotoSocketGetter
import com.uglive.gate.shareData.UDPService
import java.io.File
import java.io.FileOutputStream
import java.util.*
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread
import kotlin.concurrent.timerTask


/**抽象提取验证票证信息
 * onValidate   ->   status
 *                   1: 通过
 *                  -1: 人脸验证不通过
 *                  -2: 信息不完整不通过
 *                  -3: 清理信息
 *                  其他状态是票据信息的不同过状态
 */
class ValidateTicket(val udpDevice: UdpDevice?, val mFaceHelper: FaceHelper,
                     val onValidate: (status: String, message: String, picPath: String?,
                                      name: String?, currentInfo: Ticket,time: String?) -> Unit) {
    //只处理一条信息
    private val queue: LinkedBlockingDeque<Ticket> = LinkedBlockingDeque(1)
    private val ticketRepository = TicketRepository()
    private val apiModel = ApiModel(ticketRepository)
    //缓存当前信息
    private val currentTicket = Ticket(null, null, null)
    //上次验证时间，两次验证时间有个间隔
    private var lastValidateTime = 0L
    //两次验证间隔时间
    private val rate = 2500L
    private var alive = true

    private var passedTicket = Ticket()


    private var scheduleTicketInfoThread = thread(name = "scheduleTicketInfo") {
        while (alive) {
            var ticket = Ticket(null,null,null)
            try {
                ticket = queue.take()
            }catch (ie:InterruptedException){
                Log.e("ValidateTicket","票据验证线程被中断")
            }
            //清除上一个用户制造的信息
            if (ticket.qrCode == passedTicket.qrCode || ticket.m1 == passedTicket.m1) continue
            //需要二维码加芯片时，有可能有人只刷了一种信息就离开了会有遗留信息需要清除
            resetScanInfoDelay()
            //先检测猫眼的验证是否通过
            if (ticket.m1 == STATUS_0 && ticket.m1 == STATUS_4) {
                onValidate("0", "无效票证", null, null, currentTicket, null)
                continue
            }
            clearCurrentTicketInfo(ticket)
            //验证是否信息完整
            val (pass, msg) = validateCurrentInfo(currentTicket.m1, currentTicket.qrCode, currentTicket.idCard)
            if (pass) {
                //通过间隔时间,过滤太频繁的数据
                val currentTime = System.currentTimeMillis()
                if (currentTime - lastValidateTime < rate) {
                    continue
                } else {
                    lastValidateTime = currentTime
                }
                validate(currentTicket.m1, currentTicket.qrCode, currentTicket.idCard)
            } else {
                onValidate("-2", msg, null, null, currentTicket, null)
            }
        }
    }

    fun putTicket(ticket: Ticket) {
        queue.offer(ticket, 0, TimeUnit.MILLISECONDS)
    }

    //走服务器验证
    private fun validate(rfid: String?, qrCode: String?, idCard: IDCard?) {

        when (Config.scanType) {
            SCAN_TYPE_IDCARD_AND_QRCODE, SCAN_TYPE_IDCARD,SCAN_TYPE_IDCARD_OR_RFID, SCAN_TYPE_IDCARD_OR_RFID_HIGH -> {
                //身份证或票证当身份证是空票不是空时就验票
                if(Config.scanType == SCAN_TYPE_IDCARD_OR_RFID || Config.scanType == SCAN_TYPE_IDCARD_OR_RFID_HIGH){
                    if (rfid != null) {
                        apiModel.validateByServer(validateType = Config.scanType, rfid = rfid, data = ConvertUtils.bitmap2Bytes(mFaceHelper.captureFrame(), Bitmap.CompressFormat.JPEG), qrcode = qrCode, idCard = idCard)
                        { success, path, status, message, name, time ->
                            handleServerResult(success, path, status, message, name, time)
                        }
                        return
                    }
                }
                val bitmap: Bitmap?
                var facePosition = FacePosition()
                var message = "人脸不匹配，请调整姿态"
                if (Config.compareIdcardFace) { //需要对比身份证中的图片
                    bitmap = mFaceHelper.compareIDCard(idCard!!.photoData)
                } else if (!Config.captureNeedFace) { //不需要获取有人脸的图片
                    message = "该信息永远不会出现"
                    bitmap = mFaceHelper.captureFrame()
                } else {
                    message = "获取人脸特征失败请调整姿态"
                    val cpR = mFaceHelper.captureFace()
                    bitmap = cpR?.first
                    facePosition = cpR?.third ?: facePosition
                }
                if (bitmap == null) {
                    onValidate("-1", message, null, null, currentTicket,null)
                } else {
                    if (Config.faceValidateType ==  VALIDATE_FACE_TYPE_ALY) {
                        apiModel.validateByServer(validateType = Config.scanType, rfid = rfid,
                                data = ConvertUtils.bitmap2Bytes(bitmap, Bitmap.CompressFormat.JPEG),
                                qrcode = qrCode, idCard = idCard,
                                facePosition = facePosition
                        )
                        { success, path, status, message, name, time ->
                            handleServerResult(success, path, status, message, name, time)
                        }
                    } else if (Config.faceValidateType ==  VALIDATE_FACE_TYPE_LOCAL){
                        bitmap.compress(Bitmap.CompressFormat.PNG,100,FileOutputStream(PathUtils.getExternalStoragePath()+File.separator+"test1.png"))
                        onValidate("1", "对比通过", null, null, currentTicket,null)
                    }
                }
            }
            else -> {
                var futureData: ByteArray? = null
                var facePosition = FacePosition()
                val bitmap = if (!Config.captureNeedFace) {     //不需要获取有人脸的图片
                    mFaceHelper.captureFrame()
                } else {                                        //需要获取有人脸的图片
                    val triple = mFaceHelper.captureFace()
                    futureData = triple?.second
                    facePosition = triple?.third ?: facePosition
                    triple?.first
                }
                if (null == bitmap) {
                    onValidate("-1", "获取人脸特征失败请调整姿态", null, null, currentTicket,null)
                } else {
                    //通过本地验证人脸
                    if ((Config.scanType == SCAN_TYPE_RFID_ONLY || Config.scanType == SCAN_TYPE_RFID_ONLY_HIGH)
                            && Config.faceValidateType == VALIDATE_FACE_TYPE_LOCAL && Config.captureNeedFace) {
                        validateFaceByLocal(rfid!!,bitmap,futureData){
                            if(it) {
                                if (Config.uploadValidateInfo){
                                    TODO("需要局域网验证上传的API")
                                }
                                if (Config.uploadChenduPolice) ApiModel.uploadGongAn(ConvertUtils.bitmap2Bytes(bitmap, Bitmap.CompressFormat.JPEG),Config.deviceName!!)
                            }
                        }
                    }
                    //通过服务器验证
                    else{
                        apiModel.validateByServer(validateType = Config.scanType, rfid = rfid,
                                data = ConvertUtils.bitmap2Bytes(bitmap, Bitmap.CompressFormat.JPEG), qrcode = qrCode,
                                facePosition = facePosition)
                        { success, path, status, message, name,time -> handleServerResult(success, path, status, message, name,time) }
                    }
                }
            }
        }
    }





    //通过本地人脸识别验证
    private fun validateFaceByLocal(m1src: String, bitmap: Bitmap?, futureData: ByteArray?,result:(Boolean)->Unit) {
        val m1= if (m1src.length>12) m1src.subSequence(0,8).toString() else m1src
        //保存人脸到本地
        val filename = File(Config.facePictureDir,"$m1.png")
        val worker = ticketRepository.getWorkerTicket(m1)
        if(!Config.isTest && worker!=null){

            //有人脸就验证人脸
            if (worker.future != null) {
                if(mFaceHelper.compareFutureData(listOf(worker.future!!))!=null){

                    PhotoSocketGetter.getPhoto(ip = worker.ip!!,m1 = worker.ID){
                        onValidate("1", "验证通过", it, null, currentTicket, null)
                        result(true)
                        bitmap?.let { FileOutputStream(filename).use { out -> bitmap.compress(Bitmap.CompressFormat.PNG, 80, out) } }
                    }
                }else{
                    PhotoSocketGetter.getPhoto(ip = worker.ip!!, m1 = worker.ID){
                        onValidate("-1", "人脸不匹配", it, null, currentTicket,null)
                        result(true)
                    }
                    result(false)
                }
            }
            //没人脸就存人脸
            else {
                bitmap?.let { FileOutputStream(filename).use { out -> bitmap.compress(Bitmap.CompressFormat.PNG, 80, out) } }
                onValidate("1", "验证通过", null, null, currentTicket, null)
                worker.future = futureData
                worker.ip = NetworkUtils.getIPAddress(true)
                ticketRepository.updateWorkerTicket(worker)
                UDPService.syncFuture(id = m1,ip =  NetworkUtils.getIPAddress(true),future =  futureData!!)
                result(true)
            }
        } else if (!ticketRepository.searchLocalTicket(m1)) {
            udpDevice?.syncTicket(m1 = m1)
            ticketRepository.insertLocalTicket(m1 = m1)
            bitmap?.let { FileOutputStream(filename).use { out -> bitmap.compress(Bitmap.CompressFormat.PNG, 80, out)} }
            onValidate("1", "验证通过", null, null, currentTicket,null)
            result(true)
        } else {
            onValidate("0", "无效或已经使用票证", null, null, currentTicket,null)
            result(false)
        }
    }


    private fun handleServerResult(success: Boolean, path: String?, status: String, message: String, name: String?,time: String?) {
        if (success) {
            passedTicket = currentTicket
            onValidate("1", "验证通过", path, name, currentTicket,time)
            currentTicket.clearInfo()
        } else if (status == "8") {
            //处理连接超时的情况
            passedTicket = currentTicket
            onValidate("1", "验证通过", path, name, currentTicket,time)
            currentTicket.clearInfo()
            ToastUtils.showShort(message)
        } else if (!Config.captureNeedFace && ("5" == status || "6" == status)) {//处理不需要人脸而后台却检测了人脸的情况
            passedTicket = currentTicket
            onValidate("1", "验证通过", path, name, currentTicket,time)
        } else {
            onValidate(status, message, path, name, currentTicket,time)
        }
    }


    /**
     * 在需要两种信息的时候，需要清除上一次的信息
     * 具体情况是，上一次的信息无法通过保存在内存中，但是下一个人来的时候用到上一个人的信息
     * 清理方法是，若两种信息都在，对比这次信息和上次信息，若不同就把另一种信息清掉
     * @return 是否清理了信息
     */
    private fun clearCurrentTicketInfo(ticket: Ticket): Boolean {
        var result = false
        //清除上一次遗留信息
        when (Config.scanType) {
            SCAN_TYPE_IDCARD_AND_QRCODE -> {
                //只有两个信息都在的情况才需要清理
                if (currentTicket.idCard != null && currentTicket.qrCode != null) {
                    if (ticket.qrCode != null && currentTicket.qrCode != ticket.qrCode) {
                        currentTicket.idCard = null
                        result = true
                    }
                    if (ticket.idCard != null && currentTicket.idCard!!.id != ticket.idCard!!.id) {
                        currentTicket.qrCode = null
                        result = true
                    }
                }
                //保证先刷身份证再刷二维码，先刷二维码时不保存二维码
                if (ticket.idCard != null) currentTicket.idCard = ticket.idCard
                if (ticket.qrCode != null && currentTicket.idCard != null) {
                    currentTicket.qrCode = ticket.qrCode
                }
            }
            SCAN_TYPE_RFID_AND_QRCODE, SCAN_TYPE_RFID_AND_QRCODE_HIGH -> {
                if (currentTicket.m1 != null && currentTicket.qrCode != null) {
                    if (ticket.qrCode != null && currentTicket.qrCode != ticket.qrCode) {
                        currentTicket.m1 = null
                        result = true
                    }
                    if (ticket.m1 != null && currentTicket.m1 != ticket.m1) {
                        currentTicket.qrCode = null
                        result = true
                    }
                }
                //todo 芯片+二维码是否有先后顺序
                if (ticket.qrCode != null) currentTicket.qrCode = ticket.qrCode
                if (ticket.m1 != null) currentTicket.m1 = ticket.m1
            }
            SCAN_TYPE_RFID_OR_QRCODE, SCAN_TYPE_RFID_OR_QRCODE_HIGH -> {
                //清除遗留信息
                if (ticket.qrCode != null) {
                    currentTicket.m1 = null
                    currentTicket.qrCode = ticket.qrCode
                }
                if (ticket.m1 != null) {
                    currentTicket.qrCode = null
                    currentTicket.m1 = ticket.m1
                }
            }
            SCAN_TYPE_IDCARD_OR_RFID, SCAN_TYPE_IDCARD_OR_RFID_HIGH->{
                //清除遗留信息
                if (ticket.idCard != null) {
                    currentTicket.m1 = null
                    currentTicket.idCard = ticket.idCard
                }
                if (ticket.m1 != null) {
                    currentTicket.idCard = null
                    currentTicket.m1 = ticket.m1
                }
            }
            else -> {
                if (ticket.idCard != null) currentTicket.idCard = ticket.idCard
                if (ticket.qrCode != null) currentTicket.qrCode = ticket.qrCode
                if (ticket.m1 != null) currentTicket.m1 = ticket.m1
            }
        }
        return result
    }

    /**
     *  验证当前扫码信息是否齐全
     */
    private fun validateCurrentInfo(rfid: String?, qrCode: String?, idCard: IDCard?): Pair<Boolean, String> {
        when (Config.scanType) {
            SCAN_TYPE_RFID_OR_QRCODE, SCAN_TYPE_RFID_OR_QRCODE_HIGH -> if (rfid == null && qrCode == null) return Pair(false, "")
            SCAN_TYPE_RFID_ONLY, SCAN_TYPE_RFID_ONLY_HIGH -> if (rfid == null) return Pair(false, "")
            SCAN_TYPE_QRCODE_ONLY -> if (qrCode == null) return Pair(false, "")
            SCAN_TYPE_IDCARD -> if (idCard == null) return Pair(false, "")
            SCAN_TYPE_RFID_AND_QRCODE, SCAN_TYPE_RFID_AND_QRCODE_HIGH -> {
                if (qrCode == null) {
                    return Pair(false, "请刷二维码")
                }
                if (rfid == null) {
                    return Pair(false, "请刷票")
                }
            }
            SCAN_TYPE_IDCARD_AND_QRCODE -> {

                if (idCard == null) {
                    if (qrCode == null) {
                        return Pair(false, "请先刷身份证")
                    }
                    return Pair(false, "请刷身份证")
                }else{
                    if (qrCode == null) {
                        return Pair(false, "请刷二维码")
                    }
                }
            }
            SCAN_TYPE_IDCARD_OR_RFID, SCAN_TYPE_IDCARD_OR_RFID_HIGH -> if (rfid == null && idCard == null) return Pair(false, "")
        }
        return Pair(true, "")
    }

    private var resetUITask: TimerTask? = null

    /**
     * 4s后重置信息
     */
    private fun resetScanInfoDelay() {
        resetUITask?.cancel()
        resetUITask = timerTask {
            currentTicket.m1 = null
            currentTicket.idCard = null
            currentTicket.qrCode = null
        }
        Timer().schedule(resetUITask, 4000) //4s未读取到信息，就清空数据
    }

    fun release() {
        alive = false
        if (scheduleTicketInfoThread.isAlive)
            scheduleTicketInfoThread.interrupt()
    }

}