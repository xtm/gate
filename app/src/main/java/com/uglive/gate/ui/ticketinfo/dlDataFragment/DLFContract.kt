package com.uglive.gate.ui.ticketinfo.dlDataFragment

import com.uglive.gate.BasePresenter
import com.uglive.gate.bean.TicketBean
import hxzy.ptb.hxzyappkit.BaseView

/**
 * Created by xiantm on 2018/5/21.
 */
interface DLFContract {

    interface View : BaseView<Presenter> {


    }

    interface Presenter : BasePresenter {
        fun queryData(page: Int, size: Int, qrcode: String, rfid: String): List<TicketBean>
    }
}