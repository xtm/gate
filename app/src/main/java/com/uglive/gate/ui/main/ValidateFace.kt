package com.uglive.gate.ui.main

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.arcsoft.face.ErrorInfo
import com.arcsoft.face.FaceEngine
import com.arcsoft.face.FaceFeature
import com.arcsoft.face.FaceSimilar
import com.blankj.utilcode.util.ConvertUtils
import com.blankj.utilcode.util.NetworkUtils
import com.blankj.utilcode.util.ToastUtils
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.bean.Face
import com.uglive.gate.db.localDB.LocalDB
import com.uglive.gate.face.FaceHelper
import com.uglive.gate.model.ApiModel
import com.uglive.gate.model.VALIDATE_FACE_TYPE_ALY
import com.uglive.gate.repositroy.TicketRepository
import com.uglive.gate.shareData.PhotoSocketGetter
import com.uglive.gate.shareData.UDPService
import com.uglive.gate.shareData.bean.SocketGetInfoMessage
import java.io.File
import java.io.FileOutputStream
import java.util.concurrent.*

class ValidateFace(context: Context,
                   private val mFaceHelper: FaceHelper,
                   onSuccess: (Boolean, Bitmap?) -> Unit,
                   onFailed: (String, String) -> Unit) {

    companion object {

        val faces = CopyOnWriteArrayList<Face>()
        /**
         * 删除人脸
         */
        fun deleteFaceByRfid(rfid: String, context: Context) {
            val indexes = mutableListOf<Int>()
            val findFaces = faces.filterIndexed { index, face ->
                if (face.ticketId == rfid) {
                    indexes.add(index)
                    return@filterIndexed true
                }
                return@filterIndexed false
            }
            indexes.forEach { faces.removeAt(it) }
            AppExecutors.diskIO.execute {
                findFaces.forEach {
                    LocalDB.get(context).faceDao().deleteByName(it.fileName)
                    val file = File(Config.faceSourceDir, it.fileName)
                    if (file.exists()) file.delete()
                }
            }
            if (indexes.isEmpty()) {
                ToastUtils.showShort("'$rfid '没有录入人脸")
            } else {
                ToastUtils.showShort("'$rfid '的人脸已经删除")
            }
        }
    }

    private val api = ApiModel(TicketRepository())

    private val rootDirPath = Config.faceSourceDir.absolutePath + File.separator
    //对比阈值
    private val score = 1 - Config.faceDegree
    //获取票据信息
    private val queue: LinkedBlockingDeque<String> = LinkedBlockingDeque(1)
    //获取二维码信息
    private val qrCodeQueue: LinkedBlockingQueue<String> = LinkedBlockingQueue(1)

    private val faceDao = LocalDB.get(context).faceDao()

    private val qrcode: StringBuffer = StringBuffer()

    //正在执行的人脸对比任务
    private var future: Future<Unit>? = null

    private val resetExecutorService = Executors.newSingleThreadExecutor()
    private var cancelTask: FutureTask<Unit>? = null

    //是否执行人脸人脸对比任务
    @Volatile
    private var toggleToCompare = true

    private var compareThreadIsRun = true

    private val getQrCodeThread = Thread(Runnable {
        try {
            while (true) {
                qrcode.append(qrCodeQueue.take())
                clearQrcode()
            }
        } catch (e: InterruptedException) {
            //pass
        }
    }, "ValidateFace-GetQrCode")

    private fun clearQrcode() {
        cancelTask?.cancel(true)
        cancelTask = FutureTask(Callable<Unit> {
            Thread.sleep(3000)
            qrcode.delete(0, qrcode.length)
            cancelTask = null
        })
        resetExecutorService.submit(cancelTask)
    }

    private val getTicketThread = Thread(Runnable {
        try {

            while (true) {

                val ticketId = queue.take()
                if (qrcode.isNotEmpty()) {
                    qrcode.delete(0, qrcode.length)
                    cancelTask?.cancel(true)
                    UDPService.deleteFace(ticketId)
                    continue
                }
                toggleToCompare = false
                future?.cancel(true)
                val ticketFaces = faceDao.selectByTicketId(ticketId)

                //是否扫描卡的时候对比卡里面的人脸
                if (ticketFaces.isNotEmpty()) {
                    //以前有人脸的就直接验证人脸
                    mFaceHelper.compareFutureData(ticketFaces.map { it.feature })?.let {
                        val (bitmap, _, position) = it
                        api.upLoadFace(ticketId, ConvertUtils.bitmap2Bytes(bitmap, Bitmap.CompressFormat.JPEG), position)
                        onSuccess(true, BitmapFactory.decodeFile(rootDirPath + ticketFaces.first().fileName))
                    }
                            ?: onSuccess(false, BitmapFactory.decodeFile(rootDirPath + ticketFaces.first().fileName))
                } else {
                    //判断该芯片是否是本场活动的芯片

                    val (status, message) = if (Config.faceValidateType == VALIDATE_FACE_TYPE_ALY)
                        api.queryTicketId(ticketId) else Pair("1", "ok")
                    if (status == "1" || status == "8") {
                        //录入人脸
                        mFaceHelper.captureFace()?.let {
                            val picName = "$ticketId-${System.currentTimeMillis()}.png"
                            it.first.compress(Bitmap.CompressFormat.PNG, 100, FileOutputStream(File(Config.faceSourceDir, picName)))
                            val newFace = Face(fileName = picName, feature = it.second, ticketId = ticketId)
                            faceDao.insert(newFace)
                            //同步信息
                            UDPService.syncFuture(id = ticketId, ip = NetworkUtils.getIPAddress(true), filename = picName, future = it.second)
                            faces.add(newFace)
                            onSuccess(true, it.first)
                            if (Config.faceValidateType == VALIDATE_FACE_TYPE_ALY)
                                api.upLoadFace(ticketId, ConvertUtils.bitmap2Bytes(it.first, Bitmap.CompressFormat.JPEG), it.third)
                        } ?: onSuccess(false, null)
                    } else {
                        onFailed(status, message)
                    }
                }
                toggleToCompare = true
            }
        } catch (e: InterruptedException) {
            //pass
        }
    }, "ValidateFace-GetTicket")

    private val compareThread = Thread(Runnable {
        //验证通过间隔2秒
        var previousPassTime = 0L
        try {
            while (compareThreadIsRun) {
                if (toggleToCompare && System.currentTimeMillis() - previousPassTime > 2500) {
                    val (data, aftFaceList) = mFaceHelper.getFrameData()
                    val faceFeature = FaceFeature()
                    if (aftFaceList.isEmpty()) continue
                    val resultCode = mFaceHelper.extractFaceFeature(data, FaceEngine.CP_PAF_NV21, aftFaceList.first(), faceFeature)
                    if (resultCode == ErrorInfo.MOK) {
                        val faceSimilar = FaceSimilar()
                        for ((index, face) in faces.withIndex()) {
                            val similarResultCode = mFaceHelper.compareFeature(FaceFeature(face.feature), faceFeature, faceSimilar)
                            if (similarResultCode == ErrorInfo.MOK && faceSimilar.score > score) {
                                val picFile = File(Config.faceSourceDir.absolutePath, faces[index].fileName)
                                val (currentBitmap, position) = mFaceHelper.cropBitmap(data, aftFaceList.first().rect)
                                api.upLoadFace(face.ticketId, ConvertUtils.bitmap2Bytes(currentBitmap, Bitmap.CompressFormat.JPEG), position)
                                if (picFile.exists()) {
                                    onSuccess(true, BitmapFactory.decodeFile(picFile.absolutePath))
                                } else {
                                    PhotoSocketGetter.getPhoto(SocketGetInfoMessage.TYPE_FACE_GET_PHOTO, face.picSrcIp, face.ticketId, face.fileName) {
                                        if (it == null) {
                                            onSuccess(true, null)
                                        } else {
                                            onSuccess(true, BitmapFactory.decodeFile(it))
                                        }
                                    }
                                }
                                previousPassTime = System.currentTimeMillis()
                            }
                        }
                    }
                }
                Thread.sleep(100)
            }
        } catch (e: InterruptedException) {
            //pass
        }
    }, "ValidateFace-Compare")


    fun start() {
        AppExecutors.diskIO.submit { faces.addAll(faceDao.getAll()) }
        compareThread.start()
        getQrCodeThread.start()
        getTicketThread.start()
    }

    //推入票据信息
    fun putTicket(ticketId: String) = queue.offer(ticketId, 0, TimeUnit.MILLISECONDS)

    //推入二维码信息
    fun putQrCode(qrcode: String) = qrCodeQueue.offer(qrcode, 0, TimeUnit.MILLISECONDS)

    fun close() {
        compareThreadIsRun = false
        toggleToCompare = false
        cancelTask?.cancel(true)
        getQrCodeThread.interrupt()

    }
}