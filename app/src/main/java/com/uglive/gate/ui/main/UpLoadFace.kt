package com.uglive.gate.ui.main

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.arcsoft.face.ErrorInfo
import com.arcsoft.face.FaceEngine
import com.arcsoft.face.FaceFeature
import com.arcsoft.face.FaceSimilar
import com.blankj.utilcode.util.ConvertUtils
import com.blankj.utilcode.util.ToastUtils
import com.github.kittinunf.fuel.core.Blob
import com.github.kittinunf.fuel.httpUpload
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.bean.Face
import com.uglive.gate.db.localDB.LocalDB
import com.uglive.gate.face.FaceHelper
import com.uglive.gate.model.ApiModel
import com.uglive.gate.repositroy.TicketRepository
import com.uglive.gate.shareData.PhotoSocketGetter
import com.uglive.gate.shareData.bean.SocketGetInfoMessage
import java.io.File
import java.util.concurrent.*

class UpLoadFace(context: Context, private val mFaceHelper: FaceHelper, onSuccess: (Boolean, Bitmap?) -> Unit, onFailed:(String, String) -> Unit) {

    companion object {
        val faces = CopyOnWriteArrayList<Face>()

        /**
         * 删除人脸
         */
        fun deleteFaceByRfid(rfid: String,context: Context) {
            val indexes = mutableListOf<Int>()
            val findFaces = faces.filterIndexed { index, face ->
                if (face.ticketId == rfid) {
                    indexes.add(index)
                    return@filterIndexed true
                }
                return@filterIndexed false
            }
            indexes.forEach { faces.removeAt(it) }
            AppExecutors.diskIO.execute {
                findFaces.forEach {
                    LocalDB.get(context).faceDao().deleteByName(it.fileName)
                    val file = File(Config.faceSourceDir,it.fileName)
                    if (file.exists()) file.delete()
                }
            }
            if (indexes.isEmpty()) {
                ToastUtils.showShort("'$rfid '没有录入人脸")
            } else {
                ToastUtils.showShort("'$rfid '的人脸已经删除")
            }
        }
    }

    private val api = ApiModel(TicketRepository())

    private val rootDirPath = Config.faceSourceDir.absolutePath + File.separator
    //对比阈值
    private val score = 1 - Config.faceDegree
    //执行对比任务
    private val compareExe: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()

    //获取票据信息
    private val queue: LinkedBlockingDeque<String> = LinkedBlockingDeque(1)
    private val faceDao = LocalDB.get(context).faceDao()

    //正在执行的人脸对比任务
    private var future: Future<Unit>? = null

    //是否执行人脸人脸对比任务
    @Volatile
    private var toggleToCompare = true

    private var futureData: ByteArray? = null
    private var lastTime = System.currentTimeMillis()

    private val getTicketThread = Thread(Runnable {
        try {

            while (true) {
                mFaceHelper.captureFace()?.let {
                    val (bitmap,fut) = it

                    val data = ConvertUtils.bitmap2Bytes(bitmap, Bitmap.CompressFormat.JPEG)
                    if (System.currentTimeMillis() - lastTime > 5000 || futureData == null){
                        lastTime = System.currentTimeMillis()
                        futureData = fut

                        val req = "http://${Config.IP}:50730/getPhoto"
                                .httpUpload()
                                .timeout(1500)
                        req.blob { r, _ ->
                            r.mediaTypes.add("image/jpeg")
                            Blob("face", data.size.toLong()) { data.inputStream() }
                        }
                                .name { "face" }
                        req.responseString { _, _, result ->
                            result.fold({ Log.e("upload",it) }) { Log.e("upload","人脸上传失败") }
                        }
                    }else{
                        futureData?.let {
                            val faceSimilar = FaceSimilar()
                            val similarResultCode = mFaceHelper.compareFeature(FaceFeature(it), FaceFeature(fut), faceSimilar)
                            if (similarResultCode == ErrorInfo.MOK && faceSimilar.score > score) {
                                Log.e("upload","相似人脸")
                            }else{
                                val req = "http://${Config.IP}:50730/getPhoto"
                                        .httpUpload()
                                        .timeout(1500)
                                req.blob { r, _ ->
                                    r.mediaTypes.add("image/jpeg")
                                    Blob("face", data.size.toLong()) { data.inputStream() }
                                }
                                        .name { "face" }
                                req.responseString { _, _, result ->
                                    result.fold({ Log.e("upload",it) }) { Log.e("upload","人脸上传失败") }
                                }
                                lastTime = System.currentTimeMillis()
                                futureData = fut
                            }
                        }
                    }

                } ?: onSuccess(false, null)
            }
        } catch (e: InterruptedException) {
            //pass
        }
    }, "ValidateFace-GetTicket")

    private val compareThread = Thread(Runnable {
        //验证通过间隔2秒
        var previousPassTime = 0L
        while (true) {
            if (toggleToCompare && System.currentTimeMillis() - previousPassTime > 2500) {
                //若事务已完成则开启新事务
                future = compareExe.submit(Callable<Unit> {
                    mFaceHelper.getFrameData()
                    while (true) {
                        val (data, aftFaceList) = mFaceHelper.getFrameData()
                        val faceFeature = FaceFeature()
                        val resultCode = mFaceHelper.extractFaceFeature(data, FaceEngine.CP_PAF_NV21, aftFaceList.first(), faceFeature)
                        if (resultCode == ErrorInfo.MOK) {
                            val faceSimilar = FaceSimilar()
                            for ((index, face) in faces.withIndex()) {
                                val similarResultCode = mFaceHelper.compareFeature(FaceFeature(face.feature), faceFeature, faceSimilar)
                                if (similarResultCode == ErrorInfo.MOK && faceSimilar.score > score) {
                                    val picFile = File(Config.faceSourceDir.absolutePath, faces[index].fileName)
                                    val (currentBitmap,position) = mFaceHelper.cropBitmap(data,aftFaceList.first().rect)
                                    api.upLoadFace(face.ticketId, ConvertUtils.bitmap2Bytes(currentBitmap, Bitmap.CompressFormat.JPEG), position)
                                    if (picFile.exists()) {
                                        onSuccess(true, BitmapFactory.decodeFile(picFile.absolutePath))
                                    } else {
                                        PhotoSocketGetter.getPhoto(SocketGetInfoMessage.TYPE_FACE_GET_PHOTO, face.picSrcIp, face.ticketId, face.fileName) {
                                            if (it == null) {
                                                onSuccess(true, null)
                                            } else {
                                                onSuccess(true, BitmapFactory.decodeFile(it))
                                            }
                                        }
                                    }
                                    previousPassTime = System.currentTimeMillis()
                                    return@Callable
                                }
                            }
                        }
                    }
                })
                try {
                    future?.get()
                } catch (e: Exception) {
                    //ignore Exception cause by "ValidateFace-GetTicket" thread
                }

            }
            try {
                Thread.sleep(300)
            } catch (e: InterruptedException) {
                //pass
            }

        }
    }, "ValidateFace-Compare")



    fun start() {
//        AppExecutors.diskIO.submit { faces.addAll(faceDao.getAll()) }
//        compareThread.start()
        getTicketThread.start()
    }

    //推入票据信息
    fun putTicket(ticketId: String) = queue.offer(ticketId, 0, TimeUnit.MILLISECONDS)

    fun close() {
//        toggleToCompare = false
//        future?.cancel(true)
//        compareExe.shutdownNow()
//        compareThread.interrupt()
        getTicketThread.interrupt()
    }
}