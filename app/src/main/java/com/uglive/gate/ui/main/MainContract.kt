package com.uglive.gate.ui.main

import android.graphics.Bitmap
import com.decard.entitys.IDCard
import com.uglive.gate.BasePresenter
import com.uglive.gate.face.FaceHelper
import hxzy.ptb.hxzyappkit.BaseView

interface MainContract {

    interface View : BaseView<Presenter> {

        fun initView()

        /**
         * 提示信息
         * @message 信息
         * @color 0：成功色，1：警告色，2：失败色
         */
        fun updateMessage(message: String, color: Int)

        /**
         * 扫码信息
         */
        fun updateTicketInfo(ticketNum: String)


        fun updateStatusTip(pass: Boolean)

        /**
         * 重置UI
         */
        fun resetUI()

        /**
         * 重置警告信息
         */
        fun resetMessage()

        /**
         * 设置会场名称
         */
        fun updatePlaceName(name: String)

        /**
         * 更新二维码
         */
        fun updateQrcode(qrCode: String)

        /**
         * 预览捕获到的图片
         */
        fun updateCapturePictureImageView(bitmap: Bitmap)

        fun updateCapturePictureImageView(path: String)

        fun updateCapturePictureImageView(resourceId: Int)

        /**
         * 身份证信息
         */
        fun updateIdCard(idCard: IDCard)

        fun showProcess(process: Int, max: Int)
        //活动海报图片
        fun updateAdImage(filePath: String)
        //活动地址
        fun updateAdTitle(title: String)
        //扫码人姓名
        fun updateName(name:String)

        fun updateLastInTime(time:String)

        fun setInCount(count: Int)
    }

    interface Presenter : BasePresenter {
        /**
         * 删除以前的数据，获取活动票数据
         */
        fun getTicketInfo(activityId: String)
        //开启外设
        fun openDevice(view:android.view.View)
        //关闭外设
        fun closeDevice()

        fun setFaceHelper(faceHelper: FaceHelper)

        fun deleteFace(rfid: String)

        /**
         * 重置数据库数据
         */
        fun restTestData()
    }
}