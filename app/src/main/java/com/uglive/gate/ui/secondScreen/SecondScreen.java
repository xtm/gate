package com.uglive.gate.ui.secondScreen;

import android.app.Presentation;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;

import com.uglive.gate.R;

public class SecondScreen extends Presentation {

        public SecondScreen(Context outerContext, Display display) {
            super(outerContext,display);

        }
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.layout_second_screen);

        }
    }
