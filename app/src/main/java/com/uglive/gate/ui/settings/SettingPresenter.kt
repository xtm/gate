package com.uglive.gate.ui.settings

import com.blankj.utilcode.util.SPUtils
import com.uglive.gate.Config
import com.uglive.gate.model.ConfigModel

class SettingPresenter(val view: SettingContract.View, val configModel: ConfigModel) : SettingContract.Presenter {
    override fun saveConfigs(ip: String, type: Int, secret: String, deviceName: String, uploadValidateInfo: Boolean, projcetId: String, faceValidateType: Int, faceDegree: Float, cameraScale: Float, gonganIp: String, compareIdCadFace: Boolean, captureNeedFace: Boolean, uploadChenduPolice: Boolean, needSecret: Boolean, useKeyboardQrcodeDevice: Boolean, encByFactory: Boolean, pushStream: Boolean, faceScale: Int, faceSize: Int, cameraMirror: Boolean, rectMirror: Boolean) {
        configModel.ip = ip
        configModel.scanType = type
        configModel.secret = secret
        configModel.deviceName = deviceName
        configModel.faceValidateType = faceValidateType
        configModel.faceDegree = faceDegree
        configModel.uploadValidateInfo = uploadValidateInfo
        configModel.projectId = projcetId
        configModel.cameraScale = cameraScale
        configModel.gonganIp = gonganIp
        configModel.compareIdcardFace = compareIdCadFace
        configModel.captureNeedFace = captureNeedFace
        configModel.uploadChenduPolice = uploadChenduPolice
        configModel.needSecret = needSecret
        configModel.useKeyboardScanQrcodeDevice = useKeyboardQrcodeDevice
        configModel.encByFactory = encByFactory
        configModel.pushStream = pushStream
        configModel.faceScale = faceScale
        configModel.faceSize = faceSize
        configModel.cameraMirror = cameraMirror
        configModel.rectMirror = rectMirror

        Config.IP = ip
        Config.scanType = type
        Config.secretKey = secret
        Config.deviceName = deviceName
        Config.faceValidateType = faceValidateType
        Config.uploadValidateInfo = uploadValidateInfo
        Config.projectId = projcetId
        Config.cameraScale = cameraScale
        Config.gonganIp = gonganIp
        Config.compareIdcardFace = compareIdCadFace
        Config.captureNeedFace = captureNeedFace
        Config.uploadChenduPolice = uploadChenduPolice
        Config.needSecret = needSecret
        Config.useKeyboardScanQrcodeDevice = useKeyboardQrcodeDevice
        Config.encByFactory = encByFactory
        Config.faceScale = faceScale
        Config.faceSize = faceSize
        Config.cameraMirror = cameraMirror
        Config.rectMirror = rectMirror

        //保存时候清楚进入人数
        SPUtils.getInstance().put(Config.IN_COUNT,0)
    }

    override fun start() {
        view.initViewData(configModel.ip, configModel.scanType, configModel.secret, configModel.deviceName,
                configModel.uploadValidateInfo, configModel.projectId, configModel.faceValidateType,
                configModel.faceDegree, configModel.cameraScale, configModel.gonganIp,
                configModel.compareIdcardFace, configModel.captureNeedFace,
                configModel.uploadChenduPolice, configModel.needSecret,configModel.useKeyboardScanQrcodeDevice,
                configModel.encByFactory,configModel.pushStream, configModel.faceScale,configModel.faceSize,
                configModel.cameraMirror, configModel.rectMirror)
    }
}