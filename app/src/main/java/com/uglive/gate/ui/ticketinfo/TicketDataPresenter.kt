package com.uglive.gate.ui.ticketinfo

import com.blankj.utilcode.util.Utils
import com.uglive.gate.App
import com.uglive.gate.AppExecutors

/**
 * Created by xiantm on 2018/5/21.
 */
class TicketDataPresenter(val view:TicketDataContract.View):TicketDataContract.Presenter {
    val ticketRepository =  (Utils.getApp() as App).getTicketRepository()

    override fun getTotal(){
        AppExecutors.diskIO.execute{
            val nowNum = ticketRepository.getTicketBeanSize()
            AppExecutors.mainThread.execute {view.showTotal(nowNum = nowNum,total = ticketRepository.total)}
        }
    }

    override fun start() {
        getTotal()
    }

}