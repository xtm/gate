package com.uglive.gate.ui.requestInfo

import com.blankj.utilcode.util.Utils
import com.uglive.gate.App
import com.uglive.gate.AppExecutors


class RequestInfoPresenter(val view: RequestInfoContract.View) : RequestInfoContract.Presenter {

    val apiModel = (Utils.getApp() as App).getApiModel()

    override fun start() = querySize()

    override fun querySize() {
        AppExecutors.diskIO.execute {
            val (size, total) = apiModel.getSize()
            AppExecutors.mainThread.execute { view.setInfo(size, total) }
        }
    }
}