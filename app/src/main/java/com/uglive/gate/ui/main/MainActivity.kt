package com.uglive.gate.ui.main


import android.Manifest
import android.annotation.TargetApi
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.ProgressDialog
import android.content.*
import android.graphics.Bitmap
import android.hardware.display.DisplayManager
import android.net.Uri
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import cn.carbs.android.avatarimageview.library.AvatarImageView
import com.blankj.utilcode.util.*
import com.decard.entitys.IDCard
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.R
import com.uglive.gate.bean.CameraPreviewSize
import com.uglive.gate.device.scanCard.INFCReciver
import com.uglive.gate.device.scanCard.OnNFCRecv
import com.uglive.gate.face.FaceHelper
import com.uglive.gate.model.*
import com.uglive.gate.remoteControl.connect.RECEIVER_ACTION_UPGRADE_APP
import com.uglive.gate.remoteControl.connect.RemoteControlService
import com.uglive.gate.ui.fingerDemo.FingerDemoActivity
import com.uglive.gate.ui.requestInfo.RequestInfoActivity
import com.uglive.gate.ui.secondScreen.SecondScreen
import com.uglive.gate.ui.settings.SettingActivity
import com.uglive.gate.ui.ticketinfo.TicketDataActivity
import com.uglive.gate.utils.AppUpgradeHttpManager
import com.vector.update_app_kotlin.updateApp
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.util.concurrent.ExecutionException
import java.util.concurrent.ScheduledExecutorService

class MainActivity : AppCompatActivity(), MainContract.View, INFCReciver {


    lateinit var mPresenter: MainContract.Presenter

    lateinit var mBitmap: Bitmap


    lateinit var mProgressDialog: ProgressDialog

    var isTablet: Boolean = false

    var netStateCheckTask: ScheduledExecutorService? = null
    private lateinit var faceHelper: FaceHelper
    private var mNfcAdapter: NfcAdapter? = null
    private var mPendingIntent: PendingIntent? = null

    private  var secondScreen: SecondScreen? = null

    private val updateReceiver = object :BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            updateApp("http://${Config.IP}:60000/apk/checkAndUpdateApp", AppUpgradeHttpManager()).update()
        }
    }

    //接受NFC扫描事件
    var mOnNFCRecv: OnNFCRecv? = null

    override fun setOnNFCRecv(onNFCRecv: OnNFCRecv) {
        mOnNFCRecv = onNFCRecv
    }

    private val usbMntReceiver = object : BroadcastReceiver(){
        private val TAG = "UsbBroadcastReceiver"

        private val addFaceDirName = "/addFace"

        private val replaceFaceDirName = "/replaceFace"

        private val appDirName = "/app"

        private val api19UsbPath = "/mnt/usb_storage"
        //todo pai23USBPath
        private val api23UsbPath = ""

        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent!!.action){
                Intent.ACTION_MEDIA_MOUNTED -> {
                    Log.i(TAG,"usb media mounted")
                    val isApi19 = DeviceUtils.getSDKVersionCode() == 19
                    val appPath = if (isApi19) "$api19UsbPath$appDirName" else "$api23UsbPath$appDirName"
                    val addFacePath = if (isApi19) "$api19UsbPath$addFaceDirName" else "$api23UsbPath$addFaceDirName"
                    val replaceFacePath = if (isApi19) "$api19UsbPath$replaceFaceDirName" else "$api23UsbPath$replaceFaceDirName"

                    //若识别到app文件夹且有
                    val appDir = File(appPath)
                    if (appDir.exists()){
                        val apkFiles = appDir.listFiles().filter { Regex("""^.+\.apk$""").matches(it.name) }
                        if (apkFiles.isNotEmpty()) {
                            val intent = Intent(Intent.ACTION_VIEW)
                            intent.setDataAndType(Uri.fromFile(apkFiles.first()), "application/vnd.android.package-archive")
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            context!!.startActivity(intent)
                            return
                        }
                    }
                    if (!Config.faceSourceDir.exists()){
                        Config.faceSourceDir.mkdirs()
                    }
                    val pd = ProgressDialog.show(this@MainActivity,"复制文件中","删除原始人脸图片")
                    //人脸替换通过人脸替换目录
                    val replaceFaceDir = File(replaceFacePath)
                    if ( replaceFaceDir.exists() && replaceFaceDir.list().isNotEmpty() ){

                        AppExecutors.diskIO.execute {
                            FileUtils.deleteDir(Config.faceSourceDir)
                            val files = replaceFaceDir.listFiles()
                            pd.setMessage("正在复制${files.size}张人脸图片")
                            files.forEach {
                                Log.i("CopyFile","copy -> ${it.name}")
                                try {
                                    it.copyTo(File(Config.faceSourceDir, it.name))
                                }catch (e:ExecutionException){
                                    Log.i("CopyFile","copy error",e)
                                }
                            }
                            runOnUiThread {
                                pd.dismiss()
                                restartApp(context!!)
                            }
                        }


                        return
                    }

                    //人脸添加通过人脸添加目录
                    val addFaceDir = File(addFacePath)
                    if (addFaceDir.exists() && addFaceDir.list().isNotEmpty() ){
                        AppExecutors.diskIO.execute {
                            val files = addFaceDir.listFiles()
                            pd.setMessage("正在复制${files.size}个人脸图片")
                            files.forEach {
                                it.copyTo(File(Config.faceSourceDir,it.name),true)
                            }
                            runOnUiThread {
                                pd.dismiss()
                                restartApp(context!!)
                            }
                        }
                        return
                    }

                }

                Intent.ACTION_MEDIA_UNMOUNTED->{
                    Log.i(TAG,"usb media unmounted")
                }
            }
        }
        private fun restartApp(context: Context){
            //2S后重启程序,因为需要重新配置日志发送模块
            val intent = context.packageManager.getLaunchIntentForPackage(context.applicationContext.packageName)
            val restartIntent = PendingIntent.getActivity(context.applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 3000, restartIntent)
            onDestroy()
        }
    }

    var mConnBinder: RemoteControlService.MyBinder? = null
    val mServiceConnection = object :ServiceConnection{
        override fun onServiceDisconnected(name: ComponentName?) {
            mConnBinder?.setOnConnectChangeListener(null)
            Log.e("MainActivity","disconnect RemoteControlService Maybe Dead")
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            mConnBinder = service as RemoteControlService.MyBinder
            mConnBinder?.setOnConnectChangeListener {
                if(it){
                    runOnUiThread { netStatus.setImageResource(R.drawable.signal_online)  }
                }else{
                    runOnUiThread { netStatus.setImageResource(R.drawable.sigal_offline) }
                }
            }
            mConnBinder?.setOnChangePushStream {
                if(it == "true") faceHelper.startStream()
                else faceHelper.stopStream()
            }
            mConnBinder?.setOnConfigExposure { runOnUiThread {  exposureCompensation.setSelection( it + 3 )} }
            mConnBinder?.setOnConfigWhiteBalance { runOnUiThread { whiteBalance.setSelection(it) } }
            mConnBinder?.setOnChangeNicknameListener { runOnUiThread { nickNameTv.text = it } }
            mConnBinder?.setOnDelFaceListener { mPresenter.deleteFace(it) }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)?.let { tag -> mOnNFCRecv?.let { it.onNFCRecv(tag) } }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)
        bindService(Intent(this, RemoteControlService::class.java),mServiceConnection,BIND_AUTO_CREATE)
        registerUsbMntReceiver()
        mBitmap =  Bitmap.createBitmap(100,100,Bitmap.Config.ARGB_8888)
        isTablet = ScreenUtils.isTablet()

        //显示广告
        val mDisplayManager = getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
        val displays = mDisplayManager.displays
        if(displays.size>1) {
            secondScreen = SecondScreen(this, displays[1])
            secondScreen?.window?.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
            if (DeviceUtils.getSDKVersionCode() < 23){
                @TargetApi(23)
                if (!Settings.canDrawOverlays(this)) {
                    val intent =  Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:$packageName"))
                    startActivityForResult(intent, 101)
                }else secondScreen?.show()
            }
        }

        checkPermission { it ->
            if (it) {
                setPresenter(MainPresenter(this@MainActivity, ConfigModel()))
                mPresenter.start()
                mPresenter.openDevice(ticket_title)
                faceHelper = FaceHelper(this@MainActivity, container = faceViewContainer,cameraMirror = true,rectMirror = true,
                        initSuccess = {
                    mPresenter.setFaceHelper(it)
                    it.setOnCaptureFace {
                        if (ObjectUtils.isEmpty(it)){
                            show_current_container.visibility = View.GONE
                        }else{
                            show_current.setImageBitmap(it)
                            show_current_container.visibility = View.VISIBLE
                        }
                    }
                }) { it ->
                    //设置相机分辨率选项
                    val sizeList = it.supportedPreviewSizes
                            .asSequence()
                            .map { return@map CameraPreviewSize(it.width, it.height) }
                            .filter { it.width < 1920 }
                            .distinct()
                            .sortedBy { it.height * it.width }
                            .toList()
                    cameraPreviewSize.adapter = ArrayAdapter<CameraPreviewSize>(this, android.R.layout.simple_spinner_dropdown_item, sizeList)
                    val index = sizeList.indexOf(CameraPreviewSize(it.previewSize.width, it.previewSize.height))
                    cameraPreviewSize.setSelection(index)
                    cameraPreviewSize.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(parent: AdapterView<*>?) {}
                        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                            SPUtils.getInstance().put("cameraPreviewSizeWidth", sizeList[position].width)
                            SPUtils.getInstance().put("cameraPreviewSizeHeight", sizeList[position].height)
                            faceHelper.setPreviewSize(sizeList[position].width, sizeList[position].height)
                        }

                    }
                }
            } else {

                ToastUtils.showShort("必须授予相机和读写权限！！！")
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                intent.data = Uri.fromParts("package", packageName, null)
                startActivity(intent)
                System.exit(0)
            }
        }

        //接受升级推送
        registerReceiver(updateReceiver, IntentFilter(RECEIVER_ACTION_UPGRADE_APP))
        ad.setOnClickListener{mPresenter.restTestData()}


    }

    private fun registerUsbMntReceiver(){
        val usbFilter= IntentFilter()
        usbFilter.addAction(Intent.ACTION_MEDIA_MOUNTED)
        usbFilter.addAction(Intent.ACTION_MEDIA_REMOVED)
        usbFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED)
        usbFilter.addDataScheme("file")
        registerReceiver(usbMntReceiver,usbFilter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            101->{
                if(Build.VERSION.SDK_INT >22){
                    if (!Settings.canDrawOverlays(this)){
                        ToastUtils.showShort("授权悬浮窗！！！！")
                        val intent =  Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:$packageName"))
                        startActivityForResult(intent, 101)
                    }else{
                        secondScreen?.show()
                    }
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    /**
     * 获取授权
     */
    private fun checkPermission(granted: (Boolean) -> Unit) {
        if (DeviceUtils.getSDKVersionCode() >= Build.VERSION_CODES.M) {
            PermissionUtils
                    .permission(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_PHONE_STATE)
                    .callback(object : PermissionUtils.SimpleCallback {
                        override fun onGranted() = granted(true)
                        override fun onDenied() = granted(false)
                    })
                    .request()
        } else {
            granted(true)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    }

    override fun onResume() {
        super.onResume()
        mNfcAdapter?.enableForegroundDispatch(this, mPendingIntent, null, null)
        if (mainDrawer.isDrawerOpen(Gravity.START)) mainDrawer.closeDrawer(Gravity.START)
//        netStateCheckTask = Executors.newSingleThreadScheduledExecutor()
//        netStateCheckTask?.scheduleAtFixedRate({
//            "http://${Config.IP}:$SERVER_PORT/app/onLine".httpPost().timeout(500).responseString { _, _, result ->
//                result.fold({
//                    runOnUiThread {netStatus.setImageResource(R.drawable.signal_online) }
//                }, {
//                    runOnUiThread { netStatus.setImageResource(R.drawable.sigal_offline) }
//                })
//            }
//        }, 0, 1500, TimeUnit.MILLISECONDS)
        updateApp("http://${Config.IP}:60000/apk/checkAndUpdateApp", AppUpgradeHttpManager()).update()

    }

    // 界面不可见停止预览
    override fun onPause() {
        super.onPause()
        mNfcAdapter?.disableForegroundDispatch(this)
        netStateCheckTask?.shutdownNow()
        netStateCheckTask = null

    }

    override fun onStart() {
        super.onStart()
        if (!isTablet) {
            mNfcAdapter = NfcAdapter.getDefaultAdapter(this)
            //一旦截获NFC消息，就会通过PendingIntent调用窗口
            mPendingIntent = PendingIntent.getActivity(this, 0, Intent(this, this::class.java), 0)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        AppExecutors.diskIO.execute { mPresenter.closeDevice() }
        faceHelper.close()
        unbindService(mServiceConnection)
        unregisterReceiver(usbMntReceiver)
        unregisterReceiver(updateReceiver)
    }


    /**
     * 初始化界面
     */
    override fun initView() {
        mProgressDialog = ProgressDialog(this)
        mProgressDialog.setTitle("数据下载进度")
        if (listOf(SCAN_TYPE_RFID_ONLY,
                        SCAN_TYPE_RFID_AND_QRCODE,
                        SCAN_TYPE_RFID_OR_QRCODE,
                        SCAN_TYPE_RFID_ONLY_HIGH,
                        SCAN_TYPE_RFID_AND_QRCODE_HIGH,
                        SCAN_TYPE_RFID_OR_QRCODE_HIGH,
                        SCAN_TYPE_IDCARD_OR_RFID,
                        SCAN_TYPE_IDCARD_OR_RFID_HIGH,
                        SCAN_TYPE_FACE
                ).contains(Config.scanType)) {
            ticket_ll.visibility = View.VISIBLE
        }
        if (listOf(SCAN_TYPE_QRCODE_ONLY,
                        SCAN_TYPE_RFID_AND_QRCODE,
                        SCAN_TYPE_RFID_OR_QRCODE,
                        SCAN_TYPE_RFID_AND_QRCODE_HIGH,
                        SCAN_TYPE_RFID_OR_QRCODE_HIGH,
                        SCAN_TYPE_IDCARD_AND_QRCODE
                ).contains(Config.scanType)) {
            qrcode_ll.visibility = View.VISIBLE
        }
        if (Config.scanType == SCAN_TYPE_IDCARD || Config.scanType == SCAN_TYPE_IDCARD_AND_QRCODE) {
            //todo 不显示身份证信息
            ticket_title.text = "身份证:"
            ticket_ll.visibility = View.GONE
//            idcard_info.visibility = View.VISIBLE
        }
        val navHeaderView = mainNavView.getHeaderView(0)
        val iconTextView = navHeaderView.findViewById<AvatarImageView>(R.id.navHeaderAvatar)
        val versionTextView = navHeaderView.findViewById<TextView>(R.id.navHeaderVersion)
        versionTextView.text = "版本 ${AppUtils.getAppVersionName()}"
        iconTextView.setTextAndColorSeed("UG", "UG")
        mainNavView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_settings -> {
//                    mPresenter.closeDevice()
//                    faceHelper.close()
                    val intent =Intent(this,SettingActivity::class.java)
                    startActivity(intent)
                }
                R.id.nav_pull -> {
                    mProgressDialog.setMessage("等待下载数据")
                    mProgressDialog.show()
                    mPresenter.getTicketInfo(Config.projectId)
                }
                R.id.nav_version -> updateApp("http://${Config.IP}:60000/apk/checkAndUpdateApp", AppUpgradeHttpManager()).update()
                R.id.nav_tickets -> startActivity(Intent(this, TicketDataActivity::class.java))
                R.id.nav_requests -> startActivity(Intent(this, RequestInfoActivity::class.java))
                R.id.nav_finger -> {
                    mPresenter.closeDevice()
                    val intent =Intent(this,FingerDemoActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
            }
            return@setNavigationItemSelectedListener true
        }
        exposureCompensation.adapter = ArrayAdapter<Int>(this, android.R.layout.simple_spinner_dropdown_item, arrayOf(-3, -2, -1, 0, 1, 2, 3))
        exposureCompensation.setSelection(SPUtils.getInstance().getInt("exposureCompensation", 3))
        exposureCompensation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                SPUtils.getInstance().put("exposureCompensation", position)
                faceHelper.setExposureCompensation(position - 3)
            }

        }
        whiteBalance.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayOf("自动", "白炽灯", "荧光灯", "日光", "阴天"))
        whiteBalance.setSelection(SPUtils.getInstance().getInt("whiteBalance", 0))
        whiteBalance.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                SPUtils.getInstance().put("whiteBalance", position)
                faceHelper.setWhiteBalance(position)
            }

        }
        resetUI()
    }

    override fun showProcess(process: Int, max: Int) {
        if (!mProgressDialog.isShowing) {
            mProgressDialog.show()
        }
        if (process == -1) {
            mProgressDialog.setMessage("下载失败，请重新下载")
            AppExecutors.mainThread.schedule({ mProgressDialog.dismiss() }, 2000)
            return
        }
        if (mProgressDialog.max != max) {
            mProgressDialog.max = max
        }
        if (process < max) {
            mProgressDialog.progress = process
            mProgressDialog.setMessage("总数$max,已经下载$process")
        }
        if (process == max) {
            mProgressDialog.progress = process
            mProgressDialog.setMessage("下载完成")
            AppExecutors.mainThread.schedule({ mProgressDialog.dismiss() }, 1000)
        }

    }

    override fun updateAdImage(filePath: String) {
        if (filePath.contains("jpg")||filePath.contains("png")||filePath.contains("jpeg")){
            Picasso.get().load(File(filePath)).into(ad)
        }else {
            Picasso.get().load(R.mipmap.adv_defult).into(ad)
        }
    }


    override fun updateAdTitle(title: String) {

        adv_title.text = if(TextUtils.isEmpty(title)) getString(R.string.adv_title) else title
    }


    override fun updateName(name: String) {
        sold_name.text = name
    }

    override fun setPresenter(presenter: MainContract.Presenter) {
        mPresenter = presenter
    }

    override fun updateMessage(message: String, color: Int) {
        tooltip.text = message
        when (color) {
            0 -> tooltip.setTextColor(ContextCompat.getColor(this, R.color.msg_success))
            1 -> tooltip.setTextColor(ContextCompat.getColor(this, R.color.msg_warning))
            2 -> tooltip.setTextColor(ContextCompat.getColor(this, R.color.msg_fail))
        }
    }

    override fun updateQrcode(qrCode: String) {
        if (qrCode.length > 4) {
            qr_code.text = "****${qrCode.subSequence(qrCode.length - 4, qrCode.length)}"
        } else
            qr_code.text = qrCode
    }

    override fun updateTicketInfo(ticketNum: String) {
        if (ticketNum.length > 4) {
            ticket_code.text = "****${ticketNum.subSequence(ticketNum.length - 4, ticketNum.length)}"
        } else
            ticket_code.text = ticketNum
    }

    override fun updatePlaceName(name: String) {
        if (name.isNullOrEmpty()) adv_title.text = "未获取到该活动场馆地址"
        else adv_title.text = name
    }

    override fun updateCapturePictureImageView(path: String) {
        runOnUiThread {
            if(path.contains("http"))
                Picasso.get()
                    .load(path)
                    .placeholder(R.drawable.icon_loading)
                    .error(R.drawable.icon_load_image_fail)
                    .into(capturePreview)
            else
                Picasso.get()
                        .load(File(path))
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .placeholder(R.drawable.icon_loading)
                        .error(R.drawable.icon_load_image_fail)
                        .into(capturePreview)
        }
    }

    override fun setInCount(count: Int) {
        inCount.text = "$count"
    }

    override fun updateLastInTime(time: String) {
        lastInTime.text = "上次入场：$time"
        lastInTime.visibility = View.VISIBLE
    }

    override fun updateCapturePictureImageView(resourceId: Int) {
        runOnUiThread { Picasso.get().load(resourceId).into(capturePreview) }
    }

    override fun resetUI() {
        if (SCAN_TYPE_IDCARD == Config.scanType || SCAN_TYPE_IDCARD_AND_QRCODE == Config.scanType) {
            setTextViewNull(arrayOf(id_name, id_sex, id_nation, id_address, id_birthday, id_number))
        }
        sold_name.text = ""
        lastInTime.visibility = View.GONE
        resetCapturePictureImageView()
        updateQrcode("")
        updateTicketInfo("")
        appVersionTv.text = AppUtils.getAppVersionName()
        nickNameTv.text = Config.deviceName
        resetMessage()
        updateStatusTip(false)
        setInCount(SPUtils.getInstance().getInt(Config.IN_COUNT))
    }

    private fun setTextViewNull(textViewList: Array<TextView>) = textViewList.forEach { it.text = "" }

    override fun updateIdCard(idCard: IDCard) {
        updateTicketInfo(idCard.id ?: "")
        sold_name.text = idCard.name ?: ""
        id_name.text = idCard.name ?: ""
        id_nation.text = idCard.nation ?: ""
        id_birthday.text = idCard.birthday ?: ""
        id_sex.text = idCard.sex ?: ""
        if (idCard.address.isNullOrBlank()) id_address.text = ""
        else id_address.text = idCard.address.trim()
        id_number.text = idCard.id ?: ""
        tooltip.text = ""
    }


    override fun resetMessage() {
        when (Config.scanType) {
            SCAN_TYPE_IDCARD -> updateMessage("扫描身份证", 1)
            SCAN_TYPE_QRCODE_ONLY -> updateMessage("扫描二维码", 1)
            SCAN_TYPE_RFID_AND_QRCODE, SCAN_TYPE_RFID_AND_QRCODE_HIGH -> updateMessage("扫描二维码+票证", 1)
            SCAN_TYPE_RFID_ONLY, SCAN_TYPE_RFID_ONLY_HIGH -> updateMessage("扫描票证", 1)
            SCAN_TYPE_RFID_OR_QRCODE, SCAN_TYPE_RFID_OR_QRCODE_HIGH -> updateMessage("扫描二维码或票证", 1)
            SCAN_TYPE_IDCARD_AND_QRCODE -> updateMessage("扫描二维码+身份证", 1)
            SCAN_TYPE_IDCARD_OR_RFID_HIGH, SCAN_TYPE_IDCARD_OR_RFID -> updateMessage("芯片或身份证", 1)
            SCAN_TYPE_FACE -> updateMessage("刷脸或刷票",1)
        }
    }

    override fun updateCapturePictureImageView(bitmap: Bitmap) {
        capturePreview.setImageBitmap(bitmap)
    }

    private fun resetCapturePictureImageView() = capturePreview.setImageBitmap(mBitmap)

    override fun updateStatusTip(pass: Boolean) {
        if ( pass ){
            validateStatus.setImageResource(R.drawable.pass_seccess)
        } else {
            validateStatus.setImageResource(R.drawable.pass_fail)
        }
    }
}