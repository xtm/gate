package com.uglive.gate.ui.face

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.annotation.WorkerThread
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.arcsoft.face.*
import com.blankj.utilcode.util.SPUtils
import com.blankj.utilcode.util.ToastUtils
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.R
import com.uglive.gate.bean.Face
import com.uglive.gate.db.localDB.LocalDB
import com.uglive.gate.model.ConfigModel
import com.uglive.gate.model.SCAN_TYPE_FACE
import com.uglive.gate.ui.main.MainActivity
import com.uglive.gate.utils.ImageUtils
import java.io.File
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import kotlin.concurrent.thread

class FaceLoadActivity : AppCompatActivity() {
    private val TAG = "FaceLoadActivity"

    private var progressDialog: ProgressDialog? = null


    private lateinit var faceEngine : FaceEngine
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_face_load)
        if (ConfigModel().scanType == SCAN_TYPE_FACE){
            initEngine()
            parsePicture()
        }else{
            gotoMainActivity()
        }
    }

    private fun gotoMainActivity(){
        val intent  = Intent(this,MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun initEngine(){
        faceEngine = FaceEngine()
        var afCode = faceEngine.active(this, Config.ARC_FACE_APP_ID, Config.ARC_FACE_SDK_KEY)
        if (afCode == ErrorInfo.MOK || afCode == ErrorInfo.MERR_ASF_ALREADY_ACTIVATED) {
            afCode = faceEngine.init(this.applicationContext, FaceEngine.ASF_DETECT_MODE_IMAGE, FaceEngine.ASF_OP_0_ONLY,
                   16, 5,
                    FaceEngine.ASF_FACE_DETECT or FaceEngine.ASF_FACE_RECOGNITION)
            val versionInfo = VersionInfo()
            faceEngine.getVersion(versionInfo)
            Log.i(TAG, "FACE Engine:  init rltCode: $afCode  version:$versionInfo")
            if (afCode != ErrorInfo.MOK) {
                ToastUtils.showShort("人脸引擎初始化失败", Toast.LENGTH_SHORT)
            }
        } else {
            ToastUtils.showShort( "人脸引擎注册失败,注意：第一次注册需要联网", Toast.LENGTH_SHORT)
        }
    }


    @WorkerThread
    private fun getFaceFeature(data:ByteArray?,width: Int,height: Int,format: Int):ByteArray?{
        val faceList = arrayListOf<FaceInfo>()
        val rltCode = faceEngine.detectFaces(data,width,height,format,faceList)
//        Log.e("rltCode","$rltCode")
        if (rltCode == ErrorInfo.MOK && faceList.size>0){
            val feature = FaceFeature()
            val resultCode = faceEngine.extractFaceFeature(data, width, height, format, faceList.first(), feature)
//            Log.e("resultCode","$resultCode")
            if (resultCode == ErrorInfo.MOK) {
                return feature.featureData
            }
            return null
        }
        return null
    }

    private fun parsePicture(){
        thread (name = "parsePicture"){
            val startTime = System.currentTimeMillis()
            val faceDao = LocalDB.get(this).faceDao()
            var faces = faceDao.getAllNames()
            if (Config.faceSourceDir.exists()){
                val dirPath = Config.faceSourceDir.absolutePath
                val regex = Regex("""^.+\.(png|jpeg|jpg)$""")
                val list = mutableListOf<String>()
                Config.faceSourceDir.listFiles().forEach {
                    val name = it.name
                    if (regex.matches(name)) list.add(name)
                }
                faces.minus(list).forEach { faceDao.deleteByName(it) }
                //导入以前没有导入的数据
                //无法检测人脸的图片
                val badPicture = mutableSetOf<String>()

                var i = 0
                runOnUiThread {
                    progressDialog = ProgressDialog.show(this@FaceLoadActivity,"过滤数据", "过滤中......")
                }
                val needParsePic = list.minus(faces)
                runOnUiThread {
                    progressDialog?.setTitle("图片解析")
                    progressDialog?.setMessage("开始解析")
                }
                Log.i("FACE","start.......")
                for (it in needParsePic) {
                    i++
                    Log.i("FACE","parser pic $i $it start")
                    runOnUiThread {
                        progressDialog?.setMessage("解析-> $i $it ")
                        progressDialog?.show()
                    }

                    val filePath = dirPath + File.separator + it
                    val bitmapSrc = BitmapFactory.decodeFile(filePath)
                    if (bitmapSrc == null){
                        badPicture.add(it)
                        Log.i("FACE","parser pic $i $it end")
                        continue
                    }
                    val bitmap = resizeBitmap(bitmapSrc)
                    val byteArray = ImageUtils.bitmapToBgr24(bitmap)
                    //若能提取特征就保存起来
                    val feature = try {
                        AppExecutors.diskIO.submit(Callable {
                            return@Callable getFaceFeature(byteArray,bitmap.width,bitmap.height, FaceEngine.CP_PAF_BGR24)
                        }).get(2000, TimeUnit.MILLISECONDS)
                    } catch (e: TimeoutException){ null }

                    if (feature == null){
                        File(filePath).delete()
                        badPicture.add(it)
                    }else{
                        //解析芯片ID
                        val nameSpt= it.split(".")[0].split("-")
                        var ticketId = if (nameSpt.size>1) nameSpt.first() else it
                        faceDao.insert(Face(fileName = it,ticketId = ticketId, feature = feature))
                    }
                    bitmap.recycle()
                    bitmapSrc.recycle()
                    Log.i("FACE","parser pic $i $it end")
                }
                runOnUiThread  {
                    progressDialog?.setMessage("有${list.size}张照片,识别${list.size - badPicture.size}张" +
                            ",耗时：${(System.currentTimeMillis() - startTime)/1000/60}分钟 ")
                    SPUtils.getInstance().put("badPicture",badPicture)
                    Log.i("FACE","共有${list.size}张照片,识别${list.size - badPicture.size}张")
                    Log.i("FACE","耗时：${System.currentTimeMillis() - startTime}")
                }
                faceEngine.unInit()
                Thread.sleep(2000)
                runOnUiThread  {
                    progressDialog?.dismiss()
                    gotoMainActivity()
                }
            }else{
                gotoMainActivity()
            }
        }
    }
    /**
     * 将不符合识别宽高的图片裁剪到适合识别
     */
    @WorkerThread
    private fun resizeBitmap(bitmap: Bitmap): Bitmap {
        val outOfWidth =  bitmap.width % 4
        val outOfHeight = bitmap.height % 2
        if (outOfHeight==0 && outOfWidth==0) return bitmap
        val width = bitmap.width - outOfWidth
        val height = bitmap.height - outOfHeight
        return Bitmap.createBitmap(bitmap,0,0,width,height)
    }
}
