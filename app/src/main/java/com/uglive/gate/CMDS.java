package com.uglive.gate;

public interface CMDS {
    //握手
    int handshake = 54;
    //重定向
    int redirect = 57;
    //心跳
    int heartbeat = 14;

    // 设备基本信息
    int deviceInfo = 1;
    // 设备配置信息
    int deviceConfig = 2;
    // 设备状态，相机是否卡死
    int deviceStatus = 3;

    //App切换页面
    int changePage = 4;


    //关闭App
    int restartApp = 104;
    //关闭设备
    int restartDevice = 105;

    //配置设备
    int configDevice = 106;

    //检查升级
    int upgrade = 107;

    //开门
    int openGate = 108;

    //配置曝光度
    int configExposure = 109;

    //配置白平衡
    int configWhiteBalance = 110;
    //改变设备昵称
    int changeNickName = 111;
    //开关推流
    int changePushStream = 112;

    //删除人脸
    int deleteFace = 113;

}