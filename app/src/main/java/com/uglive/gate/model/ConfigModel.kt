package com.uglive.gate.model

import com.blankj.utilcode.util.DeviceUtils
import com.blankj.utilcode.util.SPUtils
import com.uglive.gate.Config


const val SP_IP = "SP_IP" //服务器IP
const val SP_GONGAN_IP = "SP_GONGAN_IP"//公安服务器IP
const val SP_SECRET = "SP_SECRET" //m1卡秘钥
const val SP_SCAN_TYPE = "SP_SCAN_TYPE" //扫描类型
const val SP_DEBUG_IP = "SP_DEBUG_IP" // 调试服务器IP
const val SP_DEVICE_NAME = "SP_device_Name" // 设备名称
const val SP_FACE_VALIDATE_TYPE = "SP_faceValidateType" // 人脸验证类型
const val SP_PROJECT_ID = "SP_PROJECT_ID" // 活动号
const val SP_FACE_DEGREE = "SP_FACE_DEGREE" // 人脸角度
const val SP_ONlY_SCAN_RFID = "SP_ONlY_SCAN_RFID" // 能读到电子标签就让进
const val SP_USE_KEYBOARD_QRCODE = "SP_USE_KEYBOARD_QRCODE" //是否是用键盘类型的二维码扫描器

const val SP_CAMERA_SCALE = "SP_CAMERA_SCALE" // 相机有效范围
const val SP_COMPARE_ID_CARD_FACE = "SP_COMPARE_ID_CARD_FACE"// 是否验证身份证中的人脸
const val SP_CAPUTRE_NEED_FACE = "P_CAPUTRE_NEED_FACE"// 取帧时是否必须有人脸
const val SP_UPLOAD_CHENDU_POLICE = "SP_UPLOAD_CHENDU_PLICE"
const val SP_CONSTANT_SECRET= "SP_CONSTANT_SECRET"
const val SP_FACE_SCALE = "SP_FACE_SCALED"
const val SP_FACE_SIZE = "SP_FACE_SIZE"

const val SP_PUSH_STREAM = "SP_PUSH_STREAM"



const val SP_NEED_SECRET = "SP_NEED_SECRET"

// 是否是厂家加密的芯片
const val SP_ENC_BY_FACTORY = "SP_ENC_BY_FACTORY"

// 只需要认证电子标签
const val SCAN_TYPE_RFID_ONLY = 0

// 只需要认证二维码
const val SCAN_TYPE_QRCODE_ONLY = 1

// 认证电子标签+二维码
const val SCAN_TYPE_RFID_AND_QRCODE = 2

// 认证身份证
const val SCAN_TYPE_IDCARD = 3

// 对比人脸
const val SCAN_TYPE_FACE = 4

// 认证电子标签或二维码
const val SCAN_TYPE_RFID_OR_QRCODE = 5

// 认证身份证+二维码
const val SCAN_TYPE_IDCARD_AND_QRCODE = 6

// 只需要认证电子标签
const val SCAN_TYPE_RFID_ONLY_HIGH = 7

// 认证电子标签+二维码
const val SCAN_TYPE_RFID_AND_QRCODE_HIGH = 8

// 认证电子标签或二维码
const val SCAN_TYPE_RFID_OR_QRCODE_HIGH = 9

// 认证指纹
const val SCAN_TYPE_FINGER = 10

const val SCAN_TYPE_IDCARD_OR_RFID_HIGH = 11

const val SCAN_TYPE_IDCARD_OR_RFID = 12

// 不验证直接上传人脸信息
const val VALIDATE_FACE_TYPE_NO = 0
// 通过本地图片信息验证人脸
const val VALIDATE_FACE_TYPE_LOCAL = 1

// 请求服务器验证
const val VALIDATE_FACE_TYPE_ALY = 2

const val SP_CAMERA_ISMIRROR = "SP_CAMERA_ISMIRROR"
const val SP_RECT_ISMIRROR = "SP_RECT_ISMIRROR"




/**
 * 读取本地配置
 */
class ConfigModel {
    val sp = SPUtils.getInstance()

    /**
     * ip
     */
    var ip: String
        set(value) = sp.put(SP_IP, value)
        get() = sp.getString(SP_IP, "116.62.57.42")
//        get() = sp.getString(SP_IP, "192.168.1.125")

    /**
     * M1密钥
     */
    var secret: String
        set(value) = sp.put(SP_SECRET, value)
        get() = sp.getString(SP_SECRET, "111111")

    /**
     * 扫描类型
     */
    var scanType: Int
        set(value) = sp.put(SP_SCAN_TYPE, value)
        get() = sp.getInt(SP_SCAN_TYPE, -1)

    var deviceName: String
        set(value){
            sp.put(SP_DEVICE_NAME, value)
            Config.deviceName = value
        }
        get() = sp.getString(SP_DEVICE_NAME, DeviceUtils.getAndroidID())

    var projectId: String
        set(value) {
            sp.put(SP_PROJECT_ID, value)
            Config.projectId = value
        }
        get() = sp.getString(SP_PROJECT_ID, "60")

    var faceValidateType
        set(value) = sp.put(SP_FACE_VALIDATE_TYPE, value)
        get() = sp.getInt(SP_FACE_VALIDATE_TYPE, -1)

    var faceDegree
        set(value) {
            sp.put(SP_FACE_DEGREE, value)
            Config.faceDegree = value
        }
        get() = sp.getFloat(SP_FACE_DEGREE, 0.2f)



    var uploadValidateInfo
        set(value) {
            sp.put(SP_ONlY_SCAN_RFID, value)
            Config.uploadValidateInfo = value
        }
        get() = sp.getBoolean(SP_ONlY_SCAN_RFID)

    var cameraScale
        set(value) {
            sp.put(SP_CAMERA_SCALE, value)
            Config.cameraScale = value
        }
        get() = sp.getFloat(SP_CAMERA_SCALE, 1f)
    var gonganIp
        set(value) {
            sp.put(SP_GONGAN_IP, value)
            Config.gonganIp = value
        }
        get() = sp.getString(SP_GONGAN_IP, "192.168.2.132")

    var compareIdcardFace
        set(value) {
            sp.put(SP_COMPARE_ID_CARD_FACE, value)
            Config.compareIdcardFace = value
        }
        get() = sp.getBoolean(SP_COMPARE_ID_CARD_FACE, true)

    var captureNeedFace
        set(value) {
            sp.put(SP_CAPUTRE_NEED_FACE, value)
            Config.captureNeedFace = value
        }
        get() = sp.getBoolean(SP_CAPUTRE_NEED_FACE, true)
    var uploadChenduPolice
        set(value) {
            sp.put(SP_UPLOAD_CHENDU_POLICE, value)
            Config.uploadChenduPolice = value
        }
        get() = sp.getBoolean(SP_UPLOAD_CHENDU_POLICE, false)

    var needSecret
        set(value) {
            sp.put(SP_NEED_SECRET, value)
            Config.needSecret = value
        }
        get() = sp.getBoolean(SP_NEED_SECRET, true)

    var useKeyboardScanQrcodeDevice
        set(value) {
            sp.put(SP_USE_KEYBOARD_QRCODE, value)
            Config.useKeyboardScanQrcodeDevice = value
        }
        get() = sp.getBoolean(SP_USE_KEYBOARD_QRCODE, false)

    var encByFactory
        set(value) {
            sp.put(SP_ENC_BY_FACTORY, value)
            Config.encByFactory = value
        }
        get() = sp.getBoolean(SP_ENC_BY_FACTORY, false)

    var constantSecret
        set(value) {
            sp.put(SP_CONSTANT_SECRET, value)
            Config.constantSecret = value
        }
        get() = sp.getString(SP_CONSTANT_SECRET, "BC0123456AD7")

    var pushStream
        set(value){
            sp.put(SP_PUSH_STREAM, value)
        }
        get() = sp.getBoolean(SP_PUSH_STREAM, false)

    var faceScale
        set(value) {
            sp.put(SP_FACE_SCALE, value)
        }
        get() = sp.getInt(SP_FACE_SCALE, 8)

    var faceSize
        set(value) {
            sp.put(SP_FACE_SIZE, value)
        }
        get() = sp.getInt(SP_FACE_SIZE,2)

    var cameraMirror
        set(value){
            sp.put(SP_CAMERA_ISMIRROR, value)
        }
        get() = sp.getBoolean(SP_CAMERA_ISMIRROR, true)

    var rectMirror
        set(value){
            sp.put(SP_RECT_ISMIRROR, value)
        }
        get() = sp.getBoolean(SP_RECT_ISMIRROR, true)
}
