package com.uglive.gate.model;

import android.util.Log;

import com.decard.NDKMethod.BasicOper;

import com.uglive.gate.bean.SectorDataBean;
import com.uglive.gate.model.listener.M1CardListener;
import com.uglive.gate.utils.ByteUtil;
import com.uglive.gate.utils.ConstUtils;
import com.uglive.gate.utils.MDSEUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hizha on 2017/7/31.
 */

public class M1CardModel {

    private M1CardListener mListener;

    public M1CardModel() {
    }

    public M1CardModel(M1CardListener listener) {
        mListener = listener;
    }

    /**
     * @param currentSectors 读取的扇区
     * @return 读取的值
     */
    private String readSectorData(int currentSectors) {
        int piece = (currentSectors + 1) * 4;
        SectorDataBean sectorDataBean = new SectorDataBean();
        String[] pieceDatas = new String[4];
        for (int i = piece - 4, j = 0; i < piece; i++, j++) {
            String pieceData = MDSEUtils.returnResult(BasicOper.dc_read_hex(i));
            pieceDatas[j] = pieceData;
        }
        sectorDataBean.pieceZero = pieceDatas[0];
        String string = sectorDataBean.pieceZero.substring(0, 24);
        String num = ByteUtil.decode(string);
        return num;
    }

    /**
     * 读取扇区，需要密钥
     *
     * @param newPasswordKey 密钥
     * @return
     */
    public String bt_read_ticket(int spinnerPosition,String newPasswordKey) {
        int keyType=0;
        String isSucceed;

        isSucceed=BasicOper.dc_card_hex(0);
        if (!MDSEUtils.isSucceed(isSucceed))
        {
            return isSucceed;
        }
        isSucceed=BasicOper.dc_load_key_hex(keyType, spinnerPosition , newPasswordKey);
        if (!MDSEUtils.isSucceed(isSucceed))
        {
            return isSucceed;
        }
        isSucceed=BasicOper.dc_authentication(keyType, spinnerPosition);
        if (!MDSEUtils.isSucceed(isSucceed))
        {
            return isSucceed;
        }
        isSucceed = BasicOper.dc_read_hex(spinnerPosition*4+0);
        return isSucceed;
    }


    public void bt_download(String btDownload, String sectorsNum, int keyType, String newPasswordKey, int spinnerPosition) {
        if ("All".equals(sectorsNum)) {
            List<String> loadKeyList = new ArrayList<>();
            for (int i = 0; i < 16; i++) {
                if (!MDSEUtils.isSucceed(BasicOper.dc_load_key_hex(keyType, i, newPasswordKey)))
                    loadKeyList.add(Integer.toString(i));
            }
            mListener.getM1CardResult(ConstUtils.BT_DOWNLOAD, loadKeyList, "", "");

        } else {
            String result = BasicOper.dc_load_key_hex(keyType, spinnerPosition - 1, newPasswordKey);
            mListener.getM1CardResult(ConstUtils.BT_DOWNLOAD, null, result, "");
        }
    }

    public void bt_read_card(String btReadCard, int keyType, int firstVisibleItemPosition) {
        int currentSectors = firstVisibleItemPosition - 1;

        List<String> authFailList = new ArrayList<>();
        if (firstVisibleItemPosition == 0) {
            for (int i = 0; i < 16; i++) {
                if (MDSEUtils.isSucceed(BasicOper.dc_authentication(keyType, i))) {
//                    readSectorData(i);
                    mListener.getM1CardResult(btReadCard, null, i + "", "");

                } else {
                    BasicOper.dc_card_hex(0);// 如果接口调用失败，需要进行复位才能进行后续的操作
                    authFailList.add(Integer.toString(i));
                }
            }
            mListener.getM1CardResult(btReadCard, authFailList, "", "");

        } else {
            boolean authSucceed = MDSEUtils.isSucceed(BasicOper.dc_authentication(keyType, currentSectors));
            String isSucceed;
            if (authSucceed) {
                isSucceed = "true";
            } else {
                isSucceed = "false";
            }
            Log.d("tag", "isSucceed : " + isSucceed);
            mListener.getM1CardResult(btReadCard, null, isSucceed, currentSectors + "");
//            readSectorData(currentSectors);
        }
    }


}
