package com.uglive.gate.model

import android.util.Log
import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.blankj.utilcode.util.Utils
import com.decard.entitys.IDCard
import com.github.kittinunf.fuel.core.Blob
import com.github.kittinunf.fuel.core.DataPart
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpUpload
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import com.uglive.gate.AppExecutors
import com.uglive.gate.Config
import com.uglive.gate.bean.REQUEST_TYPE_LOAD_FACE_IMAGE
import com.uglive.gate.bean.REQUEST_TYPE_UPLOAD_DATA
import com.uglive.gate.bean.RequestBean
import com.uglive.gate.db.localDB.LocalDB
import com.uglive.gate.face.domain.FacePosition
import com.uglive.gate.repositroy.TicketRepository
import java.io.File
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread

/**
 * 管理服务器请求的类
 */
val SERVER_PORT = 50731

class ApiModel(val ticketRepository: TicketRepository) {
    private val TAG = "ApiModel"
    private val gson = Gson()
    private val type = object : TypeToken<Map<String, Object>>() {}.type
    val requestDao = LocalDB.get(Utils.getApp()).requestDao()
    val timeout = 1500
    //20000条数据,一个闸机可以缓存20000条请求
    private val mQueue = LinkedBlockingDeque<RequestBean>(20000)

    /**
     * 最大请求数
     */
    val size = 5
    /**
     * 当前请求数
     */
    var currentSize = AtomicInteger(0)

    //将数据库中数据加载到队列中
    init {
        AppExecutors.diskIO.execute { requestDao.getNotUpload().forEach { mQueue.push(it) } }
        doRequest()
    }

    /**
     * 查询成功数，中暑
     */
    fun getSize() = Pair(requestDao.countSucSize(), requestDao.countTotal())

    /**
     * 删除数据库中所有信息
     */
    fun deleteAllRequests() {
        AppExecutors.diskIO.execute { requestDao.deleteAll() }
    }

    fun getQueueSize() = mQueue.size

    /**
     * 添加一条请求到请求队列
     */
    fun putRequest(requestBean: RequestBean) {
        mQueue.put(requestBean)
        if (requestBean.requestType == REQUEST_TYPE_UPLOAD_DATA) {
            requestDao.insert(requestBean)
        }
    }

    fun updateRequestToDB(requestBean: RequestBean) {
        requestBean.reqSuccess = true
        AppExecutors.diskIO.execute { requestDao.update(requestBean) }
    }

    private fun doRequest() {
        thread {
            while (true) {
                if (currentSize.getAndAdd(1) <= size) {
                    val request = mQueue.take()
                    val params = gson.fromJson<Map<String, Object>>(request.params, type)
                    when (request.requestType) {
                        REQUEST_TYPE_UPLOAD_DATA -> {
                            request.url.httpUpload(parameters = params.toList()).timeout(3000)
                                    .dataParts { _, _ ->
                                        request.filePath?.let { listOf(DataPart(File(it), name = "photoImgFiles", type = "image/jpeg")) }
                                                ?: listOf()
                                    }
                                    .name { "photoImgFiles" }
                                    .responseString { _, _, result ->
                                        currentSize.decrementAndGet()
                                        val (_, error) = result
                                        if (error == null) {
                                            if (Config.isTest) {
                                                AppExecutors.mainThread.execute {
                                                    ToastUtils.showShort("上传 ${params["ticketId"]} 成功")
                                                }
                                            }
                                            //请求成功删除请求的文件和数据库信息
                                            request.filePath?.let { File(it).delete() }
                                            updateRequestToDB(request)
                                        } else {
                                            if (Config.isTest) {
                                                if (Config.isTest) {
                                                    AppExecutors.mainThread.execute {
                                                        ToastUtils.showShort("上传 ${params["ticketId"]} 失败")
                                                    }
                                                }
                                            }
                                            //请求失败重新放回请求队列
                                            mQueue.put(request)
                                        }
                                    }
                        }
                        REQUEST_TYPE_LOAD_FACE_IMAGE -> {
                            ticketRepository.loadFaceImage(request.url, params["name"] as String, {
                                currentSize.decrementAndGet()
                                updateRequestToDB(request)
                            }, {
                                currentSize.decrementAndGet()
                                mQueue.put(request)
                            })
                        }
                        else -> {
                            updateRequestToDB(request)
                            currentSize.decrementAndGet()
                            LogUtils.i(TAG, "错误请求")
                        }
                    }
                }
                //三秒发送一次队列中的请求
                Thread.sleep(1000)
            }
        }
    }


    /**
     * 通过服务器验证
     */
    @Synchronized
    fun validateByServer(validateType: Int, qrcode: String?, rfid: String?, data: ByteArray,
                         idCard: IDCard? = null, facePosition: FacePosition = FacePosition(),
                         callback: (Boolean, String?, String, String, String?,String?) -> Unit) {
        Log.e("facePosition",facePosition.toString())
        val rfid1 = rfid?.let { it } ?: ""
        val qrcode1 = qrcode?.let { it } ?: ""
        val photoDataIDCard = idCard?.let { it.photoData.clone() } ?: byteArrayOf()
        val photoData = data.clone()
        val params = mutableListOf("chipId" to rfid1, "qrCodeId" to qrcode1, "projectId" to Config.projectId, "deviceId" to Config.deviceName, "tolerance" to Config.faceDegree)
        when (validateType) {
            SCAN_TYPE_RFID_AND_QRCODE, SCAN_TYPE_RFID_AND_QRCODE_HIGH -> params.add("checkType" to "0")
            SCAN_TYPE_RFID_ONLY, SCAN_TYPE_RFID_ONLY_HIGH -> params.add("checkType" to "1")
            SCAN_TYPE_QRCODE_ONLY -> params.add("checkType" to "2")
            SCAN_TYPE_FACE -> params.add("checkType" to "3")
            SCAN_TYPE_RFID_OR_QRCODE, SCAN_TYPE_RFID_OR_QRCODE_HIGH -> rfid?.let { params.add("checkType" to "1") }
                    ?: params.add("checkType" to "2")
            SCAN_TYPE_IDCARD, SCAN_TYPE_IDCARD_AND_QRCODE, SCAN_TYPE_IDCARD_OR_RFID, SCAN_TYPE_IDCARD_OR_RFID_HIGH -> {
                if( idCard != null ){
                    if (validateType == SCAN_TYPE_IDCARD||validateType==SCAN_TYPE_IDCARD_OR_RFID||validateType== SCAN_TYPE_IDCARD_OR_RFID_HIGH) {
                        params.add("checkType" to "4")
                    } else {
                        params.add("checkType" to "5")
                    }
                    params.add("idCardId" to idCard!!.id)
                    val map = mapOf("name" to idCard.name, "sex" to idCard.sex, "birthday" to idCard.birthday, "address" to idCard.address, "nation" to idCard.nation,
                            "id" to idCard.id, "office" to idCard.office, "startTime" to idCard.startTime, "endTime" to idCard.endTime)
                    params.add("cardMsg" to gson.toJson(map))
                }else{
                    if (validateType ==  SCAN_TYPE_IDCARD_AND_QRCODE) return
                    else params.add("checkType" to "1")
                }

            }
        }
        val req = "http://${Config.IP}:$SERVER_PORT/checkTicket/checkTicket"
                .httpUpload(parameters = params)
                .timeout(timeout)


        if (validateType == SCAN_TYPE_IDCARD || validateType == SCAN_TYPE_IDCARD_AND_QRCODE) {
            req.blobs { _, _ ->
                listOf(
                        Blob("photoImgFiles", photoData.size.toLong()) { photoData.inputStream() },
                        Blob("cardPhoto", photoDataIDCard.size.toLong()) { photoDataIDCard.inputStream() }
                )
            }.name { "photoImgFiles" }
        } else {
            req.blob { r, _ ->
                r.mediaTypes.add("image/jpeg")
                Blob("photoImgFiles", data.size.toLong()) { data.inputStream() }
            }
                    .name { "photoImgFiles" }
        }
        //同步请求
        val (_, _, result) = req.responseString()
        result.fold({
            var apiInfo = mapOf<String, Object>()
            try {
                apiInfo = gson.fromJson<Map<String, Object>>(it, type)
            } catch (jsonError: JsonSyntaxException) {
                AppExecutors.mainThread.execute { ToastUtils.showShort("json解析错误") }
                return@fold
            }

            if (apiInfo["code"].toString() == "1") {
                uploadGongAn(data, Config.deviceName!!)
                val data1 = apiInfo["list"] as LinkedTreeMap<String, String>
                callback(true, data1["face_path"], "", "", data1["id_card_name"], data1["last_in_time"])
            } else if (apiInfo["code"].toString() == "10004") {
                ToastUtils.showShort("项目号配置错误")
            } else {
                val data1 = apiInfo["list"] as LinkedTreeMap<String, String>

                callback(false, data1["face_path"], apiInfo["code"].toString(), apiInfo["message"].toString(), data1["id_card_name"],data1["last_in_time"])
            }

        }, {
            uploadGongAn(data, Config.deviceName!!)
            callback(false, null, "8", "网络连接失败", null,null)
        })
    }

    /**
     * 获取广告图片和场官地址
     */
    fun getAdvInfo() = "http://${Config.IP}:$SERVER_PORT/app/getProjectMessage"
            .httpPost(parameters = listOf("projectId" to Config.projectId))
            .timeout(10000)
            .responseString().third

    fun queryTicketId(ticketId: String):Pair<String,String> {
        //同步请求
        val (_, _, result) = "http://${Config.IP}:$SERVER_PORT/checkTicket/verificationWorkPermit"
                .httpPost(parameters = listOf(
                        "ticketId" to ticketId,
                        "projectId" to Config.projectId,
                        "deviceId" to Config.deviceName
                ))
                .timeout(1000)
                .responseString()
        result.fold({
            val apiInfo: Map<String, Object>
            try {
                apiInfo = gson.fromJson<Map<String, Object>>(it, type)
            } catch (jsonError: JsonSyntaxException) {
                Log.e("FuelError","jsonError",jsonError)
                AppExecutors.mainThread.execute { ToastUtils.showShort("服务器出错, Json 解析错误") }
                return@fold
            }
            return@queryTicketId Pair(apiInfo["code"].toString(),apiInfo["message"].toString())
        }, {
            Log.e("FuelError","FuelError",it)
            return@queryTicketId Pair("8","网络连接失败")
        })
        return Pair("8","网络连接失败")
    }

    fun upLoadFace(ticketId: String,bitmap: ByteArray,position: FacePosition){
        val params = mutableListOf(
                "ticketId" to ticketId,
                "projectId" to Config.projectId,
                "deviceId" to Config.deviceName)
        val req = "http://${Config.IP}:$SERVER_PORT/checkTicket/verificationWorkPermit"
                .httpUpload(parameters = params)
                .timeout(timeout)
//        val req = "http://192.168.3.98:60000/app/upload"
//                .httpUpload(parameters = params)
//                .timeout(timeout)
        val data = bitmap.clone()
        req.blob { r, _ ->
            r.mediaTypes.add("image/jpeg")
            Blob("photoImgFiles", data.size.toLong()) { data.inputStream() }
        }.name { "photoImgFiles" }
        req.responseString { _, _, result ->
            result.fold({
                Log.e("netUpLoadFace","upload face success")
            }, {
                Log.e("netUpLoadFace","upload face fail")
            })
        }
        uploadGongAn(bitmap,Config.deviceName!!)
    }


    companion object {
        fun uploadGongAn(imgData: ByteArray, deviceId: String) {
            if (Config.uploadChenduPolice) { //若设置了上传到成都公安
                "http://${Config.gonganIp}:8086/VIID/getimg"
                        .httpUpload(parameters = listOf("deviceId" to deviceId))
                        .blob { r, _ ->
                            r.mediaTypes.add("image/jpeg")
                            Blob("photoImgFiles", imgData.size.toLong()) { imgData.inputStream() }
                        }
                        .name { "photoImgFiles" }
                        .timeout(2000)
                        .responseString { _, _, result ->
                            result.fold({}) { ToastUtils.showShort("请求公安失败:http://${Config.gonganIp}:8086/VIID/getimg") }
                        }
            }
        }
        fun uploadShuangLiuPolice(imgData: ByteArray, deviceId: String,x: Int,y: Int,width: Int,height: Int){
            "http://${Config.shuangliuIp}:8086/VIID/getimg"
                    .httpUpload(parameters = listOf(
                            "deviceId" to deviceId,
                            "x" to x,
                            "y" to y,
                            "width" to width,
                            "height" to height
                    ))
                    .blob { r, _ ->
                        r.mediaTypes.add("image/jpeg")
                        Blob("photoImgFiles", imgData.size.toLong()) { imgData.inputStream() }
                    }
                    .name { "photoImgFiles" }
                    .timeout(2000)
                    .responseString { _, _, result ->
                        result.fold({}) { ToastUtils.showShort("请求公安失败:http://${Config.gonganIp}:8086/VIID/getimg") }
                    }
        }
    }

}