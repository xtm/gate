package com.uglive.gate.remoteControl.bean;

public class SocketMsg {
    private int cmd;
    private String dataJson;

    public SocketMsg(int cmd, String dataJson) {
        this.cmd = cmd;
        this.dataJson = dataJson;
    }

    public int getCmd() {
        return cmd;
    }

    public void setCmd(int cmd) {
        this.cmd = cmd;
    }

    public String getDataJson() {
        return dataJson;
    }

    public void setDataJson(String dataJson) {
        this.dataJson = dataJson;
    }
}
