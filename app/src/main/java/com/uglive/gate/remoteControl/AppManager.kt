package com.uglive.gate.remoteControl

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import com.blankj.utilcode.util.*
import com.uglive.gate.App
import com.uglive.gate.model.ConfigModel
import com.uglive.gate.remoteControl.bean.Device
import kotlin.concurrent.thread

object AppManager{
    val mConfigModel = ConfigModel()

    //重启设备
    fun restartDevice() {
        if (Build.MANUFACTURER =="rockchip"){
            if (Build.DEVICE=="firefly"){ //firefly3288的板子
                Runtime.getRuntime().exec(arrayOf("reboot"))
            }else{//老板子
                Runtime.getRuntime().exec(arrayOf("/system/xbin/su", "-c", "reboot now"))
            }
        }
    }


    fun restartApp(context: Context){
        //2S后重启程序,因为需要重新配置日志发送模块
        val intent = context.packageManager.getLaunchIntentForPackage(context.applicationContext.packageName)
        val restartIntent = PendingIntent.getActivity(context.applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 2500, restartIntent)
        ToastUtils.showShort("配置已经更改，程序2s后重启")
        thread {
            Thread.sleep(1000)
            //等待配置保存好同时同时提醒用户,1s后关闭程序
            System.exit(0)
        }
    }

    fun genDevice(): Device {
        val device = Device()
        device.androidId = DeviceUtils.getAndroidID()
        device.cameraExposureCompensation = SPUtils.getInstance().getInt("exposureCompensation", 3) - 3
        device.cameraPreviewWidth =SPUtils.getInstance().getInt("cameraPreviewSizeWidth", 640)
        device.cameraPreviwHeight =  SPUtils.getInstance().getInt("cameraPreviewSizeHeight", 480)
        device.cameraWhiteBalance = SPUtils.getInstance().getInt("whiteBalance", 0)
        device.cameraScale = mConfigModel.cameraScale
        device.constantSecret = mConfigModel.constantSecret
        device.nickName = mConfigModel.deviceName
        device.encByFactory = mConfigModel.encByFactory
        device.faceDegree  = mConfigModel.faceDegree
        device.faceValidateType = mConfigModel.faceValidateType
        device.gonganIp  = mConfigModel.gonganIp
        device.needSecret = mConfigModel.needSecret
        device.onlyScanRfid = mConfigModel.uploadValidateInfo
        device.projectId = mConfigModel.projectId.toInt()
        device.useKeyboardScanQrcodeDevice = mConfigModel.useKeyboardScanQrcodeDevice
        device.uploadChenduPolice = mConfigModel.uploadChenduPolice
        device.scanType = mConfigModel.scanType
        device.secrectKey = mConfigModel.secret
        device.appVersion = AppUtils.getAppVersionName()
        device.deviceType = if(ScreenUtils.isTablet()) Device.TYPE_TABLET else Device.TYPE_PHONE
        device.captureNeedFace = mConfigModel.captureNeedFace
        device.compareIDCardFace = mConfigModel.compareIdcardFace
        device.sdkVersion = Build.VERSION.SDK_INT
        device.pushStream = mConfigModel.pushStream
        return  device
    }

    fun config(device: Device){
        SPUtils.getInstance().put("exposureCompensation",device.cameraExposureCompensation + 3)
        SPUtils.getInstance().put("cameraPreviewSizeWidth",device.cameraPreviewWidth)
        SPUtils.getInstance().put("cameraPreviewSizeHeight",device.cameraPreviwHeight)
        SPUtils.getInstance().put("whiteBalance",device.cameraWhiteBalance)
        mConfigModel.cameraScale = device.cameraScale
        mConfigModel.constantSecret = device.constantSecret
        //不在配置中设置设备名，有单独的方式配置设备名
//        mConfigModel.deviceName = device.nickName
        mConfigModel.encByFactory = device.encByFactory
        mConfigModel.faceDegree = device.faceDegree
        mConfigModel.faceValidateType = device.faceValidateType
        mConfigModel.gonganIp = device.gonganIp
        mConfigModel.needSecret = device.needSecret
        mConfigModel.uploadValidateInfo = device.onlyScanRfid
        mConfigModel.projectId = device.projectId.toString()
        mConfigModel.useKeyboardScanQrcodeDevice = device.useKeyboardScanQrcodeDevice
        mConfigModel.uploadChenduPolice = device.uploadChenduPolice
        mConfigModel.scanType = device.scanType
        mConfigModel.secret = device.secrectKey
        mConfigModel.captureNeedFace = device.captureNeedFace
        mConfigModel.compareIdcardFace = device.compareIDCardFace
        mConfigModel.pushStream = device.pushStream

    }

    fun saveExposureCompensation(position:Int){
        SPUtils.getInstance().put("exposureCompensation", position + 3)
    }

    fun saveWhiteBalance(position:Int){
        SPUtils.getInstance().put("whiteBalance", position)
    }

    fun saveNickName(nickName:String){
        mConfigModel.deviceName = nickName
    }

    fun savePushStream(value: String){
        mConfigModel.pushStream = value == "true"
    }


    fun openGate(){
        (Utils.getApp() as App).gateControllerDevice?.openGate()
    }


}