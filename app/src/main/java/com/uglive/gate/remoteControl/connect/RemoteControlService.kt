package com.uglive.gate.remoteControl.connect

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.util.Log
import com.blankj.utilcode.util.DeviceUtils
import com.blankj.utilcode.util.Utils
import com.google.gson.Gson
import com.uglive.gate.App
import com.uglive.gate.AppExecutors
import com.uglive.gate.CMDS
import com.uglive.gate.Config
import com.uglive.gate.exception.RedirectException
import com.uglive.gate.remoteControl.AppManager
import com.uglive.gate.remoteControl.bean.*
import com.xuhao.didi.core.pojo.OriginalData
import com.xuhao.didi.socket.client.impl.client.action.ActionDispatcher
import com.xuhao.didi.socket.client.sdk.OkSocket
import com.xuhao.didi.socket.client.sdk.client.ConnectionInfo
import com.xuhao.didi.socket.client.sdk.client.OkSocketOptions
import com.xuhao.didi.socket.client.sdk.client.action.SocketActionAdapter
import com.xuhao.didi.socket.client.sdk.client.connection.IConnectionManager
import java.nio.charset.Charset

const val RECEIVER_ACTION_UPGRADE_APP = "RECEIVER_ACTION_UPGRADE_APP"
class RemoteControlService : Service() {

    private val mGson = Gson()

    private val TAG ="RemoteControlService"

    /**
     * APP当前在主页
     */
    private var inMainPage = true



    /**
     *  服务初始化事件,同当前时间-该时间>10*1000ms在心跳时候确定是否需要发送相机状态是否卡死
     */
    private val initTime = System.currentTimeMillis()
    /**
     * 是否已经发送了相机卡死消息
     */
    private var areadySendCameraDeadMsg = false

    //todo 修改IP和端口号
    private val mConnectionInfo = ConnectionInfo("${Config.IP}", 60001)
//    private val mConnectionInfo = ConnectionInfo("192.168.3.36", 60001)

    var mManager: IConnectionManager? = null

    /**
     * 连接状态改变监听器
     */
    var onConnectChangeListener: ((Boolean)->Unit)? = null
    /**
     * 配置曝光度
     */
    var onConfigExposure: ((Int) -> Unit)? = null
    /**
     * 配置白平衡
     */
    var onConfigWhiteBalance: ((Int) -> Unit)? = null
    /**
     * 改变设备名称
     */
    var onChangeNicknameListener:((String) -> Unit)? = null

    var onChangePushStream:((String)->Unit)? = null

    var onDelFace:((String)-> Unit)? = null

    val mSocketActionAdapter = object : SocketActionAdapter() {
        override fun onSocketConnectionSuccess(info: ConnectionInfo?, action: String?) {
            //连接成功后进行握手操作
            mManager?.send(HandShakeBean())
            // 心跳
            mManager?.pulseManager?.pulseSendable = PulseBean()
            mManager?.pulseManager?.pulse()
            onConnectChangeListener?.let { it(true) }
        }

        override fun onSocketDisconnection(info: ConnectionInfo?, action: String?, e: Exception?) {
            e?.let {
                if (e is RedirectException) {
                    mManager?.switchConnectionInfo(e.redirectInfo)
                    mManager?.connect()
                    Log.i(TAG, "正在重定向连接(Redirect Connecting)")
                } else {
                    Log.i(TAG, "异常断开(Disconnected with exception):${e.message}")
                }
            } ?: Log.i(TAG, "正常断开(Disconnect Manually)")
            onConnectChangeListener?.let { it(false) }
        }

        override fun onSocketConnectionFailed(info: ConnectionInfo?, action: String?, e: Exception?) {
            Log.i(TAG, "连接失败(Connecting Failed)")
        }

        override fun onSocketReadResponse(info: ConnectionInfo?, action: String?, data: OriginalData?) {
            data?.let { originalData ->
                val str = String(originalData.bodyBytes, Charset.forName("utf-8"))
                val socketMsg = mGson.fromJson<SocketMsg>(str, SocketMsg::class.java)
                val time = System.currentTimeMillis()
                val isCameraStuck = time - (Utils.getApp() as App).lastOnPreviewTimestamp > 3000L
                when (socketMsg.cmd) {
                    CMDS.handshake -> {//握手成功
                        val defaultSendBean = DefaultSendBean()
                        val device = AppManager.genDevice()
                        if(time - initTime > 10*1000L && isCameraStuck)
                            device.status = Device.STATUS_CAMERA_DEAD
                        else
                            device.status = Device.STATUS_CONNECT
                        if (inMainPage) device.appPage = 0
                        else device.appPage = 1
                        defaultSendBean.content = mGson.toJson(SocketMsg(CMDS.deviceInfo, mGson.toJson(device)))
                        mManager?.send(defaultSendBean)
                        Log.i(TAG,"握手成功! 握手信息(Handshake Success):${socketMsg.dataJson}. 开始心跳(Start Heartbeat)..")
                    }
                    CMDS.heartbeat -> {//心跳
                        Log.i(TAG,"收到心跳,喂狗成功(Heartbeat Received,Feed the Dog)")
                        //若发现相机卡死则发送相机卡死心跳包(满足三个条件：socket启动10后，在主页，没有发送卡死消息)
                        if(System.currentTimeMillis() - initTime > 10*1000L && inMainPage && !areadySendCameraDeadMsg){
                            if (isCameraStuck){
                                val defaultSendBean = DefaultSendBean()
                                defaultSendBean.content = mGson.toJson(SocketMsg(CMDS.deviceStatus,
                                        mGson.toJson(mapOf("androidId" to DeviceUtils.getAndroidID(), "status" to Device.STATUS_CAMERA_DEAD))))
                                mManager?.send(defaultSendBean)
                            }
                        }
                        mManager?.pulseManager?.feed()
                    }
                    // 接受到服务器返回时才停止发送相机死亡消息
                    CMDS.deviceStatus -> areadySendCameraDeadMsg = true
                    CMDS.restartDevice ->{
                        //关机前主动断开连接，不然服务器会保持该连接
                        mManager?.disconnect()
                        AppManager.restartDevice()
                    }
                    CMDS.openGate -> AppManager.openGate()
                    CMDS.restartApp-> AppManager.restartApp(this@RemoteControlService)
                    CMDS.configDevice -> {
                        AppManager.config(mGson.fromJson(socketMsg.dataJson,Device::class.java))
                        this@RemoteControlService.sMsgDeviceConfig(AppManager.genDevice())
                        AppManager.restartApp(this@RemoteControlService)
                    }
                    CMDS.upgrade -> sendBroadcast(Intent(RECEIVER_ACTION_UPGRADE_APP))
                    CMDS.configExposure -> {
                        val position = socketMsg.dataJson.toInt()
                        AppManager.saveExposureCompensation(position)
                        onConfigExposure?.let { it(position) }
                        this@RemoteControlService.sMsgDeviceConfig(AppManager.genDevice())
                    }
                    CMDS.configWhiteBalance -> {
                        val position = socketMsg.dataJson.toInt()
                        AppManager.saveWhiteBalance(position)
                        onConfigWhiteBalance?.let { it(position) }
                        this@RemoteControlService.sMsgDeviceConfig(AppManager.genDevice())
                    }
                    CMDS.changeNickName ->{
                        AppManager.saveNickName(socketMsg.dataJson)
                        onChangeNicknameListener?.let { it(socketMsg.dataJson) }
                        this@RemoteControlService.sMsgDeviceConfig(AppManager.genDevice())
                    }
                    CMDS.changePushStream ->{
                        if (socketMsg.dataJson.toBoolean() != Config.isPushStream()) {
                            AppManager.savePushStream(socketMsg.dataJson)
                            onChangePushStream?.let { it(socketMsg.dataJson) }
                            this@RemoteControlService.sMsgDeviceConfig(AppManager.genDevice())
                        }else{}
                    }
                    CMDS.deleteFace -> onDelFace?.let { it(socketMsg.dataJson) }
                    else -> Log.i(TAG,"收到远程控制信息： $str")
                }
            }
        }


    }

    inner class MyBinder:Binder(){

        /**
         * 连接状态改变监听器
         */
        fun setOnConnectChangeListener(onConnectChangeListener: ((Boolean)->Unit)?){
            this@RemoteControlService.onConnectChangeListener = onConnectChangeListener
        }


        /**
         * 配置曝光度监听器
         */
        fun setOnConfigExposure(onConfigExposure: ((Int)->Unit)?){
            this@RemoteControlService.onConfigExposure = onConfigExposure
        }


        /**
         * 配置白平衡监听器
         */
        fun setOnConfigWhiteBalance(onConfigWhiteBalance: ((Int)->Unit)?){
            this@RemoteControlService.onConfigWhiteBalance = onConfigWhiteBalance
        }


        /**
         * 设备名称改变监听器
         */
        fun setOnChangeNicknameListener(onChangeNicknameListener:((String) -> Unit)?){
            this@RemoteControlService.onChangeNicknameListener = onChangeNicknameListener
        }

        fun setOnChangePushStream(onChangePushStream: ((String) -> Unit)?){
            this@RemoteControlService.onChangePushStream = onChangePushStream
        }

        /**
         * 删除人脸图片监听器
         */
        fun setOnDelFaceListener(onDelFace: ((String)-> Unit)?){
            this@RemoteControlService.onDelFace = onDelFace
        }


        /**
         * 发送APP改变页面的消息
         */
        fun sMsgChangePage(page: String){
            if (page!="main") inMainPage = false
            val defaultSendBean = DefaultSendBean()
            defaultSendBean.content = mGson.toJson(SocketMsg(CMDS.changePage,
                    mGson.toJson(mapOf("androidId" to DeviceUtils.getAndroidID(), "page" to page))))
            mManager?.send(defaultSendBean)
        }

        /**
         * 发送修改后的配置
         */
        fun sMsgDeviceConfig(config: Device){
            this@RemoteControlService.sMsgDeviceConfig(config)
        }

        fun stopService(){
            this@RemoteControlService.stopSelf()
        }


    }

    override fun onBind(intent: Intent) = MyBinder()

    fun sMsgDeviceConfig(config: Device){
        val defaultSendBean = DefaultSendBean()
        defaultSendBean.content = mGson.toJson(SocketMsg(CMDS.deviceConfig, mGson.toJson(config)))
        mManager?.send(defaultSendBean)
    }

    private fun initConnect() {

        val builder = OkSocketOptions.Builder()
        //设置心跳频率
        builder.setPulseFrequency(2000L)
        //设置断开重连
        builder.setReconnectionManager(OkSocketOptions.getDefault().reconnectionManager)
        builder.setCallbackThreadModeToken(object : OkSocketOptions.ThreadModeToken() {
            override fun handleCallbackEvent(runnable: ActionDispatcher.ActionRunnable) {
                AppExecutors.networkIO.execute(runnable)
            }
        })
        mManager = OkSocket.open(mConnectionInfo).option(builder.build())
        mManager?.registerReceiver(mSocketActionAdapter)
        mManager?.connect()
    }

    override fun onCreate() {
        super.onCreate()
        Handler().postDelayed({
            initConnect()
        }, 2000)
    }


    override fun onDestroy() {
        mManager?.unRegisterReceiver(mSocketActionAdapter)
        mManager?.disconnect()
        super.onDestroy()
    }
}
