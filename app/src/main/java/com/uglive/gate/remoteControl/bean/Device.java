package com.uglive.gate.remoteControl.bean;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author xiantm
 * @since 2018-11-28
 */

public class Device implements Serializable {
    /**
     * 设备正常连接状态
     */
    public final static Integer STATUS_CONNECT = 0;
    /**
     * 设备相机卡死状态
     */
    public final static Integer STATUS_CAMERA_DEAD = 1;
    /**
     * 设备断开连接状态
     */
    public final static Integer STATUS_DISCONNECT = 2;

    /**
     * 手持
     */
    public final static Integer TYPE_PHONE = 0;
    /**
     * 平板
     */
    public final static Integer TYPE_TABLET = 1;
    /**
     * 用于配置
     */
    public final static Integer TYPE_CONFIG = 2;

    private static final long serialVersionUID = 1L;

    /**
     * Android唯一ID
     */
    private String androidId;

    /**
     * Android系统版本
     */
    private Integer sdkVersion;

    /**
     * 设备类型
     */
    private Integer deviceType;

    /**
     * 设备别名
     */
    private String nickName;

    /**
     * 0 正常，1相机卡死，2断开连接
     */
    private Integer status;

    /**
     * 0首页，配置页
     */
    private Integer appPage;

    /**
     * 设备当前版本
     */
    private String appVersion;

    /**
     * 只需要认证电子标签 0
     只需要认证二维码1
     认证电子标签+二维码 2
     认证身份证 3
     对比人脸 4
     认证电子标签或二维码 5
     认证身份证+二维码 6
     只需要认证电子标签 7
     认证电子标签+二维码 8
     认证电子标签或二维码9
     default 0
     */
    private Integer scanType;

    /**
     * 秘钥
     */
    private String secrectKey;

    /**
     * 编号
     */
    private String deviceName;

    /**
     * 人脸验证类型
     不验证直接上传人脸信息 0
     通过本地图片信息验证人脸 1
     请求服务器验证 2
     */
    private Integer faceValidateType;

    /**
     * 活动ID
     */
    private Integer projectId;

    /**
     * 人脸识别准确度
     */
    private Float faceDegree;

    /**
     * 扫到电子标签就可以进入
     */
    private Boolean onlyScanRfid;

    /**
     * 推送到公安网的IP地址
     */
    private String gonganIp;

    /**
     * 相机截取人脸的有效范围
     */
    private Float cameraScale;

    /**
     * 厂家加密的密码
     */
    private String constantSecret;

    /**
     * 是否通过厂家加密
     */
    private Boolean encByFactory;

    /**
     * 上传到成都公安
     */
    private Boolean uploadChenduPolice;

    /**
     * 使用键盘协议的二维码扫描仪 default false 新板子有用
     */
    private Boolean useKeyboardScanQrcodeDevice;

    /**
     * 是否需要秘钥,读取UID选false
     */
    private Boolean needSecret;

    /**
     * 截取图片是否必须有人脸
     */
    private Boolean captureNeedFace;

    /**
     * 对比身份证中的人脸
     */
    private Boolean compareIDCardFace;

    /**
     * 预览宽
     */
    private Integer cameraPreviewWidth;

    /**
     * 预览高
     */
    private Integer cameraPreviwHeight;

    /**
     * 相机白平衡
     */
    private Integer cameraWhiteBalance;

    /**
     * 相机曝光补偿
     */
    private Integer cameraExposureCompensation;

    private Boolean pushStream;


    public Boolean getPushStream() { return pushStream; }

    public void setPushStream(Boolean pushStream) { this.pushStream = pushStream; }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }
    public Integer getSdkVersion() {
        return sdkVersion;
    }

    public void setSdkVersion(Integer sdkVersion) {
        this.sdkVersion = sdkVersion;
    }
    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Integer getAppPage() {
        return appPage;
    }

    public void setAppPage(Integer appPage) {
        this.appPage = appPage;
    }
    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
    public Integer getScanType() {
        return scanType;
    }

    public void setScanType(Integer scanType) {
        this.scanType = scanType;
    }
    public String getSecrectKey() {
        return secrectKey;
    }

    public void setSecrectKey(String secrectKey) {
        this.secrectKey = secrectKey;
    }
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
    public Integer getFaceValidateType() {
        return faceValidateType;
    }

    public void setFaceValidateType(Integer faceValidateType) {
        this.faceValidateType = faceValidateType;
    }
    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
    public Float getFaceDegree() {
        return faceDegree;
    }

    public void setFaceDegree(Float faceDegree) {
        this.faceDegree = faceDegree;
    }
    public Boolean getOnlyScanRfid() {
        return onlyScanRfid;
    }

    public void setOnlyScanRfid(Boolean onlyScanRfid) {
        this.onlyScanRfid = onlyScanRfid;
    }
    public String getGonganIp() {
        return gonganIp;
    }

    public void setGonganIp(String gonganIp) {
        this.gonganIp = gonganIp;
    }
    public Float getCameraScale() {
        return cameraScale;
    }

    public void setCameraScale(Float cameraScale) {
        this.cameraScale = cameraScale;
    }
    public String getConstantSecret() {
        return constantSecret;
    }

    public void setConstantSecret(String constantSecret) {
        this.constantSecret = constantSecret;
    }
    public Boolean getEncByFactory() {
        return encByFactory;
    }

    public void setEncByFactory(Boolean encByFactory) {
        this.encByFactory = encByFactory;
    }
    public Boolean getUploadChenduPolice() {
        return uploadChenduPolice;
    }

    public void setUploadChenduPolice(Boolean uploadChenduPolice) {
        this.uploadChenduPolice = uploadChenduPolice;
    }
    public Boolean getUseKeyboardScanQrcodeDevice() {
        return useKeyboardScanQrcodeDevice;
    }

    public void setUseKeyboardScanQrcodeDevice(Boolean useKeyboardScanQrcodeDevice) {
        this.useKeyboardScanQrcodeDevice = useKeyboardScanQrcodeDevice;
    }
    public Boolean getNeedSecret() {
        return needSecret;
    }

    public void setNeedSecret(Boolean needSecret) {
        this.needSecret = needSecret;
    }
    public Integer getCameraPreviewWidth() {
        return cameraPreviewWidth;
    }

    public void setCameraPreviewWidth(Integer cameraPreviewWidth) {
        this.cameraPreviewWidth = cameraPreviewWidth;
    }
    public Integer getCameraPreviwHeight() {
        return cameraPreviwHeight;
    }

    public void setCameraPreviwHeight(Integer cameraPreviwHeight) {
        this.cameraPreviwHeight = cameraPreviwHeight;
    }
    public Integer getCameraWhiteBalance() {
        return cameraWhiteBalance;
    }

    public void setCameraWhiteBalance(Integer cameraWhiteBalance) {
        this.cameraWhiteBalance = cameraWhiteBalance;
    }
    public Integer getCameraExposureCompensation() {
        return cameraExposureCompensation;
    }

    public void setCameraExposureCompensation(Integer cameraExposureCompensation) {
        this.cameraExposureCompensation = cameraExposureCompensation;
    }

    public Boolean getCaptureNeedFace() {
        return captureNeedFace;
    }

    public void setCaptureNeedFace(Boolean captureNeedFace) {
        this.captureNeedFace = captureNeedFace;
    }

    public Boolean getCompareIDCardFace() {
        return compareIDCardFace;
    }

    public void setCompareIDCardFace(Boolean compareIDCardFace) {
        this.compareIDCardFace = compareIDCardFace;
    }

    @Override
    public String toString() {
        return "Device{" +
                "androidId='" + androidId + '\'' +
                ", sdkVersion=" + sdkVersion +
                ", deviceType=" + deviceType +
                ", nickName='" + nickName + '\'' +
                ", status=" + status +
                ", appPage=" + appPage +
                ", appVersion='" + appVersion + '\'' +
                ", scanType=" + scanType +
                ", secrectKey='" + secrectKey + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", faceValidateType=" + faceValidateType +
                ", projectId=" + projectId +
                ", faceDegree=" + faceDegree +
                ", uploadValidateInfo=" + onlyScanRfid +
                ", gonganIp='" + gonganIp + '\'' +
                ", cameraScale=" + cameraScale +
                ", constantSecret='" + constantSecret + '\'' +
                ", encByFactory=" + encByFactory +
                ", uploadChenduPolice=" + uploadChenduPolice +
                ", useKeyboardScanQrcodeDevice=" + useKeyboardScanQrcodeDevice +
                ", needSecret=" + needSecret +
                ", captureNeedFace=" + captureNeedFace +
                ", compareIDCardFace=" + compareIDCardFace +
                ", cameraPreviewWidth=" + cameraPreviewWidth +
                ", cameraPreviwHeight=" + cameraPreviwHeight +
                ", cameraWhiteBalance=" + cameraWhiteBalance +
                ", cameraExposureCompensation=" + cameraExposureCompensation +
                '}';
    }
}
