package com.uglive.gate.device.scanQrcode


interface IScanQrDevice {
    fun setDataCallBack(callback :(String) -> Unit)
    fun close()
}