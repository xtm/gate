package com.uglive.gate.device.scanCard

import com.uglive.gate.Config
import com.uglive.gate.device.DeviceType
import com.uglive.gate.model.SCAN_TYPE_RFID_AND_QRCODE_HIGH
import com.uglive.gate.model.SCAN_TYPE_RFID_ONLY_HIGH
import com.uglive.gate.model.SCAN_TYPE_RFID_OR_QRCODE_HIGH


object ScanCardFactory {
    /**
     * @param type "tablet闸机,phone手持机"
     */
    fun create(type: String): IScanCardDevice {
        if (type == "tablet") {
            if (Config.scanType == SCAN_TYPE_RFID_OR_QRCODE_HIGH
                    || Config.scanType == SCAN_TYPE_RFID_AND_QRCODE_HIGH
                    || Config.scanType == SCAN_TYPE_RFID_ONLY_HIGH)
                return HighFrequencyRfidDevice()
            else return TabletIScanCardDevice()
        }
        return PhoneScanCardDevice()
    }

    fun create(type: DeviceType): IScanCardDevice = when (type) {
        DeviceType.TABLET, DeviceType.TABLET_KEYBOARD -> {
            if (Config.scanType == SCAN_TYPE_RFID_OR_QRCODE_HIGH
                    || Config.scanType == SCAN_TYPE_RFID_AND_QRCODE_HIGH
                    || Config.scanType == SCAN_TYPE_RFID_ONLY_HIGH)
                HighFrequencyRfidDevice()
            else TabletIScanCardDevice()
        }
        DeviceType.PHONE -> PhoneScanCardDevice()
    }

}

