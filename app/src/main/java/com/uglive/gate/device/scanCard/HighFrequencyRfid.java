package com.uglive.gate.device.scanCard;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.uglive.gate.utils.ByteUtil;
import com.uglive.gate.utils.MyFunc;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;

import android_serialport_api.SerialPort;

public class HighFrequencyRfid {

    private SerialPort mSerialPort;
    private OutputStream mOutputStream;
    private InputStream mInputStream;
    private String sPort = Build.VERSION.SDK_INT == 19? "/dev/ttyS3" : "/dev/ttyS1" ;
    private int iBaudRate = 115200;
    private int TIMES = 10;
    String tidInstruction = genInstruction("A55A0000840000000000000000000200000006000D0A");//TID读取指令
    String epcInstruction = genInstruction("A55A0000840000000000000000000100020006000D0A");//EPC读取指令
    String beepInstruction = genInstruction("A55A000A5600005C0D0A");
    private OnScanListener onScanListener;
    private boolean alive = true;
    private Thread mScanThread = new Thread() {
        @Override
        public void run() {
            while (alive) {
                String epc = getEpc();
                String tid = getTid();
                if (!TextUtils.isEmpty(epc) && !TextUtils.isEmpty(tid)) {
                    if (onScanListener != null) onScanListener.onScan(tid, epc);
                }
            }
        }
    };

    public void setOnScanListener(OnScanListener onScanListener) {
        this.onScanListener = onScanListener;
    }

    public interface OnScanListener {
        void onScan(String tid, String epc);
    }

    public void beep(){
        sendHex(beepInstruction);
        readFrame();
    }


    public void open() throws SecurityException, IOException, InvalidParameterException {
        mSerialPort = new SerialPort(new File(sPort), iBaudRate, 0);
        mOutputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();
        mScanThread.start();
    }

    public void close() {
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
        try {
            mInputStream.close();
            mOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        alive = false;
        if (mScanThread.isAlive()) {
            mScanThread.interrupt();
        }
    }

    public void send(byte[] bOutArray) {
        try {
            mOutputStream.write(bOutArray);
            mOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void sendHex(String sHex) {
        byte[] bOutArray = MyFunc.HexToByteArr(sHex);
        send(bOutArray);
    }



    public void stopScanLoop() {
        sendHex("A55A00088C840D0A");
        try {
            mOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        readFrame();
    }

    //读取数据帧
    int CACHE_SIZE = 1024;
    byte[] cache = new byte[CACHE_SIZE];
    byte[] buffer = new byte[128];
    int cacheCount = 0;//已用缓存

    public synchronized String readFrame() {

        clearCache();
        int loopTime = 2;
        if (mInputStream == null) return "";
        while (loopTime-- > 0) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int size = 0;
            try {
                size = mInputStream.read(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (cacheCount + size > CACHE_SIZE) clearCache();
            for (int i = 0; i < size; i++) {
                cache[i + cacheCount] = buffer[i];
            }
            cacheCount += size;
            byte[] frame = getFrame(cache,cacheCount);
            if (null != frame) {
                String result = ByteUtil.bytesToHexString(frame);
                return result;
            }
        }
        return null;
    }

    private void clearCache() {
        cacheCount = 0;
        for (int i = 0; i < cache.length; i++) {
            cache[i] = 0;
        }
    }

    /**
     * 获取帧
     * 没有完整帧就返回空
     *
     * @param tempData
     * @param dataTail 数据末尾地址
     */
    private byte[] getFrame(byte[] tempData,int dataTail) {
        int head = -1;
        int tail = -1;
        //找头尾
        for (int i = 0; i < dataTail - 1; i++) {
            if (tempData[i] == (byte) 0xA5 && tempData[i + 1] == (byte) 0x5A) {
                head = i;
            }
            if (tempData[dataTail - 1 - i] == (byte) 0x0A && tempData[dataTail - 1 - i - 1] == (byte) 0x0D) {
                tail = dataTail - 1 - i;
            }
        }
        //取数据
        if (head != -1 && tail != -1) {
            if (tail > head) {
                byte[] data = new byte[tail - head + 1];
                for (int i = head, j = 0; i <= tail; i++, j++) {
                    data[j] = tempData[i];
                }
                return data;
            } else { //清除无效数据
                for (int i = 0; i <= tail; i++) {
                    tempData[i] = 0;
                }
            }
        }
        return null;
    }


    public String getEpc() {
        for (int i = 0; i < TIMES; i++) {
            sendHex(epcInstruction);
            String result = readFrame();
            String data = parseFrame(result);
            if (!TextUtils.isEmpty(data))
                return data;
        }
        return null;
    }

    public String getTid() {
        for (int i = 0; i < TIMES; i++) {
            sendHex(tidInstruction);
            String result = readFrame();
            String data = parseFrame(result);
            if (!TextUtils.isEmpty(data))
                return data;
        }
        return null;
    }

    public void scanRfid() {
        String epc = getEpc();
        String tid = getTid();
        Log.e("tid", "---  " + tid + " ---");
        Log.e("epc", "---  " + epc + " ---");
    }


    private String parseFrame(String frame) {
        if (TextUtils.isEmpty(frame) || Integer.parseInt(frame.substring(6, 8), 16) <= 10)
            return null;
        return frame.substring(9 * 2, 21 * 2);
    }

    /**
     * 生成计算指令的长度和校验码后的指令
     *
     * @param hexString
     */
    public String genInstruction(String hexString) {
        StringBuilder src = new StringBuilder(hexString);
        //计算帧长度
        int length = src.length() / 2;
        String hexLength = Integer.toHexString(length);
        src.replace(6, 8, hexLength);
        byte[] ba = MyFunc.HexToByteArr(src.toString());
        //计算校验码
        byte crc = 0;
        for (int i = 2; i < ba.length - 3; i++) {
            crc ^= ba[i];
        }
        ba[length - 3] = crc;
        return ByteUtil.bytesToHexString(ba).toUpperCase();
    }

    public void printHexString(String result) {
        int length = result.length();
//        if (Integer.parseInt(result.substring(6, 8), 16) <= 10)
//            return;
        StringBuilder sb = new StringBuilder();
        for (int i = 2; i <= length; i += 2) {
            sb.append(result.substring(i - 2, i) + "\t");
        }
        Log.e("scan", "---  " + sb.toString() + "  ----");
    }
}
