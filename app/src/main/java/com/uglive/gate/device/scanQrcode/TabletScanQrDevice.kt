package com.uglive.gate.device.scanQrcode

import android.util.Log
import com.blankj.utilcode.util.DeviceUtils
import com.uglive.gate.Config
import com.uglive.gate.utils.SerialHelper
import java.io.IOException
import java.security.InvalidParameterException

/**
 * 扫二维码设备
 */
class TabletScanQrDevice : SerialHelper(), IScanQrDevice {
    override fun onDataReceived(text: String?) {
        text?.let { t -> qrResultCallback?.let {
            val t1 = if (Config.isTest && t.length>16) t.substring(0,16) else t
            if (sdkVersion>19){
                //
            }else it(t1)
        } }
    }
    private val TAG = "TabletScanQrDevice"
    private var qrResultCallback: ((String) -> Unit)? = null
    private val sdkVersion = DeviceUtils.getSDKVersionCode()
    init {
        //firefly的机器Android版本大于19,
        port = if (sdkVersion>19) "/dev/ttyS0" else "/dev/ttyS4"

        baudRate = if (Config.isTest) 57600 else 115200
        try {
            this.open()
        } catch (e: SecurityException) {
            Log.i(TAG, "SecurityException" + e.toString())
        } catch (e: IOException) {
            Log.i(TAG, "IOException" + e.toString())
        } catch (e: InvalidParameterException) {
            Log.i(TAG, "InvalidParameterException" + e.toString())
        }
    }


    override fun setDataCallBack(callback: (String) -> Unit) {
        this.qrResultCallback = callback
    }

    override fun close() {
        super.close()
        Log.i(TAG, "二维码扫描串口已关闭")
    }
}