package com.uglive.gate.device.scanCard

import android.content.Context
import com.decard.entitys.IDCard
import com.gewara.rfid.RfidUtils
import java.io.IOException
import java.security.InvalidParameterException

/**
 * 猫眼验证->错误
 */
val STATUS_0 = "STATUS0"
/**
 * 猫眼验证->非猫眼票
 */
val STATUS_4 = "STATUS4"

class HighFrequencyRfidDevice : IScanCardDevice {

    private val highFrequencyRfid = HighFrequencyRfid()

    override fun open(context: Context, needScanM1: M1ScanType, needScanIDCard: Boolean, onScanM1: (String) -> Unit, onScanIDCard: (IDCard) -> Unit): Boolean {
        highFrequencyRfid.setOnScanListener { tid, epc ->
            if (needScanM1 == M1ScanType.USE_SECRET) {

                val rfidResult = RfidUtils.checkRfid(context, tid.toUpperCase(), epc.toUpperCase())
//                todo 测试用，测试以后将下面注释去掉
//                when (rfidResult.status) {
//                    0 -> onScanM1(STATUS_0)
//                    4 -> onScanM1(STATUS_4)
//                    8 -> onScanM1(rfidResult.code)
//                }
//                todo 测试用，测试以后将下面代码删去
                onScanM1(epc)
            }else{
                onScanM1(epc)
            }
        }
        try {
            highFrequencyRfid.open()
        } catch (se: SecurityException) {
            return false
        } catch (ioe: IOException) {
            return false
        } catch (ipe: InvalidParameterException) {
            return false
        }
        return true
    }

    override fun close(): Boolean {
        highFrequencyRfid.close()
        return true
    }
}