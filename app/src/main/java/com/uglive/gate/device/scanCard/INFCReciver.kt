package com.uglive.gate.device.scanCard

import android.nfc.Tag

/**
 *
 */
interface INFCReciver {
    fun setOnNFCRecv(onNFCRecv: OnNFCRecv)
}

interface OnNFCRecv {
    fun onNFCRecv(tag: Tag)
}
