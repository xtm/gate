package com.uglive.gate.device.scanCard

import android.content.Context
import android.nfc.Tag
import android.nfc.tech.MifareClassic
import com.blankj.utilcode.util.LogUtils
import com.decard.entitys.IDCard
import com.uglive.gate.Config
import com.uglive.gate.utils.ByteUtil
import java.io.IOException
import java.util.concurrent.Executors

class PhoneScanCardDevice : IScanCardDevice {

    private val singleTask = Executors.newSingleThreadExecutor()
    override fun open(context: Context, needScanM1: M1ScanType, needScanIDCard: Boolean, onScanM1: (String) -> Unit, onScanIDCard: (IDCard) -> Unit): Boolean {
        val recv = context as INFCReciver
        recv.setOnNFCRecv(object : OnNFCRecv {
            override fun onNFCRecv(tag: Tag) {
                //扫M1卡
                if (needScanM1!= M1ScanType.USE_SECRET) {
                    //读取第四扇区
                    val selector = 4
                    MifareClassic.get(tag).use { mc ->
                        try {
                            //连接
                            mc.connect()
                            //验证
                            val key = Config.secretKey ?: ""
                            val auth = mc.authenticateSectorWithKeyA(selector, key.toByteArray())
                            if (auth) {
                                //获取扇区中的块数，我们的卡只需要读取第一块就行了
                                val blocks = mc.getBlockCountInSector(selector)
                                //获取第一个块的位置
                                val index = mc.sectorToBlock(selector)
//                        // 读取扇区的全部块
//                        for (i in index..(index + blocks)) {
//                            mc.readBlock(i)
//                        }
                                //我们只要读第一块就行了
                                val data = mc.readBlock(index)
                                //闸机上扫除的是大写字母，与闸机相同才能解码
                                val string = ByteUtil.bytesToHexString(data).toUpperCase()
                                val decode = ByteUtil.decode(string)
                                val info = decode.substring(0, 12)
                                //切换到工作线程
                                singleTask.execute { onScanM1(info) }
                            }
                        } catch (ioe: IOException) {
                            LogUtils.e("nfc-mc", "读取m1卡失败")
                        }
                    }

                }
            }
        })
        return true
    }

    override fun close(): Boolean {
        singleTask.shutdownNow()
        return true
    }
}