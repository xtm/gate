package com.uglive.gate.device.scanQrcode

import android.view.View
import yryp.lib.fireflycardlib.FireFlyQrcodeBarcode

/**
 * 通过键盘协议传数据的二维码扫描仪
 */
class KeyboardQrcodeDevice(val view:View) : IScanQrDevice {
    lateinit var qrCodeScan: FireFlyQrcodeBarcode

    override fun setDataCallBack(callback: (String) -> Unit) {
        qrCodeScan = FireFlyQrcodeBarcode(view)
        qrCodeScan.setOnQrcodeBarcodeScan(callback)
    }

    override fun close() {
        qrCodeScan.close()
    }

}