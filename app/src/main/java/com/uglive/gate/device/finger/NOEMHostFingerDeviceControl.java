package com.uglive.gate.device.finger;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.util.Log;

import com.uglive.gate.device.finger.core.DevComm;
import com.uglive.gate.device.finger.core.IUsbConnState;

import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 插在UART0口
 */
public abstract class NOEMHostFingerDeviceControl implements FingerDeviceController {
    private  final String TAG = "NOEMHostFingerDevice";
    private  DevComm mDevComm;

    private int mBaudrate = 115200;
    private String mSzDevice = "/dev/ttyS2";

    private ExecutorService mExecutorService;
    private byte[] mBinImage = new byte[1024*100];
    private byte[] mBmpImage = new byte[1024*100];
    /**
     * 最大指纹数(目前买的指纹识别模块最大支持500)
     */
    private int mMaxFpCount = 500;

    private boolean mCancel = true;

    public abstract void onStateMessage(String message);

    @Override
    public boolean open(Activity activity) {
        String[] wStrInfo = new String[1];
        if (mDevComm == null) {
            mDevComm = new DevComm(activity, mIConnectionHandler);
            if (!mDevComm.IsInit()) {
                if (mDevComm.OpenComm(mSzDevice, mBaudrate) == false) {
                    String message = "Failed init device!";
                    Log.e(TAG, message);
                    onStateMessage(message);
                    return false;
                }
            }
            if (mDevComm.Run_TestConnection() == DevComm.ERR_SUCCESS) {
                if (mDevComm.Run_GetDeviceInfo(wStrInfo) == DevComm.ERR_SUCCESS) {
                    String message  = "Device open success";
                    Log.i(TAG,message);
                    onStateMessage(message);
                    mExecutorService = Executors.newSingleThreadExecutor();
                    return true;
                } else{
                    String message = "Can not connect to device!";
                    Log.e(TAG,message);
                    onStateMessage(message);
                    return false;
                }
            } else {
                String message = "Can not connect to device!";
                Log.e(TAG, message);
                onStateMessage(message);
                mDevComm.CloseComm();
                return false;
            }
        } else {
            String message = "Device already opened";
            onStateMessage(message);
            Log.e(TAG,message);
            return true;
        }
    }


    @Override
    public void close() {
        if (mDevComm!=null) {
            mCancel = false;
            mDevComm.CloseComm();
            mExecutorService.shutdown();
        }
    }

    @Override
    public void cancelTask() {
        mCancel = false;
        //关闭LED背光灯
        mDevComm.Run_SLEDControl(0);
    }


    @Override
    public int enrollFinger(Action action) {

        int		wRet;
        int[]	wState = new int[1];

        if (!mDevComm.IsInit())
            return -1;

        //检测是否有空置指纹ID
        int emptyId = getEmptyID();
        if (emptyId==-1)
            return -2;

        //检查指纹数据是否存在
        wRet = mDevComm.Run_GetStatus(emptyId, wState);
        if (wRet != DevComm.ERR_SUCCESS)
        {
            Log.e(TAG,getErrorMsg(wRet));
            return -1;
        }
        if (wState[0] == DevComm.GD_TEMPLATE_NOT_EMPTY)
        {
            Log.e(TAG,"Template is already exist");
            return -2;
        }
        //开启背光LED灯
        mDevComm.Run_SLEDControl(1);
        mCancel = false;

        Future<Boolean> result = mExecutorService.submit((Callable<Boolean>) () -> {
            //指纹录入需要扫描次数
            int wEnrollStep = 0, wGenCount = 3;
            int[] wDupID = new int[1];
            int[] wWidth = new int[1];
            int[] wHeight = new int[1];
            while (wEnrollStep < wGenCount)
            {
                action.onAction("输入指纹，次数： #"+wEnrollStep +1+"!",null);
                // Capture
                if (capturing() < 0)
                    return false;

                action.onAction("释放指纹.",null);
                // Up Captured Image
                if (mDevComm.m_nConnected == 2) {
                    // image variables

                    int wRet1 = mDevComm.Run_UpImage(0, mBinImage, wWidth, wHeight);

                    if (wRet1 != DevComm.ERR_SUCCESS) {
                        action.onAction(getErrorMsg(wRet1),null);
                        return false;
                    }
                    Bitmap bitmap = makeBitmap(wWidth[0],wHeight[0]);
                 //todo 连接USB时显示图片
                }

                // Create Template
                int wRet2 = mDevComm.Run_Generate(wEnrollStep);

                if (wRet2 != DevComm.ERR_SUCCESS)
                {
                    if (wRet2 == DevComm.ERR_BAD_QUALITY)
                    {
                        action.onAction("指纹质量太差，请重试!",null);
                        continue;
                    }
                    else
                    {
                        action.onAction(getErrorMsg(wRet2),null);
                        return false;
                    }
                }
                wEnrollStep++;
            }
            //. Merge Template
            int wRet3 = mDevComm.Run_Merge(0, wGenCount);

            if (wRet3 != DevComm.ERR_SUCCESS) {
                    action.onAction(getErrorMsg(wRet3),null);
                    return false; 
            }
            //. Store template
           int  wRet4 = mDevComm.Run_StoreChar(emptyId, 0, wDupID);

            if (wRet4 != DevComm.ERR_SUCCESS)
            {
                if (wRet4 == DevComm.ERR_DUPLICATION_ID)
                    action.onAction( String.format("录入结果 : 失败\r\nDuplication ID = %d", wDupID[0]),null);
                else
                    action.onAction(getErrorMsg(wRet4),null);
                return false;
            }
            else{
                String val = String.format("录入结果 : 成功\r\nTemplate No : %d", emptyId);
                action.onAction(val,null);
                Log.i(TAG,val);
            }
            return true;
        });
        try {
            if (result.get()) return emptyId;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private Bitmap makeBitmap(int wWidth,int wHeight){
        // Draw image
        int	nSize;

        makeBMPBuf(mBinImage, mBmpImage,wWidth,wHeight);

        if ((wWidth % 4) != 0)
            nSize = wWidth + (4 - (wWidth % 4));
        else
            nSize = wWidth;

        nSize = 1078 + nSize * wHeight;
        //DebugManage.WriteBmp(m_bmpImage, nSize);
        return BitmapFactory.decodeByteArray(mBmpImage, 0, nSize);
    }

    private void makeBMPBuf(byte[] Input, byte[] Output, int iImageX, int iImageY) {
        byte[] w_bTemp = new byte[4];
        byte[] head = new byte[1078];
        byte[] head2 = {
                /***************************/
                //file header
                0x42,0x4d,//file type
                //0x36,0x6c,0x01,0x00, //file size***
                0x0,0x0,0x0,0x00, //file size***
                0x00,0x00, //reserved
                0x00,0x00,//reserved
                0x36,0x4,0x00,0x00,//head byte***
                /***************************/
                //infoheader
                0x28,0x00,0x00,0x00,//struct size

                //0x00,0x01,0x00,0x00,//map width***
                0x00,0x00,0x0,0x00,//map width***
                //0x68,0x01,0x00,0x00,//map height***
                0x00,0x00,0x00,0x00,//map height***

                0x01,0x00,//must be 1
                0x08,0x00,//color count***
                0x00,0x00,0x00,0x00, //compression
                //0x00,0x68,0x01,0x00,//data size***
                0x00,0x00,0x00,0x00,//data size***
                0x00,0x00,0x00,0x00, //dpix
                0x00,0x00,0x00,0x00, //dpiy
                0x00,0x00,0x00,0x00,//color used
                0x00,0x00,0x00,0x00,//color important
        };

        int	i,j, num, iImageStep;

        Arrays.fill(w_bTemp, (byte)0);

        System.arraycopy(head2, 0, head, 0, head2.length);

        if ((iImageX % 4) != 0)
            iImageStep = iImageX + (4 - (iImageX % 4));
        else
            iImageStep = iImageX;

        num=iImageX; head[18]= (byte)(num & (byte)0xFF);
        num=num>>8;  head[19]= (byte)(num & (byte)0xFF);
        num=num>>8;  head[20]= (byte)(num & (byte)0xFF);
        num=num>>8;  head[21]= (byte)(num & (byte)0xFF);

        num=iImageY; head[22]= (byte)(num & (byte)0xFF);
        num=num>>8;  head[23]= (byte)(num & (byte)0xFF);
        num=num>>8;  head[24]= (byte)(num & (byte)0xFF);
        num=num>>8;  head[25]= (byte)(num & (byte)0xFF);

        j=0;
        for (i=54;i<1078;i=i+4)
        {
            head[i]=head[i+1]=head[i+2]=(byte)j;
            head[i+3]=0;
            j++;
        }

        System.arraycopy(head, 0, Output, 0, 1078);

        if (iImageStep == iImageX){
            for( i = 0; i < iImageY; i ++){
                System.arraycopy(Input, i*iImageX, Output, 1078+i*iImageX, iImageX);
            }
        }
        else{
            iImageStep = iImageStep - iImageX;

            for( i = 0; i < iImageY; i ++){
                System.arraycopy(Input, i*iImageX, Output, 1078+i*(iImageX+iImageStep), iImageX);
                System.arraycopy(w_bTemp, 0, Output, 1078+i*(iImageX+iImageStep)+iImageX, iImageStep);
            }
        }
    }

    private int capturing(){
        int		wRet;
        while(true){
            wRet = mDevComm.Run_GetImage();
            if (wRet == DevComm.ERR_CONNECTION)
            {
                String message = "Communication error!";
                onStateMessage(message);
                Log.e(TAG,message);
                return -1;
            }
            else if (wRet == DevComm.ERR_SUCCESS)
                break;
            if (mCancel){
                return -1;
            }
        }
        return 0;
    }

    @Override
    public boolean validateFinger(int id) {
        int[]	wState = new int[1];
        if (!mDevComm.IsInit())
            return false;

        if (id>mMaxFpCount||id<1)
            return false;

        int wRet = mDevComm.Run_GetStatus(id, wState);
        if (wRet != DevComm.ERR_SUCCESS)
        {
            onStateMessage(getErrorMsg(wRet));
            return false;
        }

        if (wState[0] == DevComm.GD_TEMPLATE_EMPTY )
        {
            onStateMessage("该指纹未录入");
            return false;
        }
        onStateMessage("按下手指");
        mDevComm.Run_SLEDControl(1);
        mCancel = false;
        Future<Boolean> result = mExecutorService.submit(()->{
            int		w_nRet;
            int[]	w_nLearned = new int[1];
            int[]	w_nWidth = new int[1];
            int[]	w_nHeight = new int[1];
            if (capturing() < 0)
                return false;
            onStateMessage("抬起手指");

            // Up Cpatured Image
            if (mDevComm.m_nConnected == 2) {
                w_nRet = mDevComm.Run_UpImage(0, mBinImage, w_nWidth, w_nHeight);

                if (w_nRet != DevComm.ERR_SUCCESS) {
                    onStateMessage(getErrorMsg(w_nRet));
                    return false;
                }
                Bitmap bitmap = makeBitmap(w_nWidth[0],w_nHeight[0]);
                //todo 连接USB时显示图片
            }

            // Create template
            long mPassedTime = SystemClock.elapsedRealtime();
            w_nRet = mDevComm.Run_Generate(0);

            if (w_nRet != DevComm.ERR_SUCCESS)
            {
                onStateMessage(getErrorMsg(w_nRet));
                return false;
            }

            // Verify
            w_nRet = mDevComm.Run_Verify(id, 0, w_nLearned);
            mPassedTime = SystemClock.elapsedRealtime() - mPassedTime;
            if (w_nRet == DevComm.ERR_SUCCESS)
               onStateMessage(String.format("Result : Success\r\nTemplate No : %d, Learn Result : %d\r\n" +
                       "Match Time : %dms",id, w_nLearned[0], mPassedTime));
            else{
                onStateMessage(getErrorMsg(w_nRet));
                return false;
            }
            return  true;
        });
        try {
            return result.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public int identify() {

        if (!mDevComm.IsInit())
            return -1;

        mDevComm.Run_SLEDControl(1);
        mCancel = false;



        Future<Integer> result = mExecutorService.submit(()->{
            int		w_nRet;
            int[]	w_nID = new int[1];
            int[]	w_nLearned = new int[1];
            int[]	w_nWidth = new int[1];
            int[]	w_nHeight = new int[1];
            while(true) {
                onStateMessage("按下手指");

                if (capturing() < 0)
                    return -1;

                onStateMessage("抬起手指");
                // Up Cpatured Image
                if (mDevComm.m_nConnected == 2) {
                    w_nRet = mDevComm.Run_UpImage(0, mBinImage, w_nWidth, w_nHeight);

                    if (w_nRet != DevComm.ERR_SUCCESS) {
                        onStateMessage(getErrorMsg(w_nRet));
                        return -1;
                    }
                    Bitmap bitmap = makeBitmap(w_nWidth[0],w_nHeight[0]);
                    //todo 连接USB时显示图片
                }

                // Create template
                long m_nPassedTime = SystemClock.elapsedRealtime();
                w_nRet = mDevComm.Run_Generate(0);

                if (w_nRet != DevComm.ERR_SUCCESS)
                {
                    onStateMessage(getErrorMsg(w_nRet));

                    if (w_nRet == DevComm.ERR_CONNECTION) {
                        return -1;
                    } else {
                        SystemClock.sleep(1000);
                        continue;
                    }
                }

                // Identify
                w_nRet = mDevComm.Run_Search(0, 1, mMaxFpCount, w_nID, w_nLearned);
                m_nPassedTime = SystemClock.elapsedRealtime() - m_nPassedTime;

                if (w_nRet == DevComm.ERR_SUCCESS) {
                    onStateMessage(String.format("Result : Success\r\nTemplate No : %d, Learn Result :" +
                                    " %d\r\nMatch Time : %dms", w_nID[0], w_nLearned[0], m_nPassedTime));
                    return  w_nID[0];
                } else {
                    String message = String.format("\r\nMatch Time : %dms", m_nPassedTime);
                    onStateMessage(getErrorMsg(w_nRet) + message);
                }
                return -1;
            }
        });
        try {
            return result.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int getEnrollCount() {
        int w_nRet;
        int[] w_nEnrollCount = new int[1];

        if (!mDevComm.IsInit())
            return -1;

        w_nRet = mDevComm.Run_GetEnrollCount(1, mMaxFpCount, w_nEnrollCount);

        if (w_nRet != DevComm.ERR_SUCCESS)
        {
           onStateMessage(getErrorMsg(w_nRet));
            return -1;
        }
        return w_nEnrollCount[0];
    }

    @Override
    public int getEmptyID() {
        int wRet;
        int[] wEmptyID = new int[1];

        if (!mDevComm.IsInit())
            return -1;
        wRet = mDevComm.Run_GetEmptyID(1, mMaxFpCount, wEmptyID);

        if (wRet != DevComm.ERR_SUCCESS)
        {
            Log.e(TAG,getErrorMsg(wRet));
            return -1;
        }
        return wEmptyID[0];
    }

    @Override
    public Bitmap captureImage() {
        mDevComm.Run_SLEDControl(1);
        mCancel = false;
        onStateMessage("按下指纹");
        Future<Bitmap> bitmapFuture = mExecutorService.submit(()->{
            int		w_nRet;
            int[] 	width = new int[1];
            int[] 	height = new int[1];
            if (capturing() < 0)
                return null;
            onStateMessage("抬起手指\n上传图像中...");
            w_nRet = mDevComm.Run_UpImage(0, mBinImage, width, height);

            if (w_nRet != DevComm.ERR_SUCCESS)
            {
                onStateMessage(getErrorMsg(w_nRet));
                return null;
            }
            return makeBitmap(width[0],height[0]);
        });
        try {
            return bitmapFuture.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteFinger(int fingerId) {

       Future<Boolean> result= mExecutorService.submit(() -> {
            int	w_nRet;
            if (!mDevComm.IsInit())
                return false;
            if (fingerId < 1 ||fingerId > mMaxFpCount)
                return false;
            w_nRet = mDevComm.Run_DelChar(fingerId, fingerId);

            if (w_nRet != DevComm.ERR_SUCCESS)
            {
                onStateMessage(getErrorMsg(w_nRet));
                return false;
            }
            return true;
        });
        try {
            return result.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteAllFinger() {
        Future<Boolean> result= mExecutorService.submit(() -> {
            int		w_nRet;
            if (!mDevComm.IsInit())
                return false;

            w_nRet = mDevComm.Run_DelChar(1, mMaxFpCount);

            if (w_nRet != DevComm.ERR_SUCCESS)
            {
                onStateMessage(getErrorMsg(w_nRet));
                return false;
            }
            return true;
        });
        try {
            return result.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return  false;
    }

    @Override
    public boolean getFingerCode(int fingerId) {
        return false;
    }

    private String getErrorMsg(int nErrorCode) {
        String  str;
        switch(nErrorCode) {
            case DevComm.ERR_SUCCESS:
                str = "Succcess";
                break;
            case DevComm.ERR_VERIFY:
                str = "Verify NG";
                break;
            case DevComm.ERR_IDENTIFY:
                str = "Identify NG";
                break;
            case DevComm.ERR_EMPTY_ID_NOEXIST:
                str = "Empty Template no Exist";
                break;
            case DevComm.ERR_BROKEN_ID_NOEXIST:
                str = "Broken Template no Exist";
                break;
            case DevComm.ERR_TMPL_NOT_EMPTY:
                str = "Template of this ID Already Exist";
                break;
            case DevComm.ERR_TMPL_EMPTY:
                str = "This Template is Already Empty";
                break;
            case DevComm.ERR_INVALID_TMPL_NO:
                str = "Invalid Template No";
                break;
            case DevComm.ERR_ALL_TMPL_EMPTY:
                str = "All Templates are Empty";
                break;
            case DevComm.ERR_INVALID_TMPL_DATA:
                str = "Invalid Template Data";
                break;
            case DevComm.ERR_DUPLICATION_ID:
                str = "Duplicated ID : ";
                break;
            case DevComm.ERR_BAD_QUALITY:
                str = "Bad Quality Image";
                break;
            case DevComm.ERR_MERGE_FAIL:
                str = "Merge failed";
                break;
            case DevComm.ERR_NOT_AUTHORIZED:
                str = "Device not authorized.";
                break;
            case DevComm.ERR_MEMORY:
                str = "Memory Error ";
                break;
            case DevComm.ERR_INVALID_PARAM:
                str = "Invalid Parameter";
                break;
            case DevComm.ERR_GEN_COUNT:
                str = "Generation Count is invalid";
                break;
            case DevComm.ERR_INVALID_BUFFER_ID:
                str = "Ram Buffer ID is invalid.";
                break;
            case DevComm.ERR_INVALID_OPERATION_MODE:
                str = "Invalid Operation Mode!";
                break;
            case DevComm.ERR_FP_NOT_DETECTED:
                str = "Finger is not detected.";
                break;
            default:
                str = String.format("Fail, error code=%d", nErrorCode);
                break;
        }
        return str;
    }




    private final IUsbConnState mIConnectionHandler = new IUsbConnState() {
        @Override
        public void onUsbConnected() {
            String[] wStrInfo = new String[1];

            if (mDevComm.Run_TestConnection() == DevComm.ERR_SUCCESS)
            {
                if (mDevComm.Run_GetDeviceInfo(wStrInfo) == DevComm.ERR_SUCCESS)
                {
                   //USB连接成功
                }
            }
            else ;//todo 连接失败
        }

        @Override
        public void onUsbPermissionDenied() {
            //todo 无USB访问权限
        }

        @Override
        public void onDeviceNotFound() {
           //TODO 设备未找到
        }
    };
}
