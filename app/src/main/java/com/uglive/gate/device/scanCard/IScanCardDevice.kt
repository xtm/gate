package com.uglive.gate.device.scanCard

import android.content.Context
import com.decard.entitys.IDCard

interface IScanCardDevice {

    /**
     * 打开扫卡设备
     * @onScanM1 是否需要扫描芯片
     * @needScanIDCard 是否需要扫描身份证
     * @onScanM1 扫描到芯片时回调
     * @onScanIDCard 扫描到身份证时回调 Int -> 处理信息状态
     *                                 0开始处理信息，1处理完成
     *                              IDCard -> 身份证信息
     * @return 是否打开成功
     */
    fun open(context: Context, needScanM1: M1ScanType, needScanIDCard: Boolean, onScanM1: (String) -> Unit, onScanIDCard: (IDCard) -> Unit): Boolean

    /**
     * 关闭扫卡设备
     */
    fun close(): Boolean
}

public enum class M1ScanType{
    USE_NOT, //不使用
    USE_SECRET,//使用加密内容
    USE_ID//使用芯片ID
}