package com.uglive.gate.device.scanCard

import android.content.Context
import android.util.Log
import com.blankj.utilcode.util.DeviceUtils
import com.blankj.utilcode.util.FileIOUtils
import com.decard.NDKMethod.BasicOper
import com.decard.entitys.IDCard
import com.uglive.gate.Config
import com.uglive.gate.model.M1CardModel
import com.uglive.gate.utils.ByteUtil
import com.uglive.gate.utils.ByteUtil.convertStringToHex
import com.uglive.gate.utils.MDSEUtils
import com.xs.administrator.lib_auth_debug_byyj.BYYJScanCard
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock

/**
 * 平板上的扫卡设备
 */

class TabletIScanCardDevice : IScanCardDevice {
    val TAG = "TabletIScanCardDevice"
    val lock = ReentrantLock()
    var executor: ScheduledExecutorService? = null
    var byyjScanCard = BYYJScanCard()
    /**
     * 打开扫卡设备,由于设备已经ROOT,使用LUSB连接
     * @return 是否打开成功
     */
    override fun open(context: Context, needScanM1: M1ScanType, needScanIDCard: Boolean,
                      onScanM1: (String) -> Unit, onScanIDCard: (IDCard) -> Unit) =
            if (DeviceUtils.getSDKVersionCode() < 20) oldDeviceScanCard(context, needScanM1, needScanIDCard, onScanM1, onScanIDCard)
            else newDeviceScanCard(context, needScanM1, needScanIDCard, onScanM1, onScanIDCard)

    //老开发板
    private fun oldDeviceScanCard(context: Context, needScanM1: M1ScanType, needScanIDCard: Boolean,
                                  onScanM1: (String) -> Unit, onScanIDCard: (IDCard) -> Unit): Boolean {
        val result = BasicOper.dc_open("LUSB", context, "", 0) > 0
        if (result) {
            val m1CardModel = M1CardModel()
            executor = Executors.newSingleThreadScheduledExecutor()
            executor?.scheduleWithFixedDelay({
                //由于设备只能有一个线程占用，所以要加锁
                lock.lock()
                try {
                    if (needScanM1 == M1ScanType.USE_SECRET) {
                        if (Config.encByFactory) {
                            val rfid = m1CardModel.bt_read_ticket(0, Config.constantSecret)
                            if (MDSEUtils.isSucceed(rfid)) {
                                var ticket = MDSEUtils.returnResult(rfid)
                                onScanM1(ticket.substring(0,8))
                                BasicOper.dc_beep(5)
                            }
                            BasicOper.dc_halt()
                        } else {
                            Config.secretKey?.let {
                                //读取每场活动的秘钥
                                val rfid = m1CardModel.bt_read_ticket(4, ByteUtil.convertStringToHex(it))
                                var ticket = MDSEUtils.returnResult(rfid)
                                val success = MDSEUtils.isSucceed(rfid)
                                if (success) {
                                    val decode = ByteUtil.decode(ticket)
                                    ticket = decode.substring(0, 12)
                                    onScanM1(ticket)
                                    BasicOper.dc_beep(5)
                                }else{
                                    //读取不变的公安秘钥
                                    val rfid1 = m1CardModel.bt_read_ticket(4, ByteUtil.convertStringToHex(Config.yrypConstantSecret))
                                    var ticket1 = MDSEUtils.returnResult(rfid1)
                                    val success1 = MDSEUtils.isSucceed(rfid1)
                                    if (success1) {
                                        val decode1 = ByteUtil.decode(ticket1)
                                        ticket1 = decode1.substring(0, 12)
                                        onScanM1(ticket1)
                                        BasicOper.dc_beep(5)
                                    }else{;}
                                }
                                BasicOper.dc_halt()
                            }
                        }
                    }
                    if (needScanM1 == M1ScanType.USE_ID) {
                        val rfid = BasicOper.dc_card_hex(0)
                        if (MDSEUtils.isSucceed(rfid)) {
                            var ticket = MDSEUtils.returnResult(rfid)
                            ticket = ticket.substring(6..7) +
                                    ticket.substring(4..5) +
                                    ticket.substring(2..3) +
                                    ticket.substring(0..1)
                            onScanM1(ticket)
                            BasicOper.dc_halt()
                            BasicOper.dc_beep(5)
                        }
                    }
                    if (needScanIDCard) {
                        //todo 扫描身份证速度太慢->耗时1.5S
                        BasicOper.dc_get_i_d_raw_info()?.let {
                            onScanIDCard(it)
                            BasicOper.dc_beep(5)
                        }

                    }
                } finally {
                    lock.unlock()
                }
            }, 1000, 200, TimeUnit.MILLISECONDS)
        }
        return result
    }

    //新开发板
    private fun newDeviceScanCard(context: Context, needScanM1: M1ScanType, needScanIDCard: Boolean, onScanM1: (String) -> Unit, onScanIDCard: (IDCard) -> Unit): Boolean {
        if (!byyjScanCard.open(context, "/dev/ttyS1")) return false
        else {
            executor = Executors.newSingleThreadScheduledExecutor()
            executor?.scheduleWithFixedDelay({
                lock.lock()
                try {
                    if (needScanM1 != M1ScanType.USE_NOT) {
                        if (needScanM1 == M1ScanType.USE_SECRET) {
                            if (Config.encByFactory) {
                                byyjScanCard.readBlock0(Config.constantSecret)?.let {
                                    onScanM1(ByteUtil.bytesToHexString(it).substring(0,8).toUpperCase())
                                }
                            } else {
                                Config.secretKey?.let {
                                    //读取每场活动的秘钥
                                    byyjScanCard.readBlock16(it)?.let {
                                        val data = ByteUtil.bytesToHexString(it)
                                        val decode = ByteUtil.decode(data)
                                        val ticket = decode.substring(0, 12)
                                        onScanM1(ticket)
                                    } ?:
                                    //读取不变的公安秘钥
                                    byyjScanCard.readBlock16(Config.yrypConstantSecret)?.let {
                                        val data = ByteUtil.bytesToHexString(it)
                                        val decode = ByteUtil.decode(data)
                                        val ticket = decode.substring(0, 12)
                                        onScanM1(ticket)
                                    }
                                }
                            }
                        }
                        if (needScanM1 == M1ScanType.USE_ID) {
                            byyjScanCard.readCardId()?.let(onScanM1)
                        }
                    }
                    if (needScanIDCard) {
                        byyjScanCard.readIdCard(context.filesDir.absolutePath)?.let {
                            val idCard = IDCard()
                            idCard.address = it.address
                            idCard.birthday = it.birthday
                            idCard.id = it.id
                            idCard.name = it.name
                            idCard.endTime = it.validateDate
                            idCard.sex = it.sex
                            idCard.office = it.office
                            idCard.nation = it.nation
                            idCard.photoData = FileIOUtils.readFile2BytesByStream(it.photoPath)
                            onScanIDCard(idCard)
                        }
                    }
                } finally {
                    lock.unlock()
                }
            }, 1000, 200, TimeUnit.MILLISECONDS)
        }
        return true
    }


    fun openTest(context: Context) = BasicOper.dc_open("LUSB", context, "", 0) > 0

    fun readCard(): String? {
        val m1CardModel = M1CardModel()
        val rfid = m1CardModel.bt_read_ticket(4, convertStringToHex("111111"))
        if (MDSEUtils.isSucceed(rfid)) {
            var mTicket_NO = MDSEUtils.returnResult(rfid)
            mTicket_NO = ByteUtil.decode(mTicket_NO).substring(0, 12)
            BasicOper.dc_beep(10)
            return mTicket_NO
        } else {
            return null
        }
    }

    fun writCard(secret: String, text: String): String {
        val keyType = 0
        val spinnerPosition = 4
        val secret = ByteUtil.convertStringToHex(secret)
        val text = ByteUtil.convertStringToHex(text)
        var isSucceed = BasicOper.dc_card_hex(0)
        if (!MDSEUtils.isSucceed(isSucceed)) {
            Log.e("writCard", "寻卡失败")
            return isSucceed
        }
        Log.e("writCard", "寻卡成功")
        isSucceed = BasicOper.dc_load_key_hex(keyType, spinnerPosition, secret)
        if (!MDSEUtils.isSucceed(isSucceed)) {
            Log.e("writCard", "装载失败")
            return isSucceed
        }
        Log.e("writCard", "装载成功")
        isSucceed = BasicOper.dc_authentication(keyType, spinnerPosition)
        if (!MDSEUtils.isSucceed(isSucceed)) {
            Log.e("writCard", "验证失败")
            return isSucceed
        }
        Log.e("writCard", "验证成功")
        //for ( i = 0; i < 3; i++) {
        isSucceed = BasicOper.dc_read_hex(spinnerPosition * 4 + 0)
        isSucceed = BasicOper.dc_write_hex(spinnerPosition * 4 + 0, text)
        if (!MDSEUtils.isSucceed(isSucceed)) {
            Log.e("writCard", "写卡失败")
            return isSucceed
        }

        BasicOper.dc_halt()
        return isSucceed
    }


    /**
     * 关闭扫卡设备
     * @return 是否关闭成功
     */
    override fun close(): Boolean {
        executor?.let {
            if (!it.isShutdown) {
                it.shutdownNow()
            }
        }
        return BasicOper.dc_exit() == 0
    }
}