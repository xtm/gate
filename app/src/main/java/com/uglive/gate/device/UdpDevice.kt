package com.uglive.gate.device

import android.util.Log
import com.blankj.utilcode.util.NetworkUtils
import com.uglive.gate.repositroy.TicketRepository
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import kotlin.concurrent.thread

/**
 * 用于同步数据，必须保证在一个局域网内
 */
class UdpDevice(val ticketRepository: TicketRepository) {
    val port = 36666
    val MAX_DATA_SIZE = 1024
    var isRunning = true
    lateinit var serverSocket: DatagramSocket

    init {
        //接收同步信息
        thread {
            try {
                serverSocket = DatagramSocket(port)
                while (isRunning) {
                    val body = DatagramPacket(ByteArray(MAX_DATA_SIZE + 1024), MAX_DATA_SIZE + 1024)
                    serverSocket.receive(body)
                    parseData(body.data)
                }
                if(!serverSocket.isClosed){
                    serverSocket.close()
                }
            } catch (e: Exception) {
                e.printStackTrace(System.err)
            }
        }
    }

    fun close() {
        isRunning = false
        try {
            serverSocket.close()
        }catch (e: Exception) {
            e.printStackTrace(System.err)
        }

    }

    private fun parseData(data: ByteArray) {
        var tail = 0
        var flag = '$'.toByte()
        for (i in data.indices) {
            if (data[i] == flag) {
                tail = i
                break
            }
        }
        try {
            val msg = String(data, 0, tail).split("&")
            val id = msg[0]
            val m1 = msg[1]
            if (!id.isNullOrBlank()) {
                Log.i("UDPDevice", "receive id is $id")
                val ticketBean = ticketRepository.searchCacheTicketBysoldId(id)
                ticketBean?.soldUsed = 1
                ticketBean?.let { ticketRepository.updateItem(it) }
            }
            if (!m1.isNullOrBlank()) {
                Log.i("UDPDevice", "receive m1 is $m1")
                ticketRepository.insertLocalTicket(m1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun syncTicket(id: String = "", m1: String = "") {
        val data = "$id&$m1$".toByteArray()
        thread {
            getFatherIp()?.let {
                with(DatagramSocket()) {
                    // 注意用with后若有变量和外部变量相同时有坑！
                    send(DatagramPacket(
                            data,
                            data.size,
                            InetAddress.getByName(it),
                            this@UdpDevice.port)
                    )
                }
            }
        }
    }


    private fun getFatherIp(): String? {
        val ip = NetworkUtils.getIPAddress(true)
        if (ip.isNullOrBlank()) return null
        val sp = ip.split(".").toMutableList()
        sp[sp.lastIndex] = "0"
        return sp.joinToString(".")
    }
}