package com.uglive.gate.device.bodyScan

import kotlin.concurrent.thread

class BodyScan(onScan : ()->Unit) {

    private val worker = thread (name = "bodyScanThread"){
        readLine()
    }

}