package com.uglive.gate.device.scanQrcode

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.device.ScanDevice
import com.blankj.utilcode.util.Utils
import java.util.concurrent.Executors

/**
 * 手持二位码扫描设备
 */
class PhoneScanQrcodeDevice : IScanQrDevice {

    private val sm = ScanDevice()

    private var callback: ((String) -> Unit)? = null

    private var mScanReceiver: BroadcastReceiver? = null
    private val singleTask = Executors.newSingleThreadExecutor()
    init {
        if (!sm.isScanOpened) {
            sm.outScanMode = 0
            sm.openScan()
        }
        mScanReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val barocode = intent.getByteArrayExtra("barocode")
                val barocodelen = intent.getIntExtra("length", 0)
                val barcodeStr = String(barocode, 0, barocodelen)
                //切换线程
                callback?.let { singleTask.execute { it(barcodeStr) } }
                sm.stopScan()
            }

        }
        Utils.getApp().registerReceiver(mScanReceiver, IntentFilter("scan.rcv.message"))
    }

    override fun setDataCallBack(callback: (String) -> Unit) {
        this.callback = callback
    }

    override fun close() {
        Utils.getApp().unregisterReceiver(mScanReceiver)
        sm.closeScan()
        singleTask.shutdownNow()
    }

}