package com.uglive.gate.device

import android.util.Log
import com.cmm.rkgpiocontrol.rkGpioControlNative
import com.github.kittinunf.fuel.core.requests.write
import com.uglive.gate.AppExecutors
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter
import java.util.concurrent.TimeUnit

/**
 * 开关门设备
 */
class GateControllerDevice(val isFireFly:Boolean,val isNewBoard: Boolean) {
    val syncGate = Object()
    @Volatile
    var canOpenGate = true

    val devicepath = "/dev/gpiogate"

    //Android6.0板子的gpio控制
    val gpio2 = "/sys/class/gpio/gpio2"
    val mGPIODir = gpio2 + File.separator + "direction"
    val mGPIOValue = gpio2 +  File.separator + "value"

    internal var device = File(devicepath)

    private  fun setGpio(path: String, value: String) {
        try {
            val writer = FileWriter(path)
            writer.write(value)
            writer.close()
        } catch (e: Exception) {
           Log.e("GateControllerDevice","set gpio error...")
        }

    }

    private fun openNewBoardGate(){
        setGpio(mGPIOValue,"0")
    }

    private fun closeNewBoardGate(){
        setGpio(mGPIOValue,"1")
    }

    fun openGateFireFly() {
        try {
            val fos = FileOutputStream(device)
            fos.write("1")
            fos.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun closeGateFireFly() {
        try {
            val fos = FileOutputStream(device)
            fos.write("0")
            fos.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    init {
        if (!isFireFly&&!isNewBoard)
            rkGpioControlNative.init()
        if (isNewBoard)
            setGpio(mGPIODir,"out")
        //软件打开后复位闸门，不用刷卡触发
        openGate()
    }

    fun close() {
        if (!isFireFly&&!isNewBoard)
            rkGpioControlNative.close()
        //todo 这里关闭的是什么，需不需要关闭？
//        adcNative.close(0)
//        adcNative.close(2)
    }

    fun openGate() {
        AppExecutors.diskIO.submit {
            //将警告灯变为绿色
            AppExecutors.diskIO.execute { rkGpioControlNative.ControlGpio(20, 1) }
            synchronized(syncGate) {
                if (canOpenGate) {
                    if (!isFireFly && !isNewBoard)
                        rkGpioControlNative.ControlGpio(1, 0)
                    else if (isFireFly)
                        openGateFireFly()
                    else
                        openNewBoardGate()
                    canOpenGate = false
                } else {

                }
            }
            //500ms关门
            AppExecutors.diskIO.schedule({
                synchronized(syncGate) {
                    if (!isFireFly && !isNewBoard)
                        rkGpioControlNative.ControlGpio(1, 1)
                    else if (isFireFly)
                        closeGateFireFly()
                    else
                        closeNewBoardGate()
                }
            }, 500, TimeUnit.MILLISECONDS)
            //2S可以开一次门
            AppExecutors.diskIO.schedule({
                synchronized(syncGate) {
                    if (!canOpenGate) {
                        canOpenGate = true
                    }
                }
            }, 2000, TimeUnit.MILLISECONDS)
        }

    }

    fun warningLight() {
        AppExecutors.diskIO.execute { rkGpioControlNative.ControlGpio(20, 0) }
        AppExecutors.diskIO.schedule({ rkGpioControlNative.ControlGpio(20, 1) }, 1000, TimeUnit.MILLISECONDS)
    }


}