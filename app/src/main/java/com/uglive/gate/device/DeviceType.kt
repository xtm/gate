package com.uglive.gate.device

enum class DeviceType{
    TABLET, // 平板
    PHONE, // 手机
    TABLET_KEYBOARD // 键盘（二维码扫描）
}