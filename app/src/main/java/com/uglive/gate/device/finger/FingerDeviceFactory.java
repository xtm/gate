package com.uglive.gate.device.finger;

public class FingerDeviceFactory {

    public static FingerDeviceController geFingerDevice(StateMessage stateMessage){
        return new NOEMHostFingerDeviceControl() {
            @Override
            public void onStateMessage(String message) {
                stateMessage.onStateMessage(message);
            }
        };
    }

    public interface StateMessage{
        void onStateMessage(String message);
    }
}
