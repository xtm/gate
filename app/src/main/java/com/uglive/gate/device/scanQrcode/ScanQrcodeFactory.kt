package com.uglive.gate.device.scanQrcode

import android.view.View
import com.uglive.gate.device.DeviceType

object ScanQrcodeFactory {


    /**
     * @param type "tablet闸机,phone手持机,键盘输入式二维码扫描仪"
     */
    fun create(type: DeviceType, view: View): IScanQrDevice = when(type){
            DeviceType.TABLET -> TabletScanQrDevice()
            DeviceType.TABLET_KEYBOARD -> KeyboardQrcodeDevice(view)
            DeviceType.PHONE -> PhoneScanQrcodeDevice()
    }
}