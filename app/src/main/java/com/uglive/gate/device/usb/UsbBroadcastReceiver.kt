package com.uglive.gate.device.usb

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import com.blankj.utilcode.util.DeviceUtils
import com.uglive.gate.Config
import java.io.File


class UsbBroadcastReceiver : BroadcastReceiver() {

    private val TAG = "UsbBroadcastReceiver"

    private val addFaceDirName = "/addFace"

    private val replaceFaceDirName = "/replaceFace"

    private val appDirName = "/app"

    private val api19UsbPath = "/mnt/usb_storage"
    //todo pai23USBPath
    private val api23UsbPath = ""

    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent!!.action){
            Intent.ACTION_MEDIA_MOUNTED -> {
                Log.i(TAG,"usb media mounted")
                val isApi19 = DeviceUtils.getSDKVersionCode() == 19
                val appPath = if (isApi19) "$api19UsbPath$appDirName" else "$api23UsbPath$appDirName"
                val addFacePath = if (isApi19) "$api19UsbPath$addFaceDirName" else "$api23UsbPath$addFaceDirName"
                val replaceFacePath = if (isApi19) "$api19UsbPath$replaceFaceDirName" else "$api23UsbPath$replaceFaceDirName"

                //若识别到app文件夹且有
                val appDir = File(appPath)
                if (appDir.exists()){
                    val apkFiles = appDir.listFiles().filter { Regex("""^.+\.apk$""").matches(it.name) }
                    if (apkFiles.isNotEmpty()) {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.setDataAndType(Uri.fromFile(apkFiles.first()), "application/vnd.android.package-archive")
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        context!!.startActivity(intent)
                        return
                    }
                }
                if (!Config.faceSourceDir.exists()){
                    Config.faceSourceDir.mkdirs()
                }

                //人脸替换通过人脸替换目录
                val replaceFaceDir = File(replaceFacePath)
                if ( replaceFaceDir.exists() && replaceFaceDir.list().isNotEmpty() ){
                    Config.faceSourceDir.listFiles().forEach { it.delete() }
                    replaceFaceDir.listFiles().forEach {
                        it.copyTo(File(Config.faceSourceDir,it.name))
                    }
                    restartApp(context!!)
                    return
                }

                //人脸添加通过人脸添加目录
                val addFaceDir = File(addFacePath)
                if (addFaceDir.exists() && addFaceDir.list().isNotEmpty() ){
                    addFaceDir.listFiles().forEach {
                        it.copyTo(File(Config.faceSourceDir,it.name),true)
                    }
                    restartApp(context!!)
                    return
                }

            }

            Intent.ACTION_MEDIA_UNMOUNTED->{
                Log.i(TAG,"usb media unmounted")
            }
        }
    }

    private fun restartApp(context: Context){
        //2S后重启程序,因为需要重新配置日志发送模块
        val intent = context.packageManager.getLaunchIntentForPackage(context.applicationContext.packageName)
        val restartIntent = PendingIntent.getActivity(context.applicationContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, restartIntent)
        System.exit(0)
    }

}