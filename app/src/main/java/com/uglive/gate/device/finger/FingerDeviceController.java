package com.uglive.gate.device.finger;

import android.app.Activity;
import android.graphics.Bitmap;

/**
 * create by xiantm
 * 指纹信息管理
 */
public interface FingerDeviceController {
    /**
     * open device
     * @return isSuccess
     */
    boolean open(Activity activity);

    /**
     * close device
     */
    void  close();

    /**
     * 结束正在进行的指纹注册、对比等任务
     */
    void cancelTask();

    /**
     * enroll finger
     * @return -1：设备未打开， -2：ID已经用完， 其他Finger ID
     */
    int enrollFinger(Action action);

    /***
     * validate finger
     * @param id  要验证的ID
     * @return isPass
     */
    boolean validateFinger(int id);

    /**
     * 输入指纹，找出指纹的ID号
     * @return finger ID (-1未找到)
     */
    int identify();

    /**
     * 获取已经注册指纹数量
     * @return enrollCount
     */
    int getEnrollCount();

    /**
     * 按照ID小到大获取未注册指纹的ID
     * @return 第一个未注册指纹的ID
     */
    int getEmptyID();

    /**
     * 捕获指纹图像
     * @return 指纹位图
     */
    Bitmap captureImage();

    /**
     * 删除指纹，若指定ID存在指纹返回true，否则返回false
     * @param fingerId 要删除的指纹ID
     * @return
     */
    boolean deleteFinger(int fingerId);

    /**
     * 删除所有指纹
     */
    boolean deleteAllFinger();

    /**
     * 获取指纹特征码
     * @param fingerId
     * @return
     */
    boolean getFingerCode(int fingerId);

    interface Action{
        void onAction(String message,Object data);
    }
}
