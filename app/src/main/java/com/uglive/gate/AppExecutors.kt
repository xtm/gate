package com.uglive.gate

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * 使用线程池执行异步任务
 */
object AppExecutors {
    val diskIO = Executors.newScheduledThreadPool(2)
    val mainThread = MainThreadExecutor()
    val networkIO: Executor = Executors.newFixedThreadPool(4)

    class MainThreadExecutor : Executor {
        private val mainThreadHandler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable) {
            mainThreadHandler.post(command)
        }
        fun schedule(command: ()->Unit,delay:Long){
            mainThreadHandler.postDelayed(command,delay)
        }
    }
}