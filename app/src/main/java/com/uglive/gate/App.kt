package com.uglive.gate

import android.app.Application
import android.content.Context
import android.content.Intent
import android.support.multidex.MultiDex
import com.blankj.utilcode.util.*
import com.uglive.gate.device.GateControllerDevice
import com.uglive.gate.model.ApiModel
import com.uglive.gate.model.ConfigModel
import com.uglive.gate.repositroy.TicketRepository
import com.uglive.gate.shareData.PhotoSocketService
import com.uglive.gate.shareData.UDPService
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

const val SP_APP_VERSION = "SP_APP_VERSION"

class App : Application() {


    private lateinit var mApiModel: ApiModel
    private lateinit var mTicketRepository: TicketRepository

    var gateControllerDevice: GateControllerDevice? = null

    var lastOnPreviewTimestamp = 0L

    fun getApiModel() = mApiModel
    fun getTicketRepository() = mTicketRepository

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        //小于SDK21的分包
        if (DeviceUtils.getSDKVersionCode() < 21) {
            MultiDex.install(this)
        }
//        val sp = getSharedPreferences("spUtils", Context.MODE_PRIVATE)
//        Config.deviceName = sp.getString(SP_DEVICE_NAME, DeviceUtils.getAndroidID())
        if (!Config.deviceName.isNullOrBlank()) {
//            //使用ACRA日志框架搜集崩溃日志
//            val builder = CoreConfigurationBuilder(this)
//            builder.setBuildConfigClass(BuildConfig::class.java)
//                    .setReportFormat(StringFormat.JSON)
//                    .setLogcatArguments("-t", "200", "com.uglive.gate:I")
//                    .setIncludeDropBoxSystemTags(true)
//                    //todo 这方法设置怎么生效
////                    .setReportSendSuccessToast("闸门程序错误信息发送成功")
////                    .setReportSendFailureToast("闸门程序错误信息发送失败,请检查网络连接")
//                    //自定义需要上传的字段
//                    .setReportContent(APP_VERSION_CODE, APP_VERSION_NAME, PACKAGE_NAME, ANDROID_VERSION, USER_CRASH_DATE, AVAILABLE_MEM_SIZE,
//                            REPORT_ID, BUILD, CRASH_CONFIGURATION, CUSTOM_DATA, BRAND, PHONE_MODEL, STACK_TRACE, LOGCAT, DROPBOX)
//                    .setReportSenderFactoryClasses(CrashReportSenderFactory::class.java)
//                    .setEnabled(true)
////            builder.getPluginConfigurationBuilder(ToastConfigurationBuilder::class.java)
////                    .setText("闸门程序出现异常，正在发送错误信息")
////                    .setEnabled(true)
////            builder.getPlugidnConfigurationBuilder(HttpSenderConfigurationBuilder::class.java)
////                    .setUri("http://${Config.IP}:8080/crash")
////                    .setHttpMethod(HttpSender.Method.POST)
////                    .setEnabled(true)
//            ACRA.init(this, builder)
//            //添加自定义字段
//            ACRA.getErrorReporter().putCustomData("device_name", Config.deviceName)
        }
    }

    override fun onCreate() {
        Utils.init(this)
//        startService(Intent(this, KeepAliveService::class.java))
        Config.IP = ConfigModel().ip
        setData()
        mTicketRepository = TicketRepository()
        mApiModel = ApiModel(ticketRepository = mTicketRepository)
        startService(Intent(this, PhotoSocketService::class.java))
        startService(Intent(this, UDPService::class.java))
        super.onCreate()
    }

    /**
     * 设置数据
     * 1.拷贝工作票
     * 2.删除广告图片，人脸图片
     */
    private fun setData(){
        //通过对比App库版本来确定是否拷贝数据库删除人脸图片，广告图片
        val preAppVersion = SPUtils.getInstance().getInt(SP_APP_VERSION)
        val currAppVersion = AppUtils.getAppVersionName().toInt()
        if (preAppVersion!=currAppVersion) {
            //拷贝数据库
            val dbPath = PathUtils.getInternalAppDbPath("worker")
            File(PathUtils.getInternalAppDbsPath()).apply {
                if (!exists())
                    mkdirs()
                else{
                    //删除原来的数据库
                    listFiles().iterator().forEach {
                        if (it.name.contains("worker")) it.delete()
                    }
                }
            }
            val outputStream = FileOutputStream(dbPath)
            val inputStream = this.assets.open("worker")
            try {
                inputStream.copyTo(outputStream, 1024)
                SPUtils.getInstance().put(SP_APP_VERSION,currAppVersion)
            } catch (e: IOException) {
                e.printStackTrace()
            } finally {
                inputStream.close()
                outputStream.close()
            }

        }
    }



}