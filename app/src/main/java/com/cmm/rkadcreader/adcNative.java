package com.cmm.rkadcreader;

/**
 * 该类是厂家提供的,作用未知。
 */
public class adcNative {

	static {
        System.loadLibrary("rkAdcReader_jni");
    }
	
	public static native int open(int which);
	public static native int readAdc(int which);
	public static native void close(int which);
	
}
