package com.cmm.rkgpiocontrol;
/**
 * 该类是厂家提供的,用于控制开关门的
 */
public class rkGpioControlNative {

	static {
        System.loadLibrary("rkGpioControl_jni");
    }
	
	public static native int init();
	public static native int ControlGpio(int which,int state); 
	public static native int ReadGpio(int which);
	public static native void close();
	
}
