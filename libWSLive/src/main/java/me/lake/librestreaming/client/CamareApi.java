package me.lake.librestreaming.client;

public interface CamareApi {

    void setCameraListener(CamareListener cameraListener);

    void setExposureCompensation(int value);

    void setWhiteBalance(String value);

}
