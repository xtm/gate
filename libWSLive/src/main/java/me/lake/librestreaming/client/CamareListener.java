package me.lake.librestreaming.client;

import android.hardware.Camera;

public interface CamareListener {
    void onPreviewFrame(byte[] data, Camera camera);
    void onCameraOpen(Camera camera,int displayOrientation,boolean isMirror,int cameraId);
}
