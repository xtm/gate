package com.yuwei;

import android.app.Application;
import android.content.Context;

/**
 * Created by Administrator on 2016/12/27.
 */

public class App extends Application {
    public static Context getContent() {
        return c;
    }

    private static  Context c;
    @Override
    public void onCreate() {
        super.onCreate();
        c = this;
        System.out.println("App.onCreate");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        System.out.println("App.onTerminate");
    }

}
