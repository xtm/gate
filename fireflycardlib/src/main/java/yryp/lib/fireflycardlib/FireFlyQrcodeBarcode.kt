package yryp.lib.fireflycardlib

import android.view.KeyEvent
import android.view.View
import java.lang.StringBuilder

class FireFlyQrcodeBarcode(val view: View) {

    private var lastKeyCode = -1
    private val chache = arrayListOf<Int>()

    fun setOnQrcodeBarcodeScan(onScan: (String)->Unit){
        view.setOnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN) {
                if (lastKeyCode == KeyEvent.KEYCODE_ENTER && keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    onScan(parseData(dataList = chache))
                    chache.clear()
                } else {
                    chache.add(keyCode)
                }
                lastKeyCode = keyCode
            }

            return@setOnKeyListener false
        }
    }

    private fun parseData(dataList: List<Int>): String {
        val sb = StringBuilder()
        var shift = false //大小写标记
        dataList.forEach {
            if (it == KeyEvent.KEYCODE_SHIFT_LEFT) {
                shift = true //开启大写标记
            } else {
                var ch: Char? = null
                when (it) {
                    in KeyEvent.KEYCODE_0..KeyEvent.KEYCODE_9 -> {
                        if (shift) {
                            when (it) {
                                KeyEvent.KEYCODE_1 -> ch = '!'
                                KeyEvent.KEYCODE_2 -> ch = '@'
                                KeyEvent.KEYCODE_3 -> ch = '#'
                                KeyEvent.KEYCODE_4 -> ch = '$'
                                KeyEvent.KEYCODE_5 -> ch = '%'
                                KeyEvent.KEYCODE_6 -> ch = '^'
                                KeyEvent.KEYCODE_7 -> ch = '&'
                                KeyEvent.KEYCODE_8 -> ch = '*'
                                KeyEvent.KEYCODE_9 -> ch = '('
                                KeyEvent.KEYCODE_0 -> ch = ')'
                            }
                        } else ch = (it - KeyEvent.KEYCODE_0 + '0'.toInt()).toChar()
                    }
                    in KeyEvent.KEYCODE_A..KeyEvent.KEYCODE_Z -> {
                        ch = if (shift)
                            (it - KeyEvent.KEYCODE_A + 'A'.toInt()).toChar()
                        else
                            (it - KeyEvent.KEYCODE_A + 'a'.toInt()).toChar()
                    }
                    KeyEvent.KEYCODE_EQUALS -> ch = if (shift) '+' else '='
                    KeyEvent.KEYCODE_PLUS -> ch = '+'
                    KeyEvent.KEYCODE_MINUS ->  ch = if (shift) '_' else '-'
                    KeyEvent.KEYCODE_NUMPAD_MULTIPLY -> ch = '*'
                    KeyEvent.KEYCODE_NUMPAD_DIVIDE -> ch = if (shift) '?' else '/'
                    KeyEvent.KEYCODE_GRAVE -> ch = if (shift) '~' else '`'
                    KeyEvent.KEYCODE_BACKSLASH -> ch = if (shift) '|' else '\\'
                    KeyEvent.KEYCODE_COMMA -> ch = if (shift) '<' else ','
                    KeyEvent.KEYCODE_PERIOD -> ch = if (shift) '>' else '.'
                    KeyEvent.KEYCODE_SEMICOLON -> ch = if (shift) ':' else ';'
                    KeyEvent.KEYCODE_APOSTROPHE -> ch = if (shift) '"' else '\''
                }
                ch?.let { sb.append(it) }
                shift = false //关闭大写标记
            }
        }
        return sb.toString()
    }

    fun close(){
        view.setOnKeyListener(null)
    }
}