package yryp.lib.fireflycardlib

import android.content.Context
import android.support.annotation.WorkerThread
import android.util.Log
import com.bit.rfid.RFIDReader
import com.bit.rfid.ReaderExtra
import com.bit.rfid.SubExceptions.COMException
import com.bit.rfid.Ulitily
import com.ivsign.android.IDCReader.IDCReaderSDK
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream


object FireFlyScanCard {
    private val tag = "FireFlyScanCard"
    //读卡器连接方式
    private val CONNE_STYLE_ChouKou = "uart"
    private val CONNE_STYLE_BlueTooth = "bluetooth"
    private val CONNE_STYLE_USB = "ch9326"
    //IC卡类型
    private val CARD_TYPE_M1 = "mifare_s50"
    private val KEY_TYPE = RFIDReader.PICC_KEY_A

    val SECRET_TYPE_ASCII = 0
    val SECRET_TYPE_HEX = 1


    private val connStyle = CONNE_STYLE_ChouKou

    private var readerManager: RFIDReader? = null
    private var interrupted = false

    @Synchronized
    fun open(context: Context, comName: String): Boolean {

        readerManager = RFIDReader.getInstance(context, connStyle)
        if (connStyle == CONNE_STYLE_ChouKou) {
            RFIDReader.setUartDeviceName(comName)
        }
        try {
            // 通过像设备读写文件测试连接是否成功
            val type = ByteArrayInputStream(connStyle.toByteArray())
            readerManager?.control(ReaderExtra.CONNECT, type)
        } catch (exp: COMException) {
            return false
        }
        return true
    }

    /** MifareS50卡
     * 包括：寻卡，认证，读
     * 测试时需把MifareS50放在读写器上，并确认卡初始密钥为"ffffffffffff"
     * */
    @WorkerThread
    fun readCardId(): String? {
        try {
            readerManager?.open("mifare_s50")
            return Ulitily.byteArrayToHexString(readerManager?.uid)
        } catch (exc: Exception) {
            //寻卡失败
            exc.printStackTrace()
        }
        return null
    }

    //读厂家加密的0扇区
    @WorkerThread
    fun readBlock0(secret: String): ByteArray? {
        return readBlock(secret, 0, SECRET_TYPE_HEX)
    }

    //读自己加密的4扇区
    @WorkerThread
    fun readBlock16(secret: String): ByteArray? {
        return readBlock(secret, 4, SECRET_TYPE_ASCII)
    }

    /**
     * 通过指定密码读取扇区第一个block的内容
     * @param secret 密码
     * @param selector 扇区
     * @param secretType 密码类型 SECRET_TYPE_ASCII SECRET_TYPE_HEX
     */
    @WorkerThread
    fun readBlock(secret: String, selector: Int, secretType: Int): ByteArray? {
        var secretByteArray = try {
            if (secretType == SECRET_TYPE_ASCII) {
                convertAsciiStringToHexByte(secret)
            } else {
                convertHexStringToHexByte(secret)
            }
        } catch (ex: Exception) {
            throw ex
        }
        try {
            readerManager?.open(CARD_TYPE_M1)
            readerManager?.authBlock(selector * 4, secretByteArray, KEY_TYPE)
            return readerManager?.readBlock(selector * 4)
        } catch (exc: Exception) {
            //寻卡失败、验证失败
//            exc.printStackTrace()
            Log.e("FireFlyScanCard","寻卡或验证失败")
        }
        return null
    }

    /**
     * 将长度为6的ascii字符串密码传为6个十六进制字节数组
     */
    private fun convertAsciiStringToHexByte(str: String): ByteArray {
        if (str.length != 6) throw Exception("ascii密码字符串长度必须为6")
        val chars = str.toCharArray()
        val res = ByteArray(6)
        for (i in chars.indices) {
            val hex = Integer.toHexString(chars[i].toInt())
            if (hex.length > 1) {
                res[i] = (Character.digit(hex[0].toInt(), 16).shl(4) + Character.digit(hex[1].toInt(), 16)).toByte()
            } else {
                throw Exception("ascii密码字符串长度必须为数字或字母")
            }
        }
        return res
    }

    /**
     * 将长度为12的hex字符串密码传为6个十六进制字节数组
     */
    private fun convertHexStringToHexByte(str: String): ByteArray {
        if (str.length != 12) throw Exception("hex密码字符串长度必须为6")
        val chars = str.toCharArray()
        val res = ByteArray(6)
        for (i in chars.indices step 2) {
            val tens = Character.digit(chars[i], 16)
            val units = Character.digit(chars[i + 1], 16)
            if (tens > -1 && units > -1) {
                res[i / 2] = (tens.shl(4) + units).toByte()
            } else {
                throw Exception("hex字符串只能包含'0'..'9'||'a'..'f'||'A'..'F'")
            }
        }
        return res
    }

    /**
     * 修改秘钥
     * @param secret 秘钥
     * @param selector 扇区
     */
    fun changeSelectorKey(secret: String, selector: Int, secretType: Int) {
        var secretByteArray = try {
            if (secretType == SECRET_TYPE_ASCII) {
                convertAsciiStringToHexByte(secret)
            } else {
                convertHexStringToHexByte(secret)
            }
        } catch (ex: Exception) {
            throw ex
        }
        val block = selector * 4
        try {
            readerManager?.open(CARD_TYPE_M1)
            readerManager?.authBlock(block, secretByteArray, KEY_TYPE)
            readerManager?.changeKey(block, secretByteArray, KEY_TYPE)
        } catch (exc: Exception) {
            //寻卡失败、验证失败
            exc.printStackTrace()
        }
    }

    /**
     * 写入数据到block
     * @param block
     * @param secret 该扇区的秘钥
     * @param data 要写的数据
     */
    fun writeDataToBlock(block: Int, secret: String, data: String, secretType: Int) {
        var secretByteArray = try {
            if (secretType == SECRET_TYPE_ASCII) {
                convertAsciiStringToHexByte(secret)
            } else {
                convertHexStringToHexByte(secret)
            }
        } catch (ex: Exception) {
            throw ex
        }
        try {
            readerManager?.open(CARD_TYPE_M1)
            readerManager?.authBlock(block, secretByteArray, KEY_TYPE)
            val hexData = Ulitily.hexStringToByteArray(data)
            readerManager?.writeBlock(block, hexData)
        } catch (exc: Exception) {
            //寻卡失败、验证失败
            exc.printStackTrace()
        }
    }

    /**
     * 读身份证
     * @param photoPath 图片保存路径
     */
    @WorkerThread
    fun readIdCard(photoPath: String): IdCard? {
        try {
            readerManager?.open("id_sam")
            val idReadTextPhoto = ByteArrayOutputStream()
            readerManager?.control(ReaderExtra.ID_READ_TEXT_PHOTO, idReadTextPhoto)
            val textPhoto = Ulitily.byteArrayToHexString(idReadTextPhoto.toByteArray())
            if (textPhoto != null) {
                Log.i(tag, "身份证个人信息获取成功")
                val decodeInfo = arrayOfNulls<String>(10)
                val imagePath = StringBuilder()
                IDCReaderSDK.initialize(photoPath)
                IDCReaderSDK.decodeSamAck(textPhoto, decodeInfo, imagePath)
                val idCard = IdCard()
                idCard.name = decodeInfo[0]
                idCard.sex = decodeInfo[1]
                idCard.nation = decodeInfo[2]
                idCard.birthday = decodeInfo[3]
                idCard.address = decodeInfo[4]
                idCard.id = decodeInfo[5]
                idCard.office = decodeInfo[6]
                idCard.validateDate = decodeInfo[7]
                idCard.photoPath = imagePath.toString()
                return idCard
            }
        } catch (exc: Exception) {
            //寻卡失败
            exc.printStackTrace()
        }
        return null
    }

    fun close() {

        //释放资源
        try {
            readerManager?.close()
            Log.e(tag, "-------->释放资源1")
        } catch (e1: Exception) {
            Log.e(tag, "disconnect Exception3" + e1.message)
        }

        try {
            readerManager?.control(ReaderExtra.DISCONNECT)
            Log.e(tag, "-------->释放资源2")
        } catch (e: Exception) {
            Log.e(tag, "disconnect Exception3" + e.message)
        }

    }
}