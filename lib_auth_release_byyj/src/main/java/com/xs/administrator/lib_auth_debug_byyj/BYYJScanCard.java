package com.xs.administrator.lib_auth_debug_byyj;

import android.content.Context;
import android.support.annotation.WorkerThread;
import android.util.Log;

import com.bit.rfid.RFIDReader;
import com.bit.rfid.ReaderExtra;
import com.bit.rfid.Ulitily;
import com.ivsign.android.IDCReader.IDCReaderSDK;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.InvalidParameterException;

public class BYYJScanCard {

    private String tag = "BYYJScanCard";
    //读卡器连接方式
    private String CONNE_STYLE_ChouKou = "uart";
    //IC卡类型
    private String CARD_TYPE_M1 = "mifare_s50";
    private int KEY_TYPE = RFIDReader.PICC_KEY_A;

    private int SECRET_TYPE_ASCII = 0;
    private int SECRET_TYPE_HEX = 1;


    private String connStyle = CONNE_STYLE_ChouKou;

    private RFIDReader readerManager;
    private boolean interrupted = false;


    public synchronized boolean  open(Context context, String comName) {
        setBmpDecodeEnv(context);
        try {
            readerManager = RFIDReader.getInstance(context, connStyle);
        } catch (Exception e) {
            Log.e(tag,"RFIDReader打开失败",e);
            return false;
        }
        RFIDReader.setUartDeviceName(comName);
        try {
            // 通过像设备读写文件测试连接是否成功
            ByteArrayInputStream type = new ByteArrayInputStream(connStyle.getBytes());
            readerManager.control(ReaderExtra.CONNECT, type);
        } catch (Exception e) {
            Log.e(tag,"无打开"+comName+"的权限");
            return false;
        }
        return true;
    }

    /** MifareS50卡
     * 包括：寻卡，认证，读
     * 测试时需把MifareS50放在读写器上，并确认卡初始密钥为"ffffffffffff"
     * */
    @WorkerThread
    public String readCardId() {
        try {
            readerManager.open("mifare_s50");
            return Ulitily.byteArrayToHexString(readerManager.getUid());
        } catch ( Exception exc) {
            //寻卡失败
            exc.printStackTrace();
        }
        return null;
    }

    //读厂家加密的0扇区
    @WorkerThread
    public byte[] readBlock0(String secret){
        try {
            return readBlock(secret, 0, SECRET_TYPE_HEX);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //读自己加密的4扇区
    @WorkerThread
    public byte[] readBlock16(String secret) {
        try {
            return readBlock(secret, 4, SECRET_TYPE_ASCII);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过指定密码读取扇区第一个block的内容
     * @param secret 密码
     * @param selector 扇区
     * @param secretType 密码类型 SECRET_TYPE_ASCII SECRET_TYPE_HEX
     */
    @WorkerThread
    public byte[] readBlock( String secret,int selector , int secretType) throws Exception {
        byte[] secretByteArray = convertAsciiStringToHexByte(secret);
        if (secretType != SECRET_TYPE_ASCII)
            secretByteArray = convertHexStringToHexByte(secret);
        try {
            readerManager.open(CARD_TYPE_M1);
            readerManager.authBlock(selector * 4, secretByteArray, KEY_TYPE);
            return readerManager.readBlock(selector * 4);
        } catch (Exception exc) {
            //寻卡失败、验证失败
//            exc.printStackTrace()
            Log.i("BYYJScanCard","寻卡或验证失败");
        }
        return null;
    }

    /**
     * 将长度为6的ascii字符串密码传为6个十六进制字节数组
     */
    private byte[] convertAsciiStringToHexByte(String str) throws Exception {
        if (str.length() != 6) throw new InvalidParameterException("ascii密码字符串长度必须为6");
        byte[] chars = str.getBytes();
        byte[] res = new byte[6];

        for (int i =0;i<chars.length;i++) {
            String hex = Integer.toHexString((int)chars[i]);
            if (hex.length() > 1) {
                res[i] = (byte) (Character.digit((int)hex.charAt(0), 16)<<4 + Character.digit((int)hex.charAt(1), 16));
            } else {
                throw new Exception("ascii密码字符串长度必须为数字或字母");
            }
        }
        return res;
    }

    /**
     * 将长度为12的hex字符串密码传为6个十六进制字节数组
     */
    private byte[] convertHexStringToHexByte( String str) throws Exception {
        if (str.length() != 12) throw new Exception("hex密码字符串长度必须为6");
        char[] chars = str.toCharArray();
        byte[] res = new byte[6];
        for (int i =0;i<chars.length;i+=2) {
            int tens = Character.digit(chars[i], 16);
            int units = Character.digit(chars[i + 1], 16);
            if (tens > -1 && units > -1) {
                res[i / 2] = (byte) (tens<<4 + units);
            } else {
                throw new Exception("hex字符串只能包含'0'..'9'||'a'..'f'||'A'..'F'");
            }
        }
        return res;
    }

    /**
     * 修改秘钥
     * @param secret 秘钥
     * @param selector 扇区
     */
    public void  changeSelectorKey(String secret , int selector ,int secretType) throws Exception {
        byte[] secretByteArray = convertAsciiStringToHexByte(secret);
        if (secretType != SECRET_TYPE_ASCII)
            secretByteArray = convertHexStringToHexByte(secret);
        int block = selector * 4;
        try {
            readerManager.open(CARD_TYPE_M1);
            readerManager.authBlock(block, secretByteArray, KEY_TYPE);
            readerManager.changeKey(block, secretByteArray, KEY_TYPE);
        } catch ( Exception exc) {
            //寻卡失败、验证失败
            exc.printStackTrace();
        }
    }

    /**
     * 写入数据到block
     * @param block
     * @param secret 该扇区的秘钥
     * @param data 要写的数据
     */
    public void writeDataToBlock(int block, String secret,String data, int secretType) throws Exception {
        byte[] secretByteArray = convertAsciiStringToHexByte(secret);
        if (secretType != SECRET_TYPE_ASCII)
            secretByteArray = convertHexStringToHexByte(secret);
        try {
            readerManager.open(CARD_TYPE_M1);
            readerManager.authBlock(block, secretByteArray, KEY_TYPE);
            byte[] hexData = Ulitily.hexStringToByteArray(data);
            readerManager.writeBlock(block, hexData);
        } catch (Exception exc) {
            //寻卡失败、验证失败
            exc.printStackTrace();
        }
    }

    /**
     * 读身份证
     * @param photoPath 图片保存路径
     */
    @WorkerThread
    public IdCard readIdCard(String photoPath){
        try {
            readerManager.open("id_sam");
            ByteArrayOutputStream idReadTextPhoto = new ByteArrayOutputStream();
            readerManager.control(ReaderExtra.ID_READ_TEXT_PHOTO, idReadTextPhoto);
            String textPhoto = Ulitily.byteArrayToHexString(idReadTextPhoto.toByteArray());
            if (textPhoto != null) {
                Log.i(tag, "身份证个人信息获取成功");
                String[] decodeInfo =new String[10];
                StringBuilder imagePath = new StringBuilder();
                IDCReaderSDK.initialize(photoPath);
                IDCReaderSDK.decodeSamAck(textPhoto, decodeInfo, imagePath);
                IdCard idCard = new IdCard();
                idCard.setName(decodeInfo[0]);
                idCard.setSex( decodeInfo[1]);
                idCard.setNation(decodeInfo[2]);
                idCard.setBirthday(decodeInfo[3]);
                idCard.setAddress(decodeInfo[4]);
                idCard.setId(decodeInfo[5]);
                idCard.setOffice(decodeInfo[6]);
                idCard.setValidateDate(decodeInfo[7]);
                idCard.setPhotoPath(imagePath.toString());
                return idCard;
            }
        } catch (Exception exc) {
            //寻卡失败
            exc.printStackTrace();
        }
        return null;
    }

    /**
     * 将raw下的文件复制到sdCard
     */
    public boolean setBmpDecodeEnv(Context context) {

        String path=context.getFilesDir().getAbsolutePath();
        String filename = "base.dat";
        try{
            String databaseFilename = path + "/" + filename;
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdir();
            if (!(new File(databaseFilename)).exists()){
                InputStream is = context.getResources().openRawResource(R.raw.base);
                FileOutputStream fos = new FileOutputStream(databaseFilename);
                byte[] buffer = new byte[8192];
                int count = 0;
                while ((count = is.read(buffer)) > 0)
                {
                    fos.write(buffer, 0, count);
                }
                fos.close();
                is.close();
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }

        String filename1 = "license.lic";
        try{
            String databaseFilename = path + "/" + filename1;
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdir();
            if (!(new File(databaseFilename)).exists()){
                InputStream is = context.getResources().openRawResource(R.raw.license);
                FileOutputStream fos = new FileOutputStream(databaseFilename);
                byte[] buffer = new byte[8192];
                int count = 0;
                while ((count = is.read(buffer)) > 0)
                {
                    fos.write(buffer, 0, count);
                }
                fos.close();
                is.close();
            }
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public void close() {

        //释放资源
        try {
            readerManager.close();
            Log.e(tag, "-------->释放资源1");
        } catch (Exception e1) {
            Log.e(tag, "disconnect Exception3" + e1.getMessage());
        }
        try {
            readerManager.control(ReaderExtra.DISCONNECT);
            Log.e(tag, "-------->释放资源2");
        } catch (Exception e) {
            Log.e(tag, "disconnect Exception3" + e.getMessage());
        }

    }
}
